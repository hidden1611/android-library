package nna.elcom.library.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;

import nna.elcom.library.widget.R;

/**
 * Created by Ann on 4/6/16.
 */

public class SimpleDialog {

    enum MessageBoxType {
        None,
        OKOnly,
        OkCancel,
        YesNo
    }

    public static void showMessage(Context context, String message){

        showMessage(context,
                message,
                MessageBoxType.OKOnly,
                null);

    }

    public static void showMessage(Context context, String message, DialogInterface.OnClickListener listener){

        showMessage(context,
                message,
                MessageBoxType.OkCancel,
                listener);

    }

    public static void showMessage(Context context, String message, MessageBoxType boxType, DialogInterface.OnClickListener listener){
        showDialog(context,
                "Alert",
                message,
                boxType,
                null,
                listener,
                null);

    }

    public static void showMessage(Context context, String title, String message, MessageBoxType boxType, DialogInterface.OnClickListener listener){
        showDialog(context,
                title,
                message,
                boxType,
                null,
                listener,
                null);

    }


    private static void showDialog(Context context,
                                   String title,
                                   String message,
                                   MessageBoxType type ,
                                   String[] listItems,
                                   DialogInterface.OnClickListener listener,
                                   DialogInterface.OnClickListener selectedItemListener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(title);
        builder.setMessage(message);
        if (listItems != null) {
            builder.setItems(listItems, selectedItemListener);
        }

        switch(type) {

            case OKOnly:
                builder.setPositiveButton("OK",listener);
                break;
            case OkCancel:
                builder.setPositiveButton("OK",listener);
                builder.setNegativeButton("Cancel",listener);
                break;
            case YesNo:
                builder.setPositiveButton("Yes",listener);
                builder.setNegativeButton("No",listener);
                break;
            case None:
                break;
        }
        builder.create().show();
    }


}

