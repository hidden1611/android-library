//
// Created by Ann on 2/13/17.
//

#ifndef TESTSUPPERPOWERED_SUPPERPOWEREDEXAMPLE_H
#define TESTSUPPERPOWERED_SUPPERPOWEREDEXAMPLE_H



#include <math.h>
#include <pthread.h>

#include "SuperpoweredExample.h"
#include <SuperpoweredAdvancedAudioPlayer.h>
#include <SuperpoweredFilter.h>
#include <SuperpoweredRoll.h>
#include <SuperpoweredFlanger.h>
#include <AndroidIO/SuperpoweredAndroidAudioIO.h>

class SuperpoweredExample {


public:

    SuperpoweredExample(unsigned int samplerate, unsigned int buffersize);
    ~SuperpoweredExample();

    bool process(short int *output, unsigned int numberOfSamples);
    void onPlayPause(bool play);
    void nextTrack();
    void stop();
    void setSource(const char *pathA, const char *pathB, unsigned int index);
    void setSource(const char *pathA);
    double currentMs();
    void callback(int type);
    void seekTo(long ms);

private:
    SuperpoweredAndroidAudioIO *audioSystem;
    SuperpoweredAdvancedAudioPlayer *playerA, *playerB;
    SuperpoweredRoll *roll;
    SuperpoweredFilter *filter;
    SuperpoweredFlanger *flanger;
    float *stereoBuffer;
    unsigned char activeFx;
    float crossValue, volA, volB;
    unsigned int samplerate, buffersize;
    bool isCrossAudio;

};


#endif //TESTSUPPERPOWERED_SUPPERPOWEREDEXAMPLE_H
