//
// Created by Ann on 2/13/17.
//

#include "SuperpoweredExample.h"
#include <SuperpoweredSimple.h>
#include <SuperpoweredCPU.h>
#include <jni.h>
#include <stdio.h>
#include <android/log.h>
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_AndroidConfiguration.h>


static SuperpoweredExample *example = NULL;

extern "C" JNIEXPORT jdouble
Java_elcom_nna_ehotel_library_player_core_SupperpoweredAudio_currentMs(JNIEnv *env,
                                                                       jobject instance) {

    return example->currentMs();
    // TODO

}

extern "C" JNIEXPORT void
Java_elcom_nna_ehotel_library_player_core_SupperpoweredAudio_seekTo(JNIEnv *env, jobject instance,
                                                                    jlong ms) {

    // TODO
    example->seekTo(ms);

}
extern "C" JNIEXPORT void
Java_elcom_nna_ehotel_library_player_core_SupperpoweredAudio_setSourceSupper__Ljava_lang_String_2(
        JNIEnv *env, jobject instance, jstring pathA_) {
    const char *pathA = env->GetStringUTFChars(pathA_, 0);

    example->setSource(pathA);
    env->ReleaseStringUTFChars(pathA_, pathA);
}
extern "C" JNIEXPORT void
Java_elcom_nna_ehotel_library_player_core_SupperpoweredAudio_setSourceSupper__Ljava_lang_String_2Ljava_lang_String_2I(
        JNIEnv *env, jobject instance, jstring pathA_, jstring pathB_, jint index) {
    const char *pathA = env->GetStringUTFChars(pathA_, 0);
    const char *pathB = env->GetStringUTFChars(pathB_, 0);


    example->setSource(pathA, pathB, index);

    env->ReleaseStringUTFChars(pathA_, pathA);
    env->ReleaseStringUTFChars(pathB_, pathB);
}

extern "C" JNIEXPORT void
Java_elcom_nna_ehotel_library_player_core_SupperpoweredAudio_SuperpoweredExample(JNIEnv *env,
                                                                                 jobject instance,
                                                                                 jint samplerate,
                                                                                 jint buffersize) {


    example = new SuperpoweredExample((unsigned int)samplerate, (unsigned int)buffersize);
    // TODO
}

extern "C" JNIEXPORT void
Java_elcom_nna_ehotel_library_player_core_SupperpoweredAudio_nextTrack(JNIEnv *env,
                                                                       jobject instance,
                                                                       jboolean play) {

    // TODO
    example->nextTrack();

}


extern "C" JNIEXPORT void
Java_elcom_nna_ehotel_library_player_core_SupperpoweredAudio_playPause(JNIEnv *env,
                                                                       jobject instance,
                                                                       jboolean play) {

    // TODO
    example->onPlayPause(play);

}

extern "C" JNIEXPORT void
Java_elcom_nna_ehotel_library_player_core_SupperpoweredAudio_destroyAudio(JNIEnv *env,
                                                                          jobject instance) {

    // TODO
    example->stop();

}



static void playerEventCallbackA(void *clientData, SuperpoweredAdvancedAudioPlayerEvent event, void * __unused value) {

    if (event == SuperpoweredAdvancedAudioPlayerEvent_LoadSuccess) {
        SuperpoweredAdvancedAudioPlayer *playerA = *((SuperpoweredAdvancedAudioPlayer **)clientData);
        playerA->setBpm(126.0f);
        playerA->setFirstBeatMs(353);
        playerA->setPosition(playerA->firstBeatMs, false, false);
        __android_log_print(ANDROID_LOG_VERBOSE, "nna", "start");
        printf("nna start");
        example->callback(0);
    }else if(event == SuperpoweredAdvancedAudioPlayerEvent_LoopEnd){
        __android_log_print(ANDROID_LOG_VERBOSE, "nna", "end");
        printf("nna end");
    }


}
static void playerEventCallbackB(void *clientData, SuperpoweredAdvancedAudioPlayerEvent event, void * __unused value) {
    if (event == SuperpoweredAdvancedAudioPlayerEvent_LoadSuccess) {
        SuperpoweredAdvancedAudioPlayer *playerB = *((SuperpoweredAdvancedAudioPlayer **)clientData);
        playerB->setBpm(123.0f);
        playerB->setFirstBeatMs(40);
        playerB->setPosition(playerB->firstBeatMs, false, false);
    };
}

static bool audioProcessing(void *clientdata, short int *audioIO, int numberOfSamples, int __unused samplerate) {
    return ((SuperpoweredExample *)clientdata)->process(audioIO, (unsigned int)numberOfSamples);
}

SuperpoweredExample::SuperpoweredExample(unsigned int msamplerate, unsigned int mbuffersize) {


    buffersize = mbuffersize;
    samplerate = msamplerate;

}

SuperpoweredExample::~SuperpoweredExample() {

    delete audioSystem;
    delete playerA;
    delete playerB;
    delete roll;
    delete filter;
    delete flanger;
    free(stereoBuffer);

}

bool SuperpoweredExample::process(short int *output, unsigned int numberOfSamples) {


    if(isCrossAudio) {
        bool masterIsA = (crossValue <= 0.5f);
        double masterBpm = masterIsA ? playerA->currentBpm : playerB->currentBpm;
        double msElapsedSinceLastBeatA = playerA->msElapsedSinceLastBeat; // When playerB needs it, playerA has already stepped this value, so save it now.

        bool silence = !playerA->process(stereoBuffer, false, numberOfSamples, volA, masterBpm,
                                         playerB->msElapsedSinceLastBeat);
        if (playerB->process(stereoBuffer, !silence, numberOfSamples, volB, masterBpm,
                             msElapsedSinceLastBeatA))
            silence = false;

        if (!silence) SuperpoweredFloatToShortInt(stereoBuffer, output, numberOfSamples);
        return !silence;
    } else{

        bool silence = !playerA->process(stereoBuffer, false, numberOfSamples, volA, 0, -1);
        if (!silence) SuperpoweredFloatToShortInt(stereoBuffer, output, numberOfSamples);
        return !silence;

    }
}

void SuperpoweredExample::nextTrack() {

    if(isCrossAudio) {
        if (volA == 1.0) {
            volA = 0.0;
            volB = 1.0;
        } else {
            volA = 1.0;
            volB = 0.0;
        }
    }

}

void SuperpoweredExample::onPlayPause(bool play) {

    if(isCrossAudio) {
        if (!play) {
            playerA->pause();
            playerB->pause();
        } else {
            playerA->play(false);
            playerB->play(false);
        };
    } else{
        if (!play) {
            playerA->pause();
        } else {
            playerA->play(false);
        };

    }
    SuperpoweredCPU::setSustainedPerformanceMode(play); // <-- Important to prevent audio dropouts.
}

void SuperpoweredExample::stop() {

    audioSystem->stop();

}

void SuperpoweredExample::setSource(const char *pathA, const char *pathB, unsigned int index) {

//    stop();
    isCrossAudio = true;

    stereoBuffer = (float *)memalign(16, (buffersize + 16) * sizeof(float) * 2);
    playerA = new SuperpoweredAdvancedAudioPlayer(&playerA,playerEventCallbackA, samplerate, 0);
    playerA->open(pathA);
    playerA->play(false);


    playerB = new SuperpoweredAdvancedAudioPlayer(&playerB,playerEventCallbackB, samplerate, 0);
    playerB->open(pathB);
    playerB->play(false);

    if(index == 0) {
        volA = 1.0;
        volB = 0.0;
    }else{
        volA = 0.0;
        volB = 1.0;

    }

    audioSystem = new SuperpoweredAndroidAudioIO(samplerate, buffersize, false, true, audioProcessing, this, -1, SL_ANDROID_STREAM_MEDIA, buffersize * 2);

}

void SuperpoweredExample::setSource(const char *pathA) {

    isCrossAudio = false;

    stereoBuffer = (float *)memalign(16, (buffersize + 16) * sizeof(float) * 2);
    playerA = new SuperpoweredAdvancedAudioPlayer(&playerA,playerEventCallbackA, samplerate, 0);
    playerA->open(pathA);
    playerA->play(false);

    volA = 1.0;
    audioSystem = new SuperpoweredAndroidAudioIO(samplerate, buffersize, false, true, audioProcessing, this, -1, SL_ANDROID_STREAM_MEDIA, buffersize * 2);


}

double SuperpoweredExample::currentMs() {

    return playerA->positionMs;
}

void SuperpoweredExample::callback(int type) {

//    JNIEnv *NewEnv = AttachJava();
//    jclass  clazz = myJniEnv->FindClass("elcom/nna/ehotel/library/player/core/SupperpoweredAudio");
//    jmethodID mCurrentActivityId = myJniEnv->GetMethodID(clazz,"event","(I)V");
//    myJniEnv->CallVoidMethod(Savedinstance, mCurrentActivityId,type);

}

void SuperpoweredExample::seekTo(long ms) {

    if(isCrossAudio) {
        playerA->seek(0);
        playerB->seek(0);
    } else{
        playerA->seek(0);
    }
}


