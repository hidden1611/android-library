package elcom.nna.ehotel.library.wiget.background;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import elcom.nna.ehotel.library.R;


/**
 * Created by Ann on 11/29/15.
 */
public class BackgroundView extends ImageView implements IBackgroundView{
    private static final String TAG = "BackgroundView";
    public static String host;

    public static int INTERVAL = 30;

    private List<Background> backgrounds;
    private int indexBackground;

    private int bgDefault;

    private Handler mHandler = new Handler();
    private UpdateBackground mUpdateBackgound;

    public BackgroundView(Context context) {
        super(context);
        init(context,null);
    }

    public BackgroundView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public BackgroundView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BackgroundView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context,attrs);
    }

    private void init(Context context, AttributeSet attrs){

        Log.d(TAG, "init ");
        if(attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BackgroundView);
            bgDefault = a.getResourceId(R.styleable.BackgroundView_bgdefault, R.drawable.ic_background_default);
            INTERVAL = a.getInteger(R.styleable.BackgroundView_bgvinterval, 30);
            a.recycle();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Log.d(TAG, "onAttachedToWindow ");

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopSlide();
        Log.d(TAG, "onDetachedFromWindow ");

    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        Log.d(TAG, "onVisibilityChanged " + visibility);

        if(visibility == View.VISIBLE
                && backgrounds != null
                && backgrounds.size() > 0 ){
            startSlide();
        }else if(visibility == View.INVISIBLE || visibility == View.GONE){
            stopSlide();
        }
    }



    @Override
    public void setDefault(int defaut) {
        bgDefault = defaut;

    }

    @Override
    public void setUrlBackground(String server, final String url) {
        this.host = server;

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
//                try {
//                    Document document = Jsoup.connect(host + url).get();
//                    backgrounds = new ArrayList<>();
//                    for (Element element : document.select("item")) {
//                        backgrounds.add(new Background(element));
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                startSlide();
            }
        }.execute();

    }

    @Override
    public void setListBackground(List<? extends Background> backgrounds) {
        this.backgrounds = (List<Background>) backgrounds;
        startSlide();

    }

    @Override
    public void setBackground(int background) {
        stopSlide();

        Glide.with(getContext())
                .load(background)
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(bgDefault)
                .crossFade()
                .into(BackgroundView.this);
    }

    private void startSlide(){

        stopSlide();
        if(backgrounds != null && backgrounds.size() == 1){
            Glide.with(getContext())
                    .load(backgrounds.get(++indexBackground % backgrounds.size()).getUrlImage())
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .error(bgDefault)
                    .crossFade()
                    .into(BackgroundView.this);
        }else {
            mUpdateBackgound = new UpdateBackground();
            mHandler.post(mUpdateBackgound);
        }

    }
    public void stopSlide(){

        if(mUpdateBackgound != null){
            mUpdateBackgound.isStop = true;
            mHandler.removeCallbacks(mUpdateBackgound);
        }
    }

    private class UpdateBackground  implements Runnable {
        boolean isStop = false;
        int count = 0;

        public UpdateBackground(){
            count = 169;
        }

        @Override
        public void run() {
            if(backgrounds!= null && backgrounds.size() != 0) {
                Glide.with(getContext())
                        .load(backgrounds.get(++indexBackground % backgrounds.size()).getUrlImage())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(bgDefault)
                        .centerCrop()
                        .error(bgDefault)
                        .into(BackgroundView.this);

            }else{
               Glide.with(getContext())
                        .load(bgDefault)
                       .placeholder(bgDefault)
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .into(BackgroundView.this);
                 
            }

            if(!isStop)
                mHandler.postDelayed(this, INTERVAL*1000);
        }
    }
}
