package elcom.nna.ehotel.library.player.core;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.MediaRouter;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;
import org.videolan.libvlc.util.AndroidUtil;
import org.videolan.vlc.PlaybackService;
import org.videolan.vlc.VLCApplication;
import org.videolan.vlc.gui.PlaybackServiceActivity;
import org.videolan.vlc.media.MediaWrapper;
import org.videolan.vlc.util.VLCInstance;

import java.util.ArrayList;
import java.util.List;

import elcom.nna.ehotel.library.player.model.IVideo;

/**
 * Created by Ann on 11/22/16.
 */

public class VlcCorePlayer extends AbstractCorePlayer implements PlaybackService.Callback,
        IVLCVout.Callback,
        PlaybackService.Client.Callback{

    private static final String TAG = "VlcCorePlayer";

    private PlaybackService mService;
    private final PlaybackServiceActivity.Helper mHelper = new PlaybackServiceActivity.Helper(getContext(), this);
    private MediaRouter mMediaRouter;
    private MediaRouter.SimpleCallback mMediaRouterCallback;
    private boolean mPlaybackStarted;
    private boolean mSurfacesAttached;

    boolean decodeSW = true;

    public VlcCorePlayer(Context context) {
        super(context);
    }

    @Override
    void initUI(Context context, AttributeSet attributeSet) {

        VLCApplication.context = context;
        if (!VLCInstance.testCompatibleCPU(getContext())) {
            return;
        }

        if (AndroidUtil.isJellyBeanMR1OrLater()) {
            // Get the media router service (Miracast)
            mMediaRouter = (MediaRouter) VLCApplication.getAppContext().getSystemService(Context.MEDIA_ROUTER_SERVICE);
            mMediaRouterCallback = new MediaRouter.SimpleCallback() {
                @Override
                public void onRoutePresentationDisplayChanged(
                        MediaRouter router, MediaRouter.RouteInfo info) {
                    Log.d(TAG, "onRoutePresentationDisplayChanged: info=" + info);
                    final Display presentationDisplay = info.getPresentationDisplay();
                    final int newDisplayId = presentationDisplay != null ? presentationDisplay.getDisplayId() : -1;
                }
            };
            Log.d(TAG, "MediaRouter information : " + mMediaRouter  .toString());
        }

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopPlayback();
        if (mService != null)
            mService.removeCallback(this);
        mHelper.onStop();
    }


    private static LibVLC LibVLC() {
        return VLCInstance.get();
    }

    private void startPlayback(){

        LibVLC().setOnHardwareAccelerationError(new LibVLC.HardwareAccelerationError() {
            @Override
            public void eventHardwareAccelerationError() {

            }
        });

        final IVLCVout vlcVout = mService.getVLCVout();
        vlcVout.detachViews();
        vlcVout.setVideoView(this);
        mediaRouterAddCallback(false);
        vlcVout.addCallback(this);
        vlcVout.attachViews();
        mPlaybackStarted = true;
        mSurfacesAttached = true;


    }

    private void stopPlayback() {

        if (!mPlaybackStarted)
            return;
        LibVLC().setOnHardwareAccelerationError(null);

        mPlaybackStarted = false;

        mService.removeCallback(this);
        final IVLCVout vlcVout = mService.getVLCVout();
        vlcVout.removeCallback(this);
        if (mSurfacesAttached)
            vlcVout.detachViews();
        /* Stop listening for changes to media routes. */
        if (mMediaRouter != null)
            mediaRouterAddCallback(false);

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void mediaRouterAddCallback(boolean add) {
        if(!AndroidUtil.isJellyBeanMR1OrLater() || mMediaRouter == null) return;
        if(add)
            mMediaRouter.addCallback(MediaRouter.ROUTE_TYPE_LIVE_VIDEO, mMediaRouterCallback);
        else
            mMediaRouter.removeCallback(mMediaRouterCallback);
    }

    @Override
    public void setData(@NonNull List<? extends IVideo> videos) {
        super.setData(videos);

        if(mService == null){
            mHelper.onStart();
        }else {
            setData();
        }
    }

    @Override
    public void replay() {
        if(!mService.isPlaying()){
            mService.play();
        }
    }

    @Override
    public void play() {

        if(decodeSW) {
            mService.playIndex(index, MediaWrapper.MEDIA_VIDEO | MediaWrapper.MEDIA_NO_HWACCEL);
        }else {
            mService.playIndex(index, MediaWrapper.MEDIA_VIDEO );
        }

        mOnEventListener.OnStartedItem(videos.get(index));


    }

    @Override
    public void stop() {

        if(mService != null)
            mService.stop();


    }

    @Override
    public void pause() {

        mService.play();
        mService.pause();

    }

    @Override
    public void seek(long milisecond) {
        mService.setTime(milisecond);
    }

    @Override
    public long getCurrent() {
        return mService.getTime();
    }

    @Override
    public long getLength() {
        return mService.getLength();
    }


    @Override
    public boolean isPlaying() {
        return mService == null || mService.isPlaying();
    }

    @Override
    public void onNewLayout(IVLCVout vlcVout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {

        if (width * height == 0)
            return;
        mOnEventListener.OnEventChangeSize(width, height);
    }

    @Override
    public void onSurfacesCreated(IVLCVout vlcVout) {

    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vlcVout) {

    }

    @Override
    public void update() {

    }

    @Override
    public void updateProgress() {

    }

    @Override
    public void onMediaEvent(Media.Event event) {


    }

    @Override
    public void onMediaPlayerEvent(MediaPlayer.Event event) {

        switch (event.type){

            case MediaPlayer.Event.Playing:
                mOnEventListener.OnPlayingItem();
                break;

            case MediaPlayer.Event.Paused:
                break;

            case MediaPlayer.Event.Stopped:
                mOnEventListener.OnStopedItem();
                break;

            case MediaPlayer.Event.EncounteredError:
                mOnEventListener.OnErrorItem();
                break;

            case MediaPlayer.Event.EndReached:
                mOnEventListener.OnEventComplete();
                break;
        }

    }

    @Override
    public void onConnected(PlaybackService service) {

        mService = service;
        startPlayback();
        setData();
    }

    @Override
    public void onDisconnected() {

        if(mService != null)
            mService.stop();

    }

    private void setData(){

        List<MediaWrapper> mediaWrappers = new ArrayList<>();
        index = 0;
        for (int i = 0; i < videos.size(); i++) {

            IVideo video = videos.get(i);
            MediaWrapper mw = new MediaWrapper(Uri.parse(video.getUrl()));
            mw.setTitle(video.getSubtitle());
            mw.removeFlags(MediaWrapper.MEDIA_FORCE_AUDIO);
            mw.addFlags(MediaWrapper.MEDIA_VIDEO);

            if(decodeSW) {
                mw.addFlags(MediaWrapper.MEDIA_NO_HWACCEL);
            }

            mw.setmSubtitleURL(video.getSubtitle());
            if(video.isPlaying()){
                index = i;
            }

            mediaWrappers.add(mw);
        }

        mService.addCallback(this);
        mService.load(mediaWrappers);

        play();

    }

    public boolean isDecodeSW() {
        return decodeSW;
    }

    public void setDecodeSW(boolean decodeSW) {
        this.decodeSW = decodeSW;
    }
}
