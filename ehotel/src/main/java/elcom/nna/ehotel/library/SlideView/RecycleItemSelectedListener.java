package elcom.nna.ehotel.library.SlideView;

import android.view.View;

/**
 * Created by baotruongtuan on 7/24/16.
 */
public interface RecycleItemSelectedListener {
    void onItemFocusedListener(View view, int position);
    void onItemSelectedListener(View view, int position);
}
