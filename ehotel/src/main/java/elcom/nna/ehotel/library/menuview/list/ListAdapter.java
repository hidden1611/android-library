package elcom.nna.ehotel.library.menuview.list;


import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import elcom.nna.ehotel.library.menuview.IOnEventListener;
import elcom.nna.ehotel.library.R;
import elcom.nna.ehotel.library.menuview.model.IMenu;

import static android.support.v7.widget.RecyclerView.Adapter;
import static android.view.View.OnClickListener;

/**
 * Created by Ann on 3/11/16.
 */
public class ListAdapter extends Adapter<ListAdapter.ServiceViewHolder> {

    private List<IMenu> items;

    public ListAdapter() {
        this.items = new ArrayList<>();
    }

    public void addItem(IMenu item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public void setData(List<? extends IMenu> items){
        this.items = (List<IMenu>) items;
        notifyDataSetChanged();
    }

    public IMenu getItem(int index){
        return items.get(index);
    }

    public List<IMenu> getItems(){
        return items;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }


    @Override
    public ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_item, parent, false);
        return new ServiceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ServiceViewHolder holder, int position) {

        holder.setValue(items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class ServiceViewHolder extends ViewHolder {

        TextView tvRowServiceText;
        ImageView ivRowServiceImage;

        public ServiceViewHolder(View itemView) {
            super(itemView);
            tvRowServiceText = (TextView)itemView.findViewById(R.id.tv);
            ivRowServiceImage = (ImageView)itemView.findViewById(R.id.iv);
            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventListener != null) {
                        onEventListener.onClicked(v, getLayoutPosition());
                    }

                }
            });

            itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {

                    if(hasFocus && onEventListener != null){

                        onEventListener.onSelected(v, getLayoutPosition());

                    }

                }
            });
        }

        public void setValue(IMenu item) {
            tvRowServiceText.setText(Html.fromHtml(item.getName()));
            Glide.with(itemView.getContext())
                    .load(item.getUrlIcon())
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .crossFade()
                    .into(ivRowServiceImage);

        }
    }


    public void setOnItemClickListener(IOnEventListener onItemClickListener) {
        this.onEventListener = onItemClickListener;
    }

    public static IOnEventListener onEventListener;

}



