package elcom.nna.ehotel.library.player.model;

import android.os.Parcelable;

/**
 * Created by nna on 24/05/2016.
 */
public interface IVideo extends Parcelable {

    void setName(String name) ;

    void setSubtitle(String subtitle);

    void setUrl(String url);

    void setPlaying(boolean playing);

    String getName();
    String getSubtitle();
    String getUrl();
    boolean isPlaying();


}
