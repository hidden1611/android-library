package elcom.nna.ehotel.library.menuview.horizontal;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import elcom.nna.ehotel.library.gridview.CustomLinearLayoutManager;
import elcom.nna.ehotel.library.menuview.ICoreMenu;
import elcom.nna.ehotel.library.menuview.IOnEventListener;
import elcom.nna.ehotel.library.menuview.model.IMenu;

/**
 * Created by Ann on 4/29/16.
 */
public class CoreHorizontal implements ICoreMenu {


    private final Context context;

    private final RecyclerView rv;
    private final ViewGroup viewGroup;
    private final int colorBgText;
    private final int colorText;
    private int columnWidth = -1;
    private int columnHeight = -1;

    private HorizontalAdapter horizontalAdapter;
    LinearLayoutManager linearLayoutManager;



    public CoreHorizontal(ViewGroup viewGroup,  int columnWidth, int columnHeight, int colorBgText, int colorText){
        this.viewGroup = viewGroup;
        this.columnWidth = columnWidth;
        this.columnHeight = columnHeight;
        this.colorBgText = colorBgText;
        this.colorText = colorText;

        this.context = viewGroup.getContext();
        rv = new RecyclerView(viewGroup.getContext());
        rv.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rv.setLayoutParams(params);
        viewGroup.addView(rv);

        initRecycecleView();

    }


    private void initRecycecleView(){

        linearLayoutManager = new CustomLinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv.setLayoutManager(linearLayoutManager);
        horizontalAdapter = new HorizontalAdapter(columnWidth, columnHeight,colorBgText, colorText);
        rv.setAdapter(horizontalAdapter);

    }

    @Override
    public void setData(List<? extends IMenu> menus) {

        horizontalAdapter.setData(menus);

    }

    @Override
    public IMenu getItem(int post) {
        return horizontalAdapter.getItem(post);
    }

    @Override
    public List<? extends IMenu> getItems() {
        return horizontalAdapter.getItems();
    }

    @Override
    public void setOnEventListenner(IOnEventListener onEventListenner) {

        horizontalAdapter.setOnItemClickListener(onEventListenner);

    }

    @Override
    public void setSelection(int index) {

        rv.getLayoutManager().scrollToPosition(index);

    }


}
