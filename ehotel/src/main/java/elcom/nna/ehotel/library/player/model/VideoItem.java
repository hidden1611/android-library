package elcom.nna.ehotel.library.player.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nna on 25/05/2016.
 */
public class VideoItem implements IVideo, Parcelable{

    String name;
    String subtitle;
    String url;
    boolean isPlaying;

    public VideoItem(){

    }

    public VideoItem(Parcel in) {
        name = in.readString();
        subtitle = in.readString();
        url = in.readString();
        isPlaying = in.readByte() != 0;
    }

    public static final Creator<VideoItem> CREATOR = new Creator<VideoItem>() {
        @Override
        public VideoItem createFromParcel(Parcel in) {
            return new VideoItem(in);
        }

        @Override
        public VideoItem[] newArray(int size) {
            return new VideoItem[size];
        }
    };

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSubtitle() {
        return subtitle;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public boolean isPlaying() {
        return isPlaying;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(subtitle);
        dest.writeString(url);
        dest.writeByte((byte) (isPlaying ? 1 : 0));
    }

    public boolean hasTV() {
        return false;
    }
}
