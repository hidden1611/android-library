package elcom.nna.ehotel.library.SlideView;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import elcom.nna.ehotel.library.R;
import elcom.nna.ehotel.library.utils.BitmapUtils;


/**
 * Created by baotruongtuan on 7/21/16.
 */
public class SlideViewAdapter extends RecyclerView.Adapter<SlideViewAdapter.MyViewHolder> {

    private List<ISlideViewItem> dataSet;
    private RecycleItemSelectedListener mItemSelectedListener;



    public SlideViewAdapter(List<? extends ISlideViewItem> dataSet, RecycleItemSelectedListener mItemSelectedListener) {
        this.dataSet = (List<ISlideViewItem>) dataSet;
        this.mItemSelectedListener = mItemSelectedListener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.slide_card_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TextView tvNameService = holder.tvNameService;
        final ImageView imvImgService = holder.imvImgService;
        tvNameService.setText(dataSet.get(position).getName());

        Log.e("BAOTT", "" + holder);

        float textSize = tvNameService.getTextSize();
        float transformTextSize = (float) (textSize * SlideViewConfig.RATIO_TEXTSIZE);

        if (dataSet.get(position).isSelected()) {

            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) imvImgService.getLayoutParams();
            layoutParams.width = SlideViewConfig.TRANSFORM_ITEM_WIDTH;
            imvImgService.setLayoutParams(layoutParams);

            tvNameService.setTextSize(transformTextSize);

            mItemSelectedListener.onItemFocusedListener(tvNameService, position);
        } else {

            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) imvImgService.getLayoutParams();
            layoutParams.width = SlideViewConfig.ITEM_WIDTH;
            imvImgService.setLayoutParams(layoutParams);
            tvNameService.setTextSize(textSize);
        }


        Picasso.with(imvImgService.getContext())
                .load(dataSet.get(position).getImage())
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        Bitmap bMapReflection = BitmapUtils.getRefelection(bitmap);
                        imvImgService.setImageBitmap(bMapReflection);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });


    }

    public void setSelection(int position) {
        int selectedPosition = getSelectedPosition();
        dataSet.get(selectedPosition).setSelected(false);
        dataSet.get(position).setSelected(true);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public List<ISlideViewItem> getDataSet() {
        return dataSet;
    }

    public int getSelectedPosition() {
        try {
            for (int i = 0; i < dataSet.size(); i++) {
                ISlideViewItem model = dataSet.get(i);
                if (model.isSelected()) {
                    return i;
                }
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvNameService;
        ImageView imvImgService;

        public MyViewHolder(final View itemView) {
            super(itemView);

            this.tvNameService = (TextView) itemView.findViewById(R.id.tvNameService);
            this.imvImgService = (ImageView) itemView.findViewById(R.id.imvImgService);

        }
    }


}
