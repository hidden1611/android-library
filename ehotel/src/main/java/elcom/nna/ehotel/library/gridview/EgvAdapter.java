package elcom.nna.ehotel.library.gridview;

/**
 * Created by Ann on 7/24/16.
 */

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import elcom.nna.ehotel.library.R;
import elcom.nna.ehotel.library.menuview.EMenuView;
import elcom.nna.ehotel.library.menuview.IOnEventListener;


public class EgvAdapter extends RecyclerView.Adapter<EgvAdapter.VH> {

    private int columnWidth;
    private int columnHeight;
    private int colorBgText;
    private int colorText;
    private List<IEgvLine> items;

    public EgvAdapter() {
        this.items = new ArrayList<>();
    }

    public EgvAdapter(int columnWidth, int columnHeight, int colorBgText, int colorText) {

        this.items = new ArrayList<>();
        this.columnWidth = columnWidth;
        this.columnHeight = columnHeight;
        this.colorBgText = colorBgText;
        this.colorText = colorText;
    }

    public void addItems(List<IEgvLine> item) {
        items = item;
        notifyDataSetChanged();
    }

    public void addItem(IEgvLine item) {
        items.add(item);
        notifyDataSetChanged();
    }


    public IEgvLine getItem(int index) {
        return items.get(index);
    }

    public List<IEgvLine> getItems() {
        return items;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }


    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.egv_item, parent, false);
        return new VH(v);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {

        holder.setValue(items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class VH extends RecyclerView.ViewHolder {

        TextView tvTitle;
        EMenuView ev;

        public VH(View itemView) {
            super(itemView);
            tvTitle = (TextView)itemView.findViewById(R.id.tvTitle);
            ev = (EMenuView)itemView.findViewById(R.id.ev);

            itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {

                    if(hasFocus) {
                        Log.d(TAG, "VH " + getLayoutPosition() + VH.this.getLayoutPosition());
                    }
                }
            });

            ev.setOnEventListener(new IOnEventListener() {
                @Override
                public void onClicked(View view, int pos) {

                    if (onItemClickListener != null) {
                        onItemClickListener.OnClick(view, getLayoutPosition(), pos);
                        Log.d(TAG, "VH " + ev.getItem(pos));
                    }

                }

                @Override
                public void onSelected(View view, int pos) {

                    if (onItemSelectListener != null) {
                        onItemSelectListener.OnSelect(view, getLayoutPosition(), pos);
                        Log.d(TAG, "VH " + getLayoutPosition() + VH.this.getLayoutPosition());
                    }

                }
            });
        }

        public void setValue(IEgvLine item) {

            tvTitle.setText(item.getName());
            ev.setData(item.getCells());

        }
    }

    private static final String TAG = "TestAdapter";


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener){

        this.onItemSelectListener = onItemSelectListener;
    }


    public static OnItemClickListener onItemClickListener;
    public static OnItemSelectListener onItemSelectListener;

    public interface OnItemClickListener {
        void OnClick(View view, int line, int cell);
    }

    public interface OnItemSelectListener {
        void OnSelect(View view, int line, int cell);
    }
}
