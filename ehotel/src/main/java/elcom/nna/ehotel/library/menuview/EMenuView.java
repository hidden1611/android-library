package elcom.nna.ehotel.library.menuview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import java.util.List;

import elcom.nna.ehotel.library.R;
import elcom.nna.ehotel.library.menuview.grid.CoreGrid;
import elcom.nna.ehotel.library.menuview.horizontal.CoreHorizontal;
import elcom.nna.ehotel.library.menuview.list.CoreList;
import elcom.nna.ehotel.library.menuview.model.IMenu;

/**
 * Created by Ann on 4/29/16.
 */
public class EMenuView extends LinearLayout implements IMenuView{

    private final static int TYPE_HORIZONTAL = 0;
    private final static int TYPE_GRID = 1;
    private final static int TYPE_LIST = 2;

    private ICoreMenu coreMenu;
    private int type;
    private int columnWidth;
    private int columnHeight;
    private int colorBgText;
    private int colorText;

    public EMenuView(Context context) {
        super(context);
        if(!isInEditMode()) {
            init(context, null);
        }
    }

    public EMenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    public EMenuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EMenuView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    private void init(Context context, AttributeSet attrs){


        setDescendantFocusability(FOCUS_AFTER_DESCENDANTS);
            if (attrs != null) {

                TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EMenuView);
                type = a.getInteger(R.styleable.EMenuView_typeMenuView, 0);
                columnWidth = a.getDimensionPixelSize(R.styleable.EMenuView_evColumnWidth, -1);
                columnHeight = a.getDimensionPixelSize(R.styleable.EMenuView_evColumnHeight, -1);
                colorBgText = a.getColor(R.styleable.EMenuView_evColorBgText, getResources().getColor(R.color.bg_text));
                colorText = a.getColor(R.styleable.EMenuView_evColorText, getResources().getColor(R.color.textcolor));
                a.recycle();

            }

            setCoreMenu(type);


    }

    private void setCoreMenu(int type){

        if(type == TYPE_HORIZONTAL){
            setCoreMenu(new CoreHorizontal(this, columnWidth, columnHeight, colorBgText, colorText));
        }else if(type == TYPE_GRID){
            setCoreMenu(new CoreGrid(this, columnWidth, columnHeight, colorBgText, colorText));
        }else if(type == TYPE_LIST){
            setCoreMenu(new CoreList(this));
        }

    }
    private void setCoreMenu(ICoreMenu coreMenu){
        this.coreMenu = coreMenu;

    }


    @Override
    public void setData(List<? extends IMenu> menus) {

        coreMenu.setData(menus);

    }

    @Override
    public IMenu getItem(int pos) {
        return coreMenu.getItem(pos);
    }

    @Override
    public List<? extends IMenu> getItems() {
        return coreMenu.getItems();
    }


    @Override
    public void setOnEventListener(IOnEventListener onEventListener) {

        this.coreMenu.setOnEventListenner(onEventListener);

    }

    @Override
    public void setSelection(int index) {

        this.coreMenu.setSelection(index);

    }

    @Override
    public void toogleVisible() {

        if(getVisibility() == View.VISIBLE){

            setVisibility(View.VISIBLE);

        }else{

            setVisibility(View.GONE);
        }

    }


}
