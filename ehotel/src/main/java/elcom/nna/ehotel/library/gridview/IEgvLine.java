package elcom.nna.ehotel.library.gridview;

import java.util.List;

/**
 * Created by nna on 25/07/2016.
 */
public interface IEgvLine {

    List<? extends IEgvCell> getCells();
    String getName();

}
