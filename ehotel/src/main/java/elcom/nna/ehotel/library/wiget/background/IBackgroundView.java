package elcom.nna.ehotel.library.wiget.background;

import java.util.List;

/**
 * Created by Ann on 4/10/16.
 */
public interface IBackgroundView {

    void setDefault(int defaut);

    void setUrlBackground(String host, String url);

    void setListBackground(List<? extends Background> backgrounds);

    void setBackground(int background);

}
