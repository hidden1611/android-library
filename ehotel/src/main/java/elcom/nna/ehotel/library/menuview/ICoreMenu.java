package elcom.nna.ehotel.library.menuview;

import java.util.List;

import elcom.nna.ehotel.library.menuview.model.IMenu;

/**
 * Created by Ann on 4/29/16.
 */
public interface ICoreMenu {


    void setData(List<? extends IMenu> menus);

    IMenu getItem(int post);

    List<? extends IMenu> getItems();

    void setOnEventListenner(IOnEventListener onEventListenner);

    void setSelection(int index);





}
