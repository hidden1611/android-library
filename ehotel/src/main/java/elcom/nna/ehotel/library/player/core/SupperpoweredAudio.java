package elcom.nna.ehotel.library.player.core;

import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.util.Log;

/**
 * Created by Ann on 2/13/17.
 */

public class SupperpoweredAudio extends AbstractAudioKaraokePlayer{


    String samplerateString = null, buffersizeString = null;

    public SupperpoweredAudio(Context context) {
        super(context);

        // Get the device's sample rate and buffer size to enable low-latency Android audio output, if available.
        if (Build.VERSION.SDK_INT >= 17) {
            AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            samplerateString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
            buffersizeString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER);
        }
        if (samplerateString == null) samplerateString = "44100";
        if (buffersizeString == null) buffersizeString = "512";

        SuperpoweredExample(Integer.parseInt(samplerateString),
                Integer.parseInt(buffersizeString));


    }

    @Override
    public void setSource(String urlA, String urlB) {

        if(urlB != null) {
            setSourceSupper(urlA, urlB, getIdPlayed());
        }else {
            setSourceSupper(urlA);
        }

    }

    @Override
    public void stopImpl() {

        destroyAudio();

    }

    @Override
    public void pauseImpl() {

        playPause(playing);
    }

    @Override
    public void releaseImpl() {

    }

    @Override
    public void nextTrackImpl() {

        nextTrack(true);
    }

    @Override
    public void seekAndPlayImpl(long us) {

    }

    @Override
    void seek(long us) {

        seekTo(us);

    }

    @Override
    public long getCurrentMs() {
        return (long) currentMs();
    }

    private native void SuperpoweredExample(int samplerate, int buffersize);
    private native void setSourceSupper(String pathA, String pathB, int index);
    private native void setSourceSupper( String pathA);
    private native void nextTrack(boolean play);
    private native void playPause(boolean play);
    private native void seekTo(long ms);
    private native void destroyAudio();
    private native double currentMs();

    protected void event(int type){
        Log.d(TAG, "event " + type);
    }

    private static final String TAG = "nna - poweredAudio";



    static {
        System.loadLibrary("SuperpoweredExample");
    }
}
