package elcom.nna.ehotel.library.menuview.list;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import elcom.nna.ehotel.library.menuview.ICoreMenu;
import elcom.nna.ehotel.library.menuview.IOnEventListener;
import elcom.nna.ehotel.library.menuview.model.IMenu;

/**
 * Created by Ann on 4/29/16.
 */
public class CoreList implements ICoreMenu {


    private final Context context;

    private final RecyclerView rv;

    private ListAdapter listAdapter;
    LinearLayoutManager linearLayoutManager;



    public CoreList(ViewGroup viewGroup){

        this.context = viewGroup.getContext();
        rv = new RecyclerView(viewGroup.getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rv.setLayoutParams(params);
        viewGroup.addView(rv);

        initRecycecleView();

    }

    private void initRecycecleView(){

        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(linearLayoutManager);
        listAdapter = new ListAdapter();
        rv.setAdapter(listAdapter);

    }

    @Override
    public void setData(List<? extends IMenu> menus) {

        listAdapter.setData(menus);

    }

    @Override
    public IMenu getItem(int post) {
        return listAdapter.getItem(post);
    }

    @Override
    public List<? extends IMenu> getItems() {
        return listAdapter.getItems();
    }

    @Override
    public void setOnEventListenner(IOnEventListener onEventListenner) {

        listAdapter.setOnItemClickListener(onEventListenner);

    }

    @Override
    public void setSelection(int index) {

        rv.scrollToPosition(index);

    }


}
