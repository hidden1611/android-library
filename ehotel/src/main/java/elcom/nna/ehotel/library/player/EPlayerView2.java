package elcom.nna.ehotel.library.player;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.exoplayer2.source.MediaSource;

import java.util.List;

import javax.sql.DataSource;

import elcom.nna.ehotel.library.player.control.IControl;
import elcom.nna.ehotel.library.player.core.AbstractCorePlayer;
import elcom.nna.ehotel.library.player.core.ExoCorePlayer;
import elcom.nna.ehotel.library.player.core.ExoCorePlayer2;
import elcom.nna.ehotel.library.player.core.MediaCorePlayer;
import elcom.nna.ehotel.library.player.core.VlcCorePlayer;
import elcom.nna.ehotel.library.player.dvplayer.Subtitle;
import elcom.nna.ehotel.library.player.dvplayer.Util;
import elcom.nna.ehotel.library.player.model.IVideo;
import elcom.nna.ehotel.library.player.model.VideoItem;

/**
 * Created by Ann on 11/22/16.
 */

public class EPlayerView2 extends RelativeLayout implements AbstractCorePlayer.OnEventListener{

    private static final String TAG = "EPlayer nna";
    public final static int TYPE_DEFAULT_PLAYER = 0;
    public final static int TYPE_VLC_PLAYER = 1;
    public static final int TYPE_EXO_PLAYER = 3;
    public static final int TYPE_EXO_PLAYER2 = 4;
    private static final int MSG_UPDATE_CONTROL_TIMEOUT = 500;
    private static final int MSG_UPDATE = 1;
    private static final int MSG_UPDATE_CONTROL = 2;
    private static final int MSG_SHOW_CONTROL = 3;
    private static final int MSG_SHOW_CONTROL_TIMEOUT = 5000;
    private int type;
    private Context context;
    AbstractCorePlayer corePlayer;
    Subtitle mSubtitle;
    float ratioVideo;
    private int widthScreen;
    private int heightScreen;
    private IControl controlImpl;

    private EPlayerView.OnEventConnected onEventConnected;
    private EPlayerView.OnEventPrepared onEventPrepared;
    private EPlayerView.OnEventStart onEventStart;
    private EPlayerView.OnEventPlaying onEventPlaying;
    private EPlayerView.OnEventStop onEventStop;
    private EPlayerView.OnEventComplete onEventComplete;
    private EPlayerView.OnEventError onEventError;
    private EPlayerView.OnEventChangeSize onEventChangeSize;

    boolean isPause = false;
    private View controlView;


    public void setOnEventConnected(EPlayerView.OnEventConnected connected){
        this.onEventConnected = connected;
    }

    public void setOnEventPrepared(EPlayerView.OnEventPrepared prepared){
        this.onEventPrepared = prepared;
    }

    public void setOnEventStart(EPlayerView.OnEventStart onEventStart){
        this.onEventStart = onEventStart;
    }

    public void setOnEventPlaying(EPlayerView.OnEventPlaying onEventPlaying){
        isPause = false;
        this.onEventPlaying = onEventPlaying;
    }

    public void setOnEventStop(EPlayerView.OnEventStop onEventStop){
        this.onEventStop = onEventStop;
    }

    public void setOnEventComplete(EPlayerView.OnEventComplete onEventComplete){
        this.onEventComplete = onEventComplete;
    }

    public void setOnEventError(EPlayerView.OnEventError onEventError){
        this.onEventError = onEventError;
    }

    public void setOnEventChangeSize(EPlayerView.OnEventChangeSize onEventChangeSize){
        this.onEventChangeSize = onEventChangeSize;
    }



    /*****************************************************
     * Interface
     *****************************************************/
    public interface OnEventPrepared{
        void OnPrepared();

    }

    public interface OnEventConnected{
        void OnConnected();
    }

    public interface OnEventStart {
        void OnStartedItem(IVideo videoItem);
    }

    public interface OnEventPlaying {
        void OnPlayingItem();
    }

    public interface OnEventStop {
        void OnStopedItem();
    }

    public interface OnEventError {
        void OnErrorItem();
    }

    public interface OnEventChangeSize {
        void OnEventChangeSize(int width, int height);
    }

    public interface OnEventComplete {
        void OnEventComplete();
    }


    public EPlayerView2(Context context) {
        super(context);
        if(!isInEditMode()) {
            init(context, null);
        }
    }

    public EPlayerView2(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    public EPlayerView2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EPlayerView2(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    private void init(Context context, AttributeSet attrs) {
        ratioVideo = (float) 0.5625;
        this.context = context;
        setGravity(Gravity.CENTER);
        setBackgroundColor(getResources().getColor(android.R.color.black));

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(controlImpl != null){
                    if(((ViewGroup) controlImpl).getVisibility() != VISIBLE) {
                        controlImpl.setVisible(true);
                        mHandler.removeMessages(MSG_SHOW_CONTROL);
                        mHandler.sendEmptyMessageDelayed(MSG_SHOW_CONTROL, MSG_SHOW_CONTROL_TIMEOUT);
                    }else {

                        controlImpl.setVisible(false);

                    }
                }


                if(controlView != null){

                    if(controlView.getVisibility() != VISIBLE){
                        controlView.setVisibility(VISIBLE);
                        mHandler.removeMessages(MSG_SHOW_CONTROL);
                        mHandler.sendEmptyMessageDelayed(MSG_SHOW_CONTROL, MSG_SHOW_CONTROL_TIMEOUT);
                    }else {
                        controlView.setVisibility(GONE);
                    }

                }
            }
        });
    }



    public void setTypePlayer(int type){

        this.type = type;
        RelativeLayout.LayoutParams rlParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        rlParams.addRule(CENTER_IN_PARENT);

        if(type == TYPE_DEFAULT_PLAYER) {
            corePlayer = new MediaCorePlayer(context);
        }else if (type == TYPE_VLC_PLAYER){
            corePlayer = new VlcCorePlayer(context);
        }else if (type == TYPE_EXO_PLAYER){
            corePlayer = new ExoCorePlayer(context);
        }else if (type == TYPE_EXO_PLAYER2){
            corePlayer = new ExoCorePlayer2(context);
        }

        corePlayer.setLayoutParams(rlParams);
        addView(corePlayer);


        addOnLayoutChangeListener(new OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {

                widthScreen = right;
                heightScreen = (int) (widthScreen * ratioVideo);
                scaleVideo(widthScreen, heightScreen);

                removeOnLayoutChangeListener(this);
                if(onEventChangeSize != null){
                    onEventChangeSize.OnEventChangeSize(widthScreen, heightScreen);
                }
            }
        });

        corePlayer.setOnEventListener(this);

    }


    @Override
    public void OnPrepared() {
        if(onEventPrepared != null){
            onEventPrepared.OnPrepared();
        }
    }

    @Override
    public void OnStartedItem(IVideo videoItem) {

        if (onEventStart != null){
            onEventStart.OnStartedItem(videoItem);
        }
    }

    @Override
    public void OnPlayingItem() {

        if(controlImpl != null) {
            controlImpl.seekBar().setMax((int) (corePlayer.getLength() / 1000));
            mHandler.removeMessages(MSG_UPDATE_CONTROL);
            mHandler.sendEmptyMessage(MSG_UPDATE_CONTROL);
        }
        if(onEventPlaying != null){
            onEventPlaying.OnPlayingItem();
        }

    }

    @Override
    public void OnStopedItem() {

        if(onEventStop != null){
            onEventStop.OnStopedItem();
        }

    }

    @Override
    public void OnErrorItem() {


        if(onEventError != null){
            onEventError.OnErrorItem();
        }

    }

    @Override
    public void OnEventChangeSize(int width, int height) {

        if(width * height == 0){
            return;
        }
        scaleVideo(width, height);
        if(onEventChangeSize != null){
            onEventChangeSize.OnEventChangeSize(width, height);
        }



    }

    public void scaleVideoFull(int width, int height) {

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) corePlayer.getLayoutParams();
        params.height = height;
        params.width = width;
        invalidate();
    }
    public void scaleVideo(int width, int height){

        if(width * height == 0){
            return;
        }


        widthScreen = width;
        heightScreen = height;
//        if(widthScreen >= heightScreen) {
//
//            widthVideo = widthScreen;
//            heightVideo =  (widthVideo * height / width);
//
//        }else {
//            heightVideo = heightScreen;
//            widthVideo =  (heightVideo * height / width);
//
//        }
//        widthVideo = widthScreen;
//        heightVideo =  (widthVideo * height / width);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) corePlayer.getLayoutParams();
        params.height = (int) (width * ratioVideo);
        params.width = width;

//        RelativeLayout.LayoutParams paramsEP = (RelativeLayout.LayoutParams)getLayoutParams();
//        paramsEP.height = height;
//        paramsEP.width = width;
        invalidate();
    }

    @Override
    public void OnEventComplete() {

        if(onEventComplete != null){
            onEventComplete.OnEventComplete();
        }

    }




    public void setTitle(String title){

        if(controlImpl != null){
            ((TextView) controlImpl.viewTitle()).setText(title);
        }
    }

    private void updateControl(){

        ((TextView) controlImpl.viewTimeCurrent()).setText(Util.formatTime(corePlayer.getCurrent() / 1000));
        ((TextView) controlImpl.viewTimeDuration()).setText(Util.formatTime(corePlayer.getLength() / 1000));
        controlImpl.seekBar().setProgress((int) (corePlayer.getCurrent() / 1000));
    }
    public void addControl(IControl defaultControl){

        controlImpl = defaultControl;
        RelativeLayout.LayoutParams rlParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rlParams.addRule(ALIGN_PARENT_BOTTOM);
        ((ViewGroup) controlImpl).setLayoutParams(rlParams);
        addView((ViewGroup) controlImpl);

        controlImpl.controlPlay().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isPause) {
                    corePlayer.replay();
                    ((ImageView) controlImpl.controlPlay()).setImageResource(controlImpl.getResPause());
                }else {
                    corePlayer.pause();
                    ((ImageView) controlImpl.controlPlay()).setImageResource(controlImpl.getResPlay());

                }
                isPause = !isPause;

            }
        });

        controlImpl.seekBar().setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                ((TextView) controlImpl.viewTimeCurrent()).setText(Util.formatTime(seekBar.getProgress() ));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                mHandler.removeMessages(MSG_UPDATE_CONTROL);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                int time = seekBar.getProgress();
                corePlayer.seek(time * 1000);
                mHandler.sendEmptyMessage(MSG_UPDATE_CONTROL);


            }
        });

        mHandler.sendEmptyMessageDelayed(MSG_SHOW_CONTROL, MSG_SHOW_CONTROL_TIMEOUT);



    }


    public void addViewControl(View controlView){

        this.controlView = controlView;
        addView(controlView);
        mHandler.sendEmptyMessageDelayed(MSG_SHOW_CONTROL, MSG_SHOW_CONTROL_TIMEOUT);

    }


    public void start() {

        corePlayer.play();

    }

    public void resume(){

        corePlayer.replay();

    }

    public void stop() {

        corePlayer.stop();

    }

    public void pause() {
        corePlayer.pause();

    }

    public void loadMedia(@NonNull final List<? extends IVideo> videos) {

        corePlayer.setData(videos);

    }

    public void setVideoPath(@NonNull final String ... path){

        corePlayer.setMultiSource(path);

    }

    public void setMediaDataSource(MediaSource mediaDataSource){

        corePlayer.setMediaSource(mediaDataSource);
    }
    public void setVideoPath(@NonNull final String path){

        corePlayer.setVideoPath(path);

    }



    public void loadMedia(@NonNull final VideoItem video) {

        corePlayer.setData(video);

    }

    public void next() {

        corePlayer.next();

    }

    public void prev() {

        corePlayer.prev();

    }

    public void rewind() {

        corePlayer.rewind();

    }

    public void fastforward() {

        corePlayer.fastforward();

    }

    public boolean isPlaying() {
        return corePlayer.isPlaying();
    }

    public long getLength() {
        return corePlayer.getLength();
    }


    public void seek(long milisecond) {
        corePlayer.seek(milisecond);
    }

    public long getCurrent() {
        return corePlayer.getCurrent();
    }


    private Handler mHandler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what){
                case MSG_UPDATE:

                    long current= corePlayer.getCurrent();
                    mSubtitle.findSubTitle((int) current);
                    mHandler.sendEmptyMessageDelayed(MSG_UPDATE,1000);
                    return  true;

                case MSG_UPDATE_CONTROL:
                    if(controlImpl != null){
                        updateControl();
                        mHandler.removeMessages(MSG_UPDATE_CONTROL);
                        mHandler.sendEmptyMessageDelayed(MSG_UPDATE_CONTROL, MSG_UPDATE_CONTROL_TIMEOUT);
                    }

                    return true;

                case MSG_SHOW_CONTROL:

                    if(controlImpl != null){

                        controlImpl.setVisible(false);

                    }
                    if(controlView != null){

                        controlView.setVisibility(GONE);

                    }
                    return true;





            }
            return false;
        }
    });


//    public int getWidthVideo() {
//        return widthVideo;
//    }
//
//    public int getHeightVideo() {
//        return heightVideo;
//    }

    public float getRatioVideo(){
        return ratioVideo;
    }


    public void nextTrackAudio(){
        corePlayer.nextTrackAudio();
    }

}
