package elcom.nna.ehotel.library.player.dvplayer;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ann on 4/1/16.
 */
public class Video implements Parcelable {
    public final static int FILM = 1;
    public final static int TV = 2;


//    {url: url, subtitle: url, title: title}

    public String url;
    public String subtitle;
    public String title;
    public int type;
    public boolean isPlay;

    protected Video(Parcel in) {
        url = in.readString();
        subtitle = in.readString();
        title = in.readString();
        type = in.readInt();
        isPlay = in.readByte() != 0;
    }

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };

    public boolean isPlay() {
        return isPlay;
    }

    public void setPlay(boolean play) {
        isPlay = play;
    }


    public Video(){

    }

    public Video(String json){

        try {
            JSONObject joVideo = new JSONObject(json);
            if(joVideo.has("subtitle")){
                type = FILM;
                subtitle = joVideo.getString("subtitle");
            }else{
                type = TV;
                subtitle = null;
            }

            url = joVideo.getString("url");
            title = joVideo.getString("title");

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
    public boolean hasTV(){

        return type == TV ;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(subtitle);
        dest.writeString(title);
        dest.writeInt(type);
        dest.writeByte((byte) (isPlay ? 1 : 0));
    }
}
