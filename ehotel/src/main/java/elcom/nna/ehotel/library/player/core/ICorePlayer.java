package elcom.nna.ehotel.library.player.core;

import android.support.annotation.NonNull;

import java.util.List;

import elcom.nna.ehotel.library.player.model.IVideo;

/**
 * Created by nna on 24/05/2016.
 */
public interface ICorePlayer {

    int TIMESEEK = 30*1000;


    void replay();

    void play();

    void stop();

    void pause();

    void setData(@NonNull List<? extends IVideo> videos);

    void setData(@NonNull IVideo video);

    void setVideoPath(String url);

    void next();

    void prev();

    boolean isPlaying();

    void rewind();

    void fastforward();

    void seek(long milisecond);

    long getCurrent();

    long getLength();

    void setOnEventListener(OnEventListener mOnEventListener);

    interface OnEventListener {

        void OnPrepared();

        void OnStartedItem(IVideo videoItem);

        void OnPlayingItem();

        void OnStopedItem();

        void OnErrorItem();

        void OnEventChangeSize(int width, int height);

        void OnEventComplete();

    }


}
