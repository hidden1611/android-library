package elcom.nna.ehotel.library.player.core;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaRouter;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceView;
import android.view.View;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;
import org.videolan.libvlc.util.AndroidUtil;
import org.videolan.vlc.PlaybackService;
import org.videolan.vlc.VLCApplication;
import org.videolan.vlc.gui.PlaybackServiceActivity;
import org.videolan.vlc.media.MediaWrapper;
import org.videolan.vlc.util.VLCInstance;

import java.util.ArrayList;
import java.util.List;

import elcom.nna.ehotel.library.player.model.IVideo;
import elcom.nna.ehotel.library.player.model.VideoItem;

/**
 * Created by nna on 24/05/2016.
 */
public class VlcVideoView extends SurfaceView implements PlaybackService.Callback,
        IVLCVout.Callback,
        ICorePlayer,
        PlaybackService.Client.Callback{

    private static final String TAG = "VlcVideoView";
    private PlaybackService mService;
    private final PlaybackServiceActivity.Helper mHelper = new PlaybackServiceActivity.Helper(getContext(), this);
    private MediaRouter mMediaRouter;
    private MediaRouter.SimpleCallback mMediaRouterCallback;
    private boolean mPlaybackStarted;
    private boolean mSurfacesAttached;

    int index;
    private List<IVideo> videos;
    private OnEventListener mOnEventListtener;

    public VlcVideoView(Context context) {
        super(context);
        init(context, null);
    }

    public VlcVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public VlcVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public VlcVideoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    public boolean isConnected(){
        return mService != null;
    }

    private void init(Context context, AttributeSet attrs){

        VLCApplication.context = context;
        if (!VLCInstance.testCompatibleCPU(getContext())) {
            return;
        }

        if (AndroidUtil.isJellyBeanMR1OrLater()) {
            // Get the media router service (Miracast)
            mMediaRouter = (MediaRouter) VLCApplication.getAppContext().getSystemService(Context.MEDIA_ROUTER_SERVICE);
            mMediaRouterCallback = new MediaRouter.SimpleCallback() {
                @Override
                public void onRoutePresentationDisplayChanged(
                        MediaRouter router, MediaRouter.RouteInfo info) {
                    Log.d(TAG, "onRoutePresentationDisplayChanged: info=" + info);
                    final Display presentationDisplay = info.getPresentationDisplay();
                    final int newDisplayId = presentationDisplay != null ? presentationDisplay.getDisplayId() : -1;
                }
            };
            Log.d(TAG, "MediaRouter information : " + mMediaRouter  .toString());
        }

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopPlayback();
        if (mService != null)
            mService.removeCallback(this);
        mHelper.onStop();
    }


    private static LibVLC LibVLC() {
        return VLCInstance.get();
    }

    private void startPlayback(){

        LibVLC().setOnHardwareAccelerationError(new LibVLC.HardwareAccelerationError() {
            @Override
            public void eventHardwareAccelerationError() {

            }
        });

        final IVLCVout vlcVout = mService.getVLCVout();
        vlcVout.detachViews();
        vlcVout.setVideoView(this);
        mediaRouterAddCallback(false);
        vlcVout.addCallback(this);
        vlcVout.attachViews();
        mPlaybackStarted = true;
        mSurfacesAttached = true;


    }

    private void stopPlayback() {

        if (!mPlaybackStarted)
            return;
        LibVLC().setOnHardwareAccelerationError(null);

        mPlaybackStarted = false;

        mService.removeCallback(this);
        final IVLCVout vlcVout = mService.getVLCVout();
        vlcVout.removeCallback(this);
        if (mSurfacesAttached)
            vlcVout.detachViews();

        /* Stop listening for changes to media routes. */
        if (mMediaRouter != null)
            mediaRouterAddCallback(false);

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void mediaRouterAddCallback(boolean add) {
        if(!AndroidUtil.isJellyBeanMR1OrLater() || mMediaRouter == null) return;
        if(add)
            mMediaRouter.addCallback(MediaRouter.ROUTE_TYPE_LIVE_VIDEO, mMediaRouterCallback);
        else
            mMediaRouter.removeCallback(mMediaRouterCallback);
    }

    private void log(String ss){
        Log.d(TAG, "nna " + ss);
    }

    @Override
    public void update() {

    }

    @Override
    public void updateProgress() {

    }

    @Override
    public void onMediaEvent(Media.Event event) {

    }

    @Override
    public void onMediaPlayerEvent(MediaPlayer.Event event) {
        switch (event.type){
            case MediaPlayer.Event.Playing:
                mOnEventListtener.OnPlayingItem();
                break;
            case MediaPlayer.Event.Paused:
                break;
            case MediaPlayer.Event.Stopped:
                mOnEventListtener.OnStopedItem();
                break;

            case MediaPlayer.Event.EncounteredError:
                mOnEventListtener.OnErrorItem();
                break;
            case MediaPlayer.Event.EndReached:
                mOnEventListtener.OnEventComplete();
                break;
        }

    }

    @Override
    public void onDisconnected() {


    }

    @Override
    public void onConnected(PlaybackService service) {
        mService = service;
        startPlayback();
        setData();
    }

    /*********************************************
    *************IVLCVout Callback****************
    **********************************************/
    @Override
    public void onNewLayout(IVLCVout vlcVout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {
        if (width * height == 0)
            return;
        mOnEventListtener.OnEventChangeSize(width, height);

    }

    @Override
    public void onSurfacesCreated(IVLCVout vlcVout) {

    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vlcVout) {

    }



    @Override
    public void replay() {

        if(!mService.isPlaying()){
            mService.play();
        }

        if(getVisibility() == View.GONE){
            setVisibility(VISIBLE);
        }

    }

    /*
            Core Player
             */
    @Override
    public void play() {
//        mService.playIndex(index, MediaWrapper.MEDIA_VIDEO | MediaWrapper.MEDIA_NO_HWACCEL);
        mService.playIndex(index, MediaWrapper.MEDIA_VIDEO );
        mOnEventListtener.OnStartedItem(videos.get(index));

        if(getVisibility() == View.GONE){
            setVisibility(VISIBLE);
        }

    }

    @Override
    public void stop() {

        stopPlayback();
        if (mService != null)
            mService.removeCallback(this);

        mHelper.onStop();

        if(getVisibility() == View.VISIBLE){
            setVisibility(GONE);
        }

    }

    @Override
    public void pause() {

        mService.play();
        mService.pause();

    }

    private void setData(){
        List<MediaWrapper> mediaWrappers = new ArrayList<>();
        index = 0;
        for (int i = 0; i < videos.size(); i++) {

            IVideo video = videos.get(i);
            MediaWrapper mw = new MediaWrapper(Uri.parse(video.getUrl()));
            mw.setTitle(video.getSubtitle());
            mw.removeFlags(MediaWrapper.MEDIA_FORCE_AUDIO);
            mw.addFlags(MediaWrapper.MEDIA_VIDEO);
            mw.addFlags(MediaWrapper.MEDIA_NO_HWACCEL);
            mw.setmSubtitleURL(video.getSubtitle());
            if(video.isPlaying()){
                index = i;
            }

            mediaWrappers.add(mw);
        }

        mService.addCallback(this);
        mService.load(mediaWrappers);

        play();

    }
    @Override
    public void setData(List<? extends IVideo> videos) {

        this.videos = (List<IVideo>) videos;
        if(mService == null){

            mHelper.onStart();

        }else {

            setData();

        }

    }

    @Override
    public void setData(IVideo video) {

        List<IVideo> videoItems = new ArrayList<>();
        videoItems.add(video);
        setData(videoItems);
        index = 0;

    }

    @Override
    public void setVideoPath(String url) {

        VideoItem videoItem =new VideoItem();
        videoItem.setUrl(url);
        videoItem.setName(url);

        setData(videoItem);



    }

    @Override
    public void next() {
        if(++index >= mService.sizeMedia() ){
            index = 0;
        }

        play();
    }

    @Override
    public void prev() {
        if(--index < 0 ){
            index = mService.sizeMedia() - 1;
        }
        play();

    }

    @Override
    public boolean isPlaying() {
        return mService.isPlaying();
    }

    @Override
    public void rewind() {

        mService.setTime(mService.getTime() - TIMESEEK);
    }

    @Override
    public void fastforward() {

        mService.setTime(mService.getTime() + TIMESEEK);

    }

    @Override
    public void seek(long milisecond) {

        mService.setTime(milisecond);

    }

    @Override
    public long getCurrent() {
        return mService.getTime();
    }

    @Override
    public long getLength() {
        return mService.getLength();
    }

    @Override
    public void setOnEventListener(OnEventListener mOnEventListener) {
        this.mOnEventListtener = mOnEventListener;
    }


    public PlaybackService getmService() {
        return mService;
    }

}
