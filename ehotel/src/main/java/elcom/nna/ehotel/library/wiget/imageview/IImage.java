package elcom.nna.ehotel.library.wiget.imageview;

/**
 * Created by Ann on 5/11/16.
 */
public interface IImage {


    String getName();

    String getUrl();

}
