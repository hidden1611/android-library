package elcom.nna.ehotel.library.mpview;

/**
 * Created by Ann on 5/3/16.
 */
public interface IMusicItem {

    String getName();

    String getUrl();

    boolean isPlaying();

    void setPlaying(boolean value);

}
