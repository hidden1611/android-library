package elcom.nna.ehotel.library.utils;

import android.content.Context;
import android.util.TypedValue;

/**
 * Created by Ann on 4/8/16.
 */
public class Dimen {
    public static float pixelsToSp(Context context, Float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px / scaledDensity;
    }

    public static float spTopixels(Context context, Float sp) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
        return px;
    }

    public static float dpTopixels(Context context, Float dip) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, context.getResources().getDisplayMetrics());
        return px;
    }

}
