package elcom.nna.ehotel.library.utils.ota;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ann on 4/5/16.
 */
public class Ota {

    String urlapk;
    int version;
    String message;
    String file;


    public Ota(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            urlapk = jsonObject.getString("link");
            message = jsonObject.getString("message");
            version = Integer.parseInt(jsonObject.getString("version"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getUrlapk() {
        return urlapk;
    }

    public void setUrlapk(String urlapk) {
        this.urlapk = urlapk;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
