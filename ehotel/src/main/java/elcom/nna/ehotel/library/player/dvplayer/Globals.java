package elcom.nna.ehotel.library.player.dvplayer;

import android.graphics.Color;
import android.os.Environment;

import java.io.File;


public class Globals {

	/* public final static String path = "/mnt/flash/"; */
	public final static String path = "/mnt/sdcard/";
	public final static int timeOverplay = 10000;
	public static String fileName;
	public static String fileTitle;
	public static String subtitlesize[];
	public static String subtitlepos[];
	public static String subtitlecol[];
	// Set this value to true if you're planning to render 3D using OpenGL - it
	// eats some GFX resources, so disabled for 2D
	// public static boolean NeedDepthBuffer = false;
	// public static int numberofImages = 4; //Random images to be shown for
	// audio file, being played
	// Used in AudioThread.
	// Use the minimum BufferSize of AudioTrack. from version 2.6, this value is
	// 0. previously it was 2.
	public static int AudioBufferConfig = 0;

	// public static String ApplicationName = "Dolphin Player Universal";
	public static String defaultSubtitleFont = null;
	public static String defaultSubtitleFile = "Not Used Font";


	public static String getFileName() {
		if (fileName == null) {
			return "";
		}
		return fileName;
	}

	public static final String supportedVideoFileFormats[] = { "mp4", "wmv",
			"avi", "mkv", "dv", "rm", "mpg", "mpeg", "flv", "divx", "swf",
			"dat", "h264", "h263", "h261", "3gp", "3gpp", "asf", "mov", "m4v",
			"ogv", "vob", "ts",
			"webm",
			// to verify below file formats - reference vlc
			"vro", "tts", "tod", "rmvb", "rec", "ps", "ogx", "ogm", "nuv",
			"nsv", "mxf", "mts", "mpv2", "mpeg1", "mpeg2", "mpeg4", "mpe",
			"mp4v", "mp2v", "mp2", "m2ts", "m2t", "m2v", "m1v", "amv", "3gp2"
	// "ttf"
	};

	public static final String supportedAudioFileFormats[] = { "mp3", "wma",
			"ogg", "mp2", "flac", "aac", "ac3", "amr", "pcm", "wav", "au",
			"aiff", "3g2",
			"m4a",
			"astream",
			// to verify below file formats - reference vlc
			"a52", "adt", "adts", "aif", "aifc", "aob", "ape", "awb", "dts",
			"cda", "it", "m4p", "mid", "mka", "mlp", "mod", "mp1", "mp2",
			"mpc", "oga", "oma", "rmi", "s3m", "spx", "tta", "voc", "vqf",
			"w64", "wv", "xa", "xm" };

	public static final String supportedFontFileType[] = { "ttf" };

	public static final String supportedImageFileFormats[] = { "gif", "bmp",
			"png", "jpg" };

	public static final String supportedAudioStreamFileFormats[] = { "astream" };

	public static final String supportedSubtileFileFormats[] = { "srt" };

	public static String supportedFileFormats[] = concat(
			supportedAudioFileFormats, supportedVideoFileFormats,
			supportedFontFileType, supportedSubtileFileFormats);

	public static final String PREFS_NAME = "BroovPrefsFileTypeTwo"; // user
																		// preference
																		// file
																		// name
	public static final String PREFS_SUBTITLE = "subtitle";
	public static final String PREFS_SUBTITLESIZE = "subtitlesize";
	public static final String PREFS_SUBTITLEPOS = "subtitlepos";
	public static final String PREFS_SUBTITLECOL = "subtitlecol";
	public static final String PREFS_SUBTITLEFONT = "subtitlefont";
	public static final String PREFS_SUBTITLEFILE = "subtitlefile";

	public static final int PLAY_ALL = 1;
	public static final int REPEAT_ONE = 2;
	public static final int REPEAT_ALL = 3;


	public static final int SUBTITLE_FONT_SMALL = 25; // 9
	public static final int SUBTITLE_FONT_MEDIUM = 35; // 11
	public static final int SUBTITLE_FONT_LARGE = 45; // 13

	public static final int SUBTITLE_COLOR_WHILE = 0;
	public static final int SUBTITLE_COLOR_BLUE = 3;
	public static final int SUBTITLE_COLOR_GREEN = 2;
	public static final int SUBTITLE_COLOR_RED = 1;
	public static final int SUBTITLE_COLOR_GREY = 4;


	/**
	 * This method is used to concat 2 string arrays to one
	 * 
	 * @param A
	 *            First string Array
	 * @param B
	 *            Second string Array
	 * @return Concatenated string Array of First and Second
	 */
	public static String[] concat(String[] A, String[] B, String[] C, String[] D) {
		String[] temp = new String[A.length + B.length + C.length + D.length];
		System.arraycopy(A, 0, temp, 0, A.length);
		System.arraycopy(B, 0, temp, A.length, B.length);
		System.arraycopy(C, 0, temp, A.length + B.length, C.length);
		System.arraycopy(D, 0, temp, A.length + B.length + C.length, D.length);
		return temp;
	}

	public static boolean dbHide = false; // Show/Hide hidden folders
	public static boolean dbSubtitle = true; // Show/Hide subtitles
	public static int dbColor = Color.WHITE;
	public static int dbSort = 2;
	public static int dbAudioLoop = Globals.PLAY_ALL;
	public static int dbVideoLoop = Globals.REPEAT_ONE;
	public static int dbSubtitleSize = 1;
	public static String dbSubtitleFont = getSdcardPath() + "/arial.ttf";
	// public static String dbDefaultHome = "/mnt/sdcard/";
	// public static String dbSubtitleFont = "/mnt/sdcard/"+"/broov.ttf";

	public static String dbSubtitleFile = "Not Selected Font";
	public static int dbSubtitlePos = 0;
	public static int dbSubtitleCol = 0;
	public static String dbLastOpenDir = getSdcardPath();

	public static void setShowSubTitle(boolean type) {
		Globals.dbSubtitle = type;
	}

	public static void setSubTitleSize(int value) {
		Globals.dbSubtitleSize = value;
	}

	public static void setSubTitlePos(int value) {
		Globals.dbSubtitlePos = value;
	}

	public static void setSubTitleCol(int value) {
		Globals.dbSubtitleCol = value;
	}


	public static void setSubTitleFont(String value) {
		System.out.println("setDefault Subtitle font value:" + value);
		if (value != null) {
			File file = new File(value);

			// if (value.contains("/")){
			if (file.exists()) {
				Globals.dbSubtitleFont = value;
			} else {
				Globals.dbSubtitleFont = defaultSubtitleFont; // "/sdcard";
			}
		}
	}

	public static void setSubTitleFile(String value) {
		System.out.println("setDefault Subtitle font value:" + value);
		if (value != null) {
			if (value.contains("http")) {
				Globals.dbSubtitleFile = value;
			} else {
				File file = new File(value);

				// if (value.contains("/")){
				if (file.exists()) {
					Globals.dbSubtitleFile = value;
				} else {
					Globals.dbSubtitleFile = defaultSubtitleFile; // "/sdcard";
				}
			}
		}
	}

	public static void setColor(int value) {
		Globals.dbColor = value;
	}

	public static String getSdcardPath() {
		String sdcardPath = Environment.getExternalStorageDirectory()
				.getAbsolutePath();
		// String sdcardPath = "/mnt/sdcard/";
		System.out.println("sdcardPath:" + sdcardPath);
		return sdcardPath;
	}

}
