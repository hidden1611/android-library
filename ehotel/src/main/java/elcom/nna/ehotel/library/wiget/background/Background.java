package elcom.nna.ehotel.library.wiget.background;



/**
 * Created by Ann on 3/10/16.
 */
public class Background {

    public String urlImage;
    boolean isUsing;

    public Background(){

    }
//    public Background(Element element) {
//        urlImage = element.select("url").text();
//
//
//        if(!urlImage.startsWith("http")){
//            urlImage = BackgroundView.host + urlImage;
//
//        }
//
//        isUsing = false;
//    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }


    public void setUsing(boolean using) {
        isUsing = using;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public boolean isUsing() {
        return isUsing;
    }
}
