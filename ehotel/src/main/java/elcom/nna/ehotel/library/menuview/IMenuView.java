package elcom.nna.ehotel.library.menuview;

import java.util.List;

import elcom.nna.ehotel.library.menuview.model.IMenu;

/**
 * Created by Ann on 4/29/16.
 */
public interface IMenuView {

    void setData(List<? extends IMenu> menus);

    IMenu getItem(int pos);

    List<? extends IMenu> getItems();

    void setOnEventListener(IOnEventListener onEventListener);

    void setSelection(int index);

    void toogleVisible();


}
