package elcom.nna.ehotel.library.SlideView;

/**
 * Created by baotruongtuan on 7/25/16.
 */
public class SlideViewConfig {

    public static int WIDTH = 0;
    public static int HEIGHT = 300;

    public static int ITEM_WIDTH = 120;
    public static int TRANSFORM_ITEM_WIDTH = 140;

    public static float RATIO_TEXTSIZE = (float)1.2;

    public static int getWIDTH() {
        return WIDTH;
    }

    public static void setWIDTH(int WIDTH) {
        SlideViewConfig.WIDTH = WIDTH;
    }

    public static float getRatioTextsize() {
        return RATIO_TEXTSIZE;
    }

    public static void setRatioTextsize(float ratioTextsize) {
        RATIO_TEXTSIZE = ratioTextsize;
    }

    public static int getTransformItemWidth() {
        return TRANSFORM_ITEM_WIDTH;
    }

    public static void setTransformItemWidth(int transformItemWidth) {
        TRANSFORM_ITEM_WIDTH = transformItemWidth;
    }

    public static int getItemWidth() {
        return ITEM_WIDTH;
    }

    public static void setItemWidth(int itemWidth) {
        ITEM_WIDTH = itemWidth;
    }

    public static int getHEIGHT() {
        return HEIGHT;
    }

    public static void setHEIGHT(int HEIGHT) {
        SlideViewConfig.HEIGHT = HEIGHT;
    }
}
