package elcom.nna.ehotel.library.SlideView;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import elcom.nna.ehotel.library.R;


/**
 * Created by baotruongtuan on 7/25/16.
 */
public class SlideView extends RelativeLayout {

    private RecyclerView recyclerView;
    private SlideViewAdapter adapter;
    private List<ISlideViewItem> data = new ArrayList<ISlideViewItem>();
    private int currentPosition = 0;
    private LinearLayoutManager layoutManager;
    private Context context;
    private RecycleItemSelectedListener onItemSelectedListener;

    public SlideView(Context context, SlideViewConfig config) {
        super(context);
        this.context = context;
        SlideView_new();

    }

    public SlideView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        SlideView_new();
    }

    public SlideView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;

        SlideView_new();
    }

    public void SlideView_new() {
        setUpView();
    }

    private void setUpView() {
//        View view = LayoutInflater.from(context).inflate(R.layout.layout_slideview, null, false);

        View view = inflate(context, R.layout.layout_slideview, this);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);

        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                // Return false if scrolled to the bounds and allow focus to move off the list
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                        currentPosition++;
                        if (currentPosition > data.size() - 1) {
                            currentPosition = data.size() - 1;
                        }
                        adapter.setSelection(currentPosition);
                        layoutManager.smoothScrollToPosition(recyclerView, null, currentPosition);

                        return true;
                    } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {

                        currentPosition--;
                        if (currentPosition < 0) {
                            currentPosition = 0;
                        }
                        adapter.setSelection(currentPosition);
                        layoutManager.smoothScrollToPosition(recyclerView, null, currentPosition);

                        return true;
                    } else if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        View view = recyclerView.findViewHolderForAdapterPosition(currentPosition).itemView;
                        if ((event.getFlags() & KeyEvent.FLAG_LONG_PRESS) == KeyEvent.FLAG_LONG_PRESS) {
                            view.performLongClick();
                        } else {
                            if (onItemSelectedListener != null)
                                onItemSelectedListener.onItemSelectedListener(view, currentPosition);
                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public void setData(List<? extends ISlideViewItem> dataSet, RecycleItemSelectedListener onItemSelectedListener) {
        if (dataSet != null) {
            this.onItemSelectedListener = onItemSelectedListener;
            adapter = new SlideViewAdapter(dataSet, onItemSelectedListener);
            recyclerView.setAdapter(adapter);

        } else {
            Toast.makeText(context, "dataset null", Toast.LENGTH_LONG).show();
        }
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int position) {
        currentPosition = position;
        adapter.setSelection(currentPosition);
        layoutManager.smoothScrollToPosition(recyclerView, null, currentPosition);
    }

}
