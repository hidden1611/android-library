package elcom.nna.ehotel.library.menuview.horizontal;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import elcom.nna.ehotel.library.R;
import elcom.nna.ehotel.library.menuview.IOnEventListener;
import elcom.nna.ehotel.library.menuview.model.IMenu;

import static android.support.v7.widget.RecyclerView.Adapter;

/**
 * Created by Ann on 3/11/16.
 */
public class HorizontalAdapter extends Adapter<HorizontalAdapter.ServiceViewHolder> {

    private int colorBgText;
    private int colorText;
    private int width;
    private int height;

    private List<IMenu> items;

    public HorizontalAdapter() {
        this.items = new ArrayList<>();
    }

    public HorizontalAdapter(int width, int height, int colorBgText, int colorText){
        this.items = new ArrayList<>();
        this.width = width;
        this.height = height;

        this.colorBgText = colorBgText;
        this.colorText = colorText;
    }

    public void addItem(IMenu item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public void setData(List<? extends IMenu> items){
        this.items = (List<IMenu>) items;
        notifyDataSetChanged();
    }

    public IMenu getItem(int index){
        return items.get(index);
    }

    public List<IMenu> getItems(){
        return items;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }


    @Override
    public ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_service, parent, false);

        RecyclerView.LayoutParams pr = (RecyclerView.LayoutParams) v.getLayoutParams();
        pr.width = width;
        pr.height = height;

        return new ServiceViewHolder(v, width, height, colorBgText, colorText);
    }

    @Override
    public void onBindViewHolder(ServiceViewHolder holder, int position) {

        holder.setValue(items.get(position));
//        if(position == 1){
//
//            holder.enlarge(true);
//
//        }else {
//
//            holder.reduce(true);
//
//        }
//        holder.onBind();

    }

    @Override
    public int getItemCount() {

        return items.size();

    }


    public void setOnItemClickListener(IOnEventListener onItemClickListener) {
        this.onEventListener = onItemClickListener;
    }

    public static IOnEventListener onEventListener;



    public class ServiceViewHolder extends RecyclerView.ViewHolder {


        final static float scaleEnlarged = 1.2f;
        final static float scaleReduced = 1.0f;
        protected boolean enlarged = false;

        TextView tvRowServiceText;
        ImageView ivRowServiceImage;
        Animator currentAnimator;
        CardView cardView;

        public ServiceViewHolder(View itemView, int width, int height,  int bgText, int text) {
            super(itemView);
            cardView = (CardView)itemView.findViewById(R.id.cv);
            tvRowServiceText = (TextView)itemView.findViewById(R.id.tvRowServiceText);
            ivRowServiceImage = (ImageView)itemView.findViewById(R.id.ivRowServiceImage);
            tvRowServiceText.setBackgroundColor(bgText);
            tvRowServiceText.setTextColor(text);
            tvRowServiceText.getLayoutParams().width = width;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventListener != null) {
                        onEventListener.onClicked(v, getLayoutPosition());
                    }

                }
            });

            itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {

                    if(hasFocus ){
                        enlarge(true);
                        onEventListener.onSelected(v, getLayoutPosition());
                    }else {
                        reduce(true);
                    }


                }
            });
        }

        public void setValue(IMenu item) {
            tvRowServiceText.setText(Html.fromHtml(item.getName()));
            Glide.with(itemView.getContext())
                    .load(item.getUrlIcon())
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .crossFade()
                    .into(ivRowServiceImage);

        }

        public void enlarge(boolean withAnimation) {
            if (!enlarged ) {

                if(currentAnimator != null) {
                    currentAnimator.cancel();
                    currentAnimator = null;
                }

                int duration = withAnimation? 300 : 0;

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.setDuration(duration);
                List<Animator> animatorList = new ArrayList<>();
                animatorList.add(ObjectAnimator.ofFloat(cardView, "scaleX", scaleEnlarged));
                animatorList.add(ObjectAnimator.ofFloat(cardView, "scaleY", scaleEnlarged));

                //animatorList.add(ObjectAnimator.ofFloat(cardView, "translationX", translationX));
                animatorSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        cardView.setCardElevation(8);
                        currentAnimator = null;
                    }
                });

                animatorSet.playTogether(animatorList);
                currentAnimator = animatorSet;
                animatorSet.start();

                enlarged = true;
            }
        }

        public void reduce(boolean withAnimation) {
            if (enlarged ) {
                if(currentAnimator != null) {
                    currentAnimator.cancel();
                    currentAnimator = null;
                }

                int duration = withAnimation? 300 : 0;

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.setDuration(duration);

                List<Animator> animatorList = new ArrayList<>();
                animatorList.add(ObjectAnimator.ofFloat(cardView,"scaleX",scaleReduced));
                animatorList.add(ObjectAnimator.ofFloat(cardView,"scaleY",scaleReduced));

                //animatorList.add(ObjectAnimator.ofFloat(cardView, "translationX", translationX));
                animatorSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        cardView.setCardElevation(5);
                        currentAnimator = null;
                    }
                });

                animatorSet.playTogether(animatorList);
                currentAnimator = animatorSet;
                animatorSet.start();

                enlarged = false;

            }
        }

        public void newPosition(int position) {
            if (position == 1)
                enlarge(true);
            else
                reduce(true);
        }


//    public void onBind() {
//        int cell = getAdapterPosition();
//        viewHolder.cell = cell;
//        adapter.onBindViewHolder(viewHolder,cell);
//    }

    }






}



