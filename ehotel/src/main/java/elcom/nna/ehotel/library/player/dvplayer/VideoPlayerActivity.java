package elcom.nna.ehotel.library.player.dvplayer;


import android.app.Activity;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import elcom.nna.ehotel.library.R;

public class VideoPlayerActivity extends Activity implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, SurfaceHolder.Callback, SeekBar.OnSeekBarChangeListener{

    private static final int MAXSEEKBAR = 100;

//    @Bind(R.id.duration)
    TextView mTextDuration;
//    @Bind(R.id.total)
    TextView mTextTotalDuratio;
//    @Bind(R.id.butPlaypause)
    ImageButton btPlayPause;
//    @Bind(R.id.seekbar)
    SeekBar mSeekBar;
//    @Bind(R.id.layoutControler)
    RelativeLayout layoutControler;
//    @Bind(R.id.imageMusic)
    SurfaceView vidSurface;
//    @Bind(R.id.nameMovie)
    TextView mTitleMovie;
//    @Bind(R.id.layoutprogress)
    LinearLayout processbar;

    private MediaPlayer mediaPlayer;
    private boolean bSeeking;
    private Handler mHandler = new Handler();
    private Updater seekBarUpdate;
    private UpdaterController updaterController;
    private boolean isPause;
    private SurfaceHolder vidHolder;
    private int mVideoWidth;
    private int mVideoHeight;
    int position;

    private Video video;
    private ImageButton btSubtitle;
    private LinearLayout mLinearSubtitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.videoplayer);
        mTextDuration = (TextView) findViewById(R.id.duration);
        mTextTotalDuratio = (TextView) findViewById(R.id.total);
         btPlayPause = (ImageButton) findViewById(R.id.butPlaypause);
        btSubtitle = (ImageButton) findViewById(R.id.btSubtitle);
        mSeekBar = (SeekBar)findViewById(R.id.seekbar);
        layoutControler = (RelativeLayout) findViewById(R.id.layoutControler);
        vidSurface = (SurfaceView) findViewById(R.id.imageMusic);
        mTitleMovie = (TextView) findViewById(R.id.nameMovie);
        processbar = (LinearLayout) findViewById(R.id.layoutprogress);
        mLinearSubtitle = (LinearLayout) findViewById(R.id.lineSubtitle);

        btSubtitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toogleSubtitleSetting();

            }
        });
        btPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!isPause) {
                        btPlayPause.setBackgroundResource(R.drawable.btt_play_normal);
                        mediaPlayer.pause();
                        isPause = true;
                    } else {
                        btPlayPause.setBackgroundResource(R.drawable.btt_pause_normal);
                        mediaPlayer.start();
                        isPause = false;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


        mSeekBar.setOnSeekBarChangeListener(this);
        updaterController = new UpdaterController();
        mHandler.post(updaterController);
        if(getIntent().hasExtra("BUDDLE_VIDEO")){
            video = getIntent().getParcelableExtra("BUDDLE_VIDEO");
        }else{
            video = new Video();
            video.setUrl("http://172.16.9.141/vod_hotel/mp4/1563.mp4");
            video.setTitle("Test");
        }

        if(video.hasTV()){
            mSeekBar.setVisibility(View.GONE);
            mTextDuration.setVisibility(View.GONE);
            mTextTotalDuratio.setVisibility(View.GONE);
        }

        vidSurface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toogleControler();

            }
        });
        vidHolder = vidSurface.getHolder();
        vidHolder.addCallback(this);
        processbar.setVisibility(View.VISIBLE);

    }

//    @OnClick(R.id.butPlaypause)
    void OnClickPlayPause(){
        try {
            if (!isPause) {
                btPlayPause.setBackgroundResource(R.drawable.btt_play_normal);
                mediaPlayer.pause();
                isPause = true;
            } else {
                btPlayPause.setBackgroundResource(R.drawable.btt_pause_normal);
                mediaPlayer.start();
                isPause = false;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
//    @OnClick(R.id.btSubtitle)
    void OnClickSubtitle(){

    }


    private void toogleSubtitleSetting(){

        if (mLinearSubtitle.getVisibility() == View.VISIBLE) {
            mLinearSubtitle.setVisibility(View.GONE);
            btSubtitle.setBackgroundResource(R.drawable.bg_subtitle);
        } else {
            btSubtitle .setBackgroundResource(R.drawable.button_subtitle_focus);
//            mOvetPlaySub.showInfo(Globals.timeOverplay);
        }
    }
    private void toogleControler(){
        if(layoutControler.getVisibility() == View.VISIBLE){
            layoutControler.setVisibility(View.GONE);
            btPlayPause.setVisibility(View.GONE);

        }else if(layoutControler.getVisibility() == View.GONE){
            layoutControler.setVisibility(View.VISIBLE);
            btPlayPause.setVisibility(View.VISIBLE);
        }

        if(updaterController != null) {
            updaterController.stopIt();
        }
        updaterController = new UpdaterController();
        mHandler.post(updaterController);
    }

    private class UpdaterController implements Runnable {
        private boolean stop;
        private int count = 0;
        public void stopIt() {
            stop = true;
        }
        @Override
        public void run() {
            if(!stop) {
                if(++count >= 7){
                    layoutControler.setVisibility(View.GONE);
                    btPlayPause.setVisibility(View.GONE);
                }else{
                    mHandler.postDelayed(this, 1000);
                }
            }
        }
    }

    private class Updater implements Runnable {
        private boolean stop;
        public void stopIt() {
            stop = true;
        }
        @Override
        public void run() {
            if(!bSeeking){
                if(!stop) {
                    mHandler.postDelayed(this, 1000);

                    try {
                        if(mediaPlayer == null) return;

                        mTextDuration.setText(Util.formatTime(mediaPlayer.getCurrentPosition() / 1000));
                        mTextTotalDuratio.setText(Util.formatTime(mediaPlayer.getDuration() / 1000));

                        if(mediaPlayer.getDuration() != 0) {
                            int progress = ((MAXSEEKBAR * mediaPlayer.getCurrentPosition()) / mediaPlayer.getDuration());
                            mSeekBar.setProgress(progress);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(video.getUrl());
                    mediaPlayer.setDisplay(vidHolder);
                    mediaPlayer.setOnPreparedListener(VideoPlayerActivity.this);
                    mediaPlayer.prepare();
                    if (seekBarUpdate != null) {
                        seekBarUpdate.stopIt();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                try {
                    mTitleMovie.setText(video.getTitle());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        changeSurfaceSize(mVideoWidth, mVideoHeight);

    }

    @Override
    public void onResume() {
        super.onResume();
        try{
            if(mediaPlayer != null && !mediaPlayer.isPlaying()) {
                mediaPlayer.start();
                isPause = false;
                seekBarUpdate = new Updater();
                mHandler.post(seekBarUpdate);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopPlayer();
    }
    private void stopPlayer(){
        try{
            mediaPlayer.pause();
            isPause = true;
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            seekBarUpdate.stopIt();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void pausePlayser(){
        try {
            mediaPlayer.pause();
            isPause = true;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        pausePlayser();
    }

    @Override
    public void onPause() {
        super.onPause();
        pausePlayser();

    }

    private static final String TAG = "VideoPlayerActivity";
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown " + keyCode);
        if(keyCode == KeyEvent.KEYCODE_DEL) {
            onBackPressed();
            return true;
        }else if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_ENTER){
            toogleControler();
            return true;

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        processbar.setVisibility(View.GONE);
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        processbar.setVisibility(View.GONE);
        mediaPlayer.start();
        if (seekBarUpdate != null) {
            seekBarUpdate.stopIt();
        }
        if(position != 0){
            mediaPlayer.seekTo(position);
            position = 0;
        }
        seekBarUpdate = new Updater();
        mHandler.post(seekBarUpdate);

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if(b) {
            try {
                if(isPause || mediaPlayer.isPlaying()){
                    int currentSecsMoved = mediaPlayer.getDuration() * i / MAXSEEKBAR;
                    mTextDuration.setText(Util.formatTime(currentSecsMoved / 1000));
                    mTextTotalDuratio.setText(Util.formatTime(mediaPlayer.getDuration() / 1000));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        try {
            if (isPause || mediaPlayer.isPlaying()) {
                bSeeking = false;
                int progress = seekBar.getProgress();
                mediaPlayer.seekTo(progress * mediaPlayer.getDuration() / MAXSEEKBAR);
                seekBarUpdate.stopIt();
                seekBarUpdate = new Updater();
                mHandler.post(seekBarUpdate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void changeSurfaceSize(int mVideoWidth, int mVideoHeight) {
        // get screen size
        int dw = getWindow().getDecorView().getWidth();
        int dh = getWindow().getDecorView().getHeight();
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;

        if (dw > dh && isPortrait || dw < dh && !isPortrait) {
            int d = dw;
            dw = dh;
            dh = d;
        }
        if (dw * dh == 0)
            return;
//        // compute the aspect ratio
        double ar;
        if(isPortrait) {
            double dar = (double) dw / (double) dh;
            ar = 16.0 / 9.0;
            if (dar < ar)
                dh = (int) (dw / ar);
            else
                dw = (int) (dh * ar);
        }
        vidHolder.setFixedSize(mVideoWidth, mVideoHeight);
        android.view.ViewGroup.LayoutParams lp = vidSurface.getLayoutParams();
        lp.width = dw;
        lp.height = dh;
        vidSurface.setLayoutParams(lp);
        vidSurface.invalidate();
    }
}
