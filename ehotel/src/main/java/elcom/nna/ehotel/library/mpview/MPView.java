package elcom.nna.ehotel.library.mpview;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import elcom.nna.ehotel.library.R;

/**
 * Created by Ann on 5/3/16.
 */
public class MPView extends RelativeLayout implements SeekBar.OnSeekBarChangeListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    private static final int MAXSEEKBAR = 100;
    private List<IMusicItem> musicItems;
    public int index = 0;
    private MediaPlayer mediaPlayer;
    private TextView txtTitle;
    private TextView txtTime;
    private TextView txtDuration;
    private boolean bSeeking;
    private Handler mHandler = new Handler();
    private Updater seekBarUpdate;
    private SeekBar mcSeek;
    private ImageView imgPrevious;
    private ImageView imgPlay;
    private ImageView imgNext;
    private boolean paused = false;

    private IOnChangeItemListener onChangeItemListener;

    public MPView(Context context) {
        super(context);

        if(!isInEditMode()) {
            init(context, null);
        }
    }

    public MPView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    public MPView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MPView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        destroy();
    }

    private void init(Context context, AttributeSet attrs){

        View view = LayoutInflater.from(context).inflate(R.layout.mpview, this, false);
        RelativeLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(ALIGN_PARENT_BOTTOM);
        view.setLayoutParams(params);
        addView(view);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        txtTitle = (TextView)view.findViewById(R.id.txtTitle);
        txtTime = (TextView)view.findViewById(R.id.txtTime);
        txtDuration = (TextView)view.findViewById(R.id.txtDuraion);

        mcSeek = (SeekBar)view.findViewById(R.id.seekbar);
        imgPlay = (ImageView)view.findViewById(R.id.playpause);
        imgPrevious = (ImageView)view.findViewById(R.id.previous);
        imgNext = (ImageView)view.findViewById(R.id.next);
        mcSeek.setOnSeekBarChangeListener(this);

        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(paused) {
                    resume();
                    imgPlay.setImageResource(R.drawable.btn_pause);
                }else {
                    mediaPlayer.pause();
                    seekBarUpdate.stopIt();
                    imgPlay.setImageResource(R.drawable.btn_play);
                }
                paused = !paused;
            }
        });
        imgPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previous();
            }
        });
        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
    }

    public void setData (List<? extends IMusicItem> musicItems){
        this.musicItems = (List<IMusicItem>) musicItems;
        index = 0;
        start();
    }

    public void setOnChangeItemListener(IOnChangeItemListener onChangeItemListener){
        this.onChangeItemListener = onChangeItemListener;
    }

    public void resume(){
        mediaPlayer.start();
        seekBarUpdate.stopIt();
        seekBarUpdate = new Updater();
        mHandler.post(seekBarUpdate);
    }

    public void destroy(){
        try {
            stop();
            mediaPlayer.release();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void stop(){
        try {
            if(mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.reset();
            }
            if(seekBarUpdate != null)
                seekBarUpdate.stopIt();
        }catch (Exception e){
            e.printStackTrace();
        }
        setPlayinng(false);

    }
    public void start(){
        try {
            paused = false;
            imgPlay.setImageResource(R.drawable.btn_pause);
            txtTitle.setText(Html.fromHtml(musicItems.get(index).getName()));
            txtTime.setText("00:00");
            txtDuration.setText("00:00");
            mcSeek.setProgress(0);
            setPlayinng(true);
            mediaPlayer.setDataSource(musicItems.get(index).getUrl());
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnErrorListener(this);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                    if(seekBarUpdate != null)
                        seekBarUpdate.stopIt();
                    seekBarUpdate = new Updater();
                    mHandler.post(seekBarUpdate);
                }
            });

            if(onChangeItemListener != null){
                onChangeItemListener.onChanged(index);
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        next();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        next();
        return false;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(fromUser) {
            try {
                if(mediaPlayer.isPlaying()){
                    int currentSecsMoved = mediaPlayer.getDuration() * progress / MAXSEEKBAR;
                    txtTime.setText(formatTime(currentSecsMoved / 1000));
                    txtDuration.setText(formatTime(mediaPlayer.getDuration() / 1000));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        bSeeking = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        try {
            if(mediaPlayer.isPlaying()) {
                bSeeking = false;
                int progress = seekBar.getProgress();
                mediaPlayer.seekTo(progress * mediaPlayer.getDuration() / MAXSEEKBAR);
                seekBarUpdate.stopIt();
                seekBarUpdate = new Updater();
                mHandler.post(seekBarUpdate);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }



    private class Updater implements Runnable {
        private boolean stop;
        public void stopIt() {
            stop = true;
        }
        @Override
        public void run() {
            if(!bSeeking){
                if(!stop) {
                    mHandler.postDelayed(this, 1000);
                    try {
                        txtTime.setText(formatTime(mediaPlayer.getCurrentPosition() / 1000));
                        txtDuration.setText(formatTime(mediaPlayer.getDuration() / 1000));
                        int progress = ((MAXSEEKBAR * mediaPlayer.getCurrentPosition()) / mediaPlayer.getDuration());
                        mcSeek.setProgress(progress);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    private void setPlayinng(boolean value){

        if(musicItems != null && musicItems.size() > index) {
            musicItems.get(index).setPlaying(value);
        }
    }


    public List<? extends IMusicItem> getItems(){
        return musicItems;
    }

    public void playAtindex(int index){

        setPlayinng(false);
        this.index = index;
        stop();
        start();

    }

    public void next(){
        setPlayinng(false);
        if(++index >= musicItems.size()) index = 0;
        stop();
        start();
    }
    public void previous(){
        setPlayinng(false);
        if(--index < 0) index = musicItems.size() - 1;
        stop();
        start();

    }

    public static String formatTime(long seconds) {
        String output = "";
        //long seconds = millis / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        seconds = seconds % 60;
        minutes = minutes % 60;
        hours = hours % 24;

        String secondsD = String.valueOf(seconds);
        String minutesD = String.valueOf(minutes);
        String hoursD = String.valueOf(hours);

        if (seconds < 10)
            secondsD = "0" + seconds;
        if (minutes < 10)
            minutesD = "0" + minutes;
        if (hours < 10){
            hoursD = "0" + hours;
        }

        if( days > 0 ){
            output = days +"d ";
        }

        if(hours > 0) {
            output = hoursD + ":";
        }else {
            output = "";
        }
        //output += hoursD + ":" + minutesD + ":" + secondsD;
        output += minutesD + ":" + secondsD;

        return output;
    }

}
