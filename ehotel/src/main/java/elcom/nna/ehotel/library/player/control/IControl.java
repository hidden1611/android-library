package elcom.nna.ehotel.library.player.control;

import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by Ann on 11/14/16.
 */


public interface IControl {

    int getResPlay();

    int getResPause();


    View viewTitle();

    View viewTimeDuration();

    View viewTimeCurrent();

    SeekBar seekBar();

    View controlPlay();

    void setVisible(boolean show);




}
