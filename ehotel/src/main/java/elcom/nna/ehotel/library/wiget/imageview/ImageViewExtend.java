package elcom.nna.ehotel.library.wiget.imageview;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.List;

/**
 * Created by Ann on 5/11/16.
 * Support to display playlist
 */
public class ImageViewExtend extends ImageView{

    public static final int MSG_SLIDE = 1;

    private int mInterval = 10000;

    private List<IImage> images;
    private Bitmap bitmapReflection;
    private int index = 0;

    public ImageViewExtend(Context context) {
        super(context);
        init(context, null);
    }

    public ImageViewExtend(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ImageViewExtend(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ImageViewExtend(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mHandler.removeCallbacksAndMessages(null);
    }

    private void init(Context context, AttributeSet attrs){


    }
    public void setListImage(List<? extends IImage> images){

        this.images = (List<IImage>) images;
        start();
        
    }

    public IImage getItem(int index){
        return images.get(index);
    }

    public void setImageReflection(IImage image,final float widthReflection,final int reflectionGap,final int angle){
        int width = getLayoutParams().width;
        int height = getLayoutParams().height;
        Glide
                .with(getContext())
                .load(image.getUrl()).asBitmap()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .into(new SimpleTarget<Bitmap>(width, height) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        bitmapReflection = getReflectionBitmap(resource, widthReflection,reflectionGap,angle);
                        setImageBitmap(bitmapReflection); // Possibly runOnUiThread()
                    }
                });
    }
    
    private void start(){

        loadImage();
        if(images.size() >= 2) {
            mHandler.removeMessages(MSG_SLIDE);
            mHandler.sendEmptyMessageDelayed(MSG_SLIDE, mInterval);
        }
    }
    public void pause() {

        mHandler.removeMessages(MSG_SLIDE);

    }

    public void resume() {

        if(images.size() >= 2) {
            mHandler.sendEmptyMessageDelayed(MSG_SLIDE, mInterval);
        }

    }

    private void loadImage(){

        if(onChangeItem != null){
            onChangeItem.onChange(index);
        }
        Glide.with(getContext())
                .load(images.get(index).getUrl())
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.ALL )
                .centerCrop()
                .crossFade()
                .into(this);
    }

    private void nextImage(){

        index = ++index >= images.size() ? 0 : index;

        loadImage();

    }

    public void setEffect(int modeEffect) {

    }


    private Handler mHandler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what){
                case MSG_SLIDE:
                    nextImage();
                    mHandler.sendEmptyMessageDelayed(MSG_SLIDE, mInterval);
                    break;
            }
            return false;
        }
    });

    private Bitmap getReflectionBitmap(Bitmap mainImage, float widthReflection, int reflectionGap, int angle){
        //default angle: 15 degree
        mainImage = rotate3DBitmap(mainImage,angle);

        /*float delta = mainImage.getHeight() / 5;

        float topLeftX = 0;
        float topLeftY = delta;

        float topRightX = mainImage.getWidth();
        float topRightY = 0;

        float bottomLeftX = 0;
        float bottomLeftY = mainImage.getWidth() - delta;

        float bottomRightX = mainImage.getWidth();
        float bottomRightY = mainImage.getHeight();

        float[] mVerts = {
                topLeftX, topLeftY,
                topRightX, topRightY,
                bottomLeftX, bottomLeftY,
                bottomRightX, bottomRightY
        };

        canvas.drawBitmapMesh(mainImage, 1, 1, mVerts, 0, null, 0, null);*/

        // gap space between original and reflected



        Matrix matrixRotate = new Matrix();
        matrixRotate.postRotate(-90);
        mainImage = Bitmap.createBitmap(mainImage, 0, 0, mainImage.getWidth(), mainImage.getHeight(), matrixRotate, true);

        /*final int reflectionGap = 10;*/

        // get image size
        int width = mainImage.getWidth();
        int height = mainImage.getHeight();

        Matrix matrix = new Matrix();

        matrix.preScale(1, -1);

        /*Create Bitmap with height = 1/2 origin height bitmap*/
        /*Bitmap reflectionImage = Bitmap.createBitmap(mainImage, 0,
                height/2 , width, height / 2, matrix, false);

        Bitmap reflectedBitmap = Bitmap.createBitmap(width,
                 (height + (height /2 )), Bitmap.Config.ARGB_8888);*/

        Bitmap reflectionImage = Bitmap.createBitmap(mainImage, 0,
                0 , width, height, matrix, false);

        Bitmap reflectedBitmap = Bitmap.createBitmap(width,
                (int) (height + (height * widthReflection)), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(reflectedBitmap);
        canvas.drawBitmap(mainImage, 0, 0, null);
        Paint defaultPaint = new Paint();
        canvas.drawRect(0, height, width, height + reflectionGap, defaultPaint);
        canvas.drawBitmap(reflectionImage, 0, height + reflectionGap, null);
        Paint paint = new Paint();
        LinearGradient shader = new LinearGradient(0,
                mainImage.getHeight(), 0, reflectedBitmap.getHeight()
                + reflectionGap, 0x70ffffff, 0x00ffffff, Shader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0, height, width, reflectedBitmap.getHeight()
                + reflectionGap, paint);

        Matrix matrixReturn = new Matrix();
        matrixReturn.postRotate(90);
        reflectedBitmap = Bitmap.createBitmap(reflectedBitmap, 0, 0, reflectedBitmap.getWidth(), reflectedBitmap.getHeight(), matrixReturn, true);
        return reflectedBitmap;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }

    private Bitmap rotate3DBitmap(Bitmap bmp, int degress){
        if(degress % 90 == 0)

            degress+=1;

        Matrix matrix= new Matrix();
        System.out.println("martrix:" + matrix.toString());
        Camera camera=new Camera();
        camera.save();
//        camera.rotateX(degress);
        camera.rotateY(-degress);
//        camera.rotateZ(-degress);
        camera.getMatrix(matrix);
        camera.restore();
        System.out.println("martrix1:"+matrix.toString());

        /*ImageView img=(ImageView)Base3dImp.this.findViewById(R.id.iv_base_3d);*/
        int centerX = getWidth()/2;
        int centerY = getHeight()/2;

        Bitmap bmp1 = Bitmap.createBitmap(bmp,0,0,bmp.getWidth(),bmp.getHeight(),matrix,true);

        return bmp1;
    }

    /*
        * Just call when this image is not visible
        * */
    public void releaseBitmap(){
        try {
            bitmapReflection.recycle();
            bitmapReflection = null;
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public OnChangeItem onChangeItem;
    public void setOnChangeItem(OnChangeItem onChangeItem){
        this.onChangeItem = onChangeItem;
    }
    public interface OnChangeItem {
        void onChange(int position);
    }
}
