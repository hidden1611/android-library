package elcom.nna.ehotel.library.player.dvplayer;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.SeekBar;
import android.widget.TextView;

import elcom.nna.ehotel.library.R;

public class DialogDownloadSub extends Dialog {

	private SeekBar 	mSeekDownloadSub;
	private TextView	txtAlert;
	private TextView	txtPercentDownload;
	
	public DialogDownloadSub(Context context) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_download_sub);
		setCancelable(true);
		setUpView();
	}
	
	private void setUpView(){
		mSeekDownloadSub = (SeekBar) findViewById(R.id.playerSeekBarDownloading);
		txtPercentDownload = (TextView) findViewById(R.id.txtPercentDownloading);
		txtAlert = (TextView) findViewById(R.id.txtAlertDownloading);
	}
	
	public void setProgressSeekBarDownload(int progress){
		mSeekDownloadSub.setProgress(progress);
	}
	
	public void setPercentDownloading(String txtPercent){
		txtPercentDownload.setText(txtPercent);
	}
	
	public void setAler(String strAlert){
		txtAlert.setText(strAlert);
	}
	
	public void setMax(int max){
		mSeekDownloadSub.setMax(max);
	}
		

}
