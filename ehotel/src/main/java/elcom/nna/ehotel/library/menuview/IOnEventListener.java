package elcom.nna.ehotel.library.menuview;

import android.view.View;

/**
 * Created by Ann on 4/29/16.
 */
public interface IOnEventListener {

    void onClicked(View view, int pos);

    void onSelected(View view, int pos);

}
