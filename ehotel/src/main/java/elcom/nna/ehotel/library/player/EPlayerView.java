package elcom.nna.ehotel.library.player;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import elcom.nna.ehotel.library.player.control.IControl;
import elcom.nna.ehotel.library.player.core.DefaultVideoView;
import elcom.nna.ehotel.library.player.core.ICorePlayer;
import elcom.nna.ehotel.library.player.core.VlcVideoView;
import elcom.nna.ehotel.library.player.dvplayer.Subtitle;
import elcom.nna.ehotel.library.player.dvplayer.Util;
import elcom.nna.ehotel.library.player.model.IVideo;
import elcom.nna.ehotel.library.player.model.VideoItem;


/**
 * Created by Ann on 5/25/16.
 */
public class EPlayerView extends RelativeLayout implements  ICorePlayer.OnEventListener{

    private static final String TAG = "EPlayer nna";
    public final static int TYPE_DEFAULT_PLAYER = 0;
    public final static int YPE_VLC_PLAYER = 1;
    private static final int MSG_UPDATE = 1;
    private static final int MSG_UPDATE_CONTROL = 2;
    private static final int MSG_SHOW_CONTROL = 3;
    private static final int MSG_SHOW_CONTROL_TIMEOUT = 5000;
    private static final int MSG_UPDATE_CONTROL_TIMEOUT = 500;
    private int type;

    private Context context;
    ICorePlayer corePlayer;
    Subtitle mSubtitle;
    private int widthVideo;
    private int heightVideo;
    private IControl controlImpl;
    private OnEventConnected onEventConnected;
    private OnEventPrepared onEventPrepared;
    private OnEventStart onEventStart;
    private OnEventPlaying onEventPlaying;
    private OnEventStop onEventStop;
    private OnEventComplete onEventComplete;
    private OnEventError onEventError;
    private OnEventChangeSize onEventChangeSize;

    boolean isPause = false;
    private View controlView;


    public void setOnEventConnected(OnEventConnected connected){
        this.onEventConnected = connected;
    }

    public void setOnEventPrepared(OnEventPrepared prepared){
        this.onEventPrepared = prepared;
    }

    public void setOnEventStart(OnEventStart onEventStart){
        this.onEventStart = onEventStart;
    }

    public void setOnEventPlaying(OnEventPlaying onEventPlaying){
        isPause = false;
        this.onEventPlaying = onEventPlaying;
    }

    public void setOnEventStop(OnEventStop onEventStop){
        this.onEventStop = onEventStop;
    }

    public void setOnEventComplete(OnEventComplete onEventComplete){
        this.onEventComplete = onEventComplete;
    }

    public void setOnEventError(OnEventError onEventError){
        this.onEventError = onEventError;
    }

    public void setOnEventChangeSize(OnEventChangeSize onEventChangeSize){
        this.onEventChangeSize = onEventChangeSize;
    }



    /*****************************************************
     * Interface
     *****************************************************/
    public interface OnEventPrepared{
        void OnPrepared();

    }

    public interface OnEventConnected{
        void OnConnected();
    }

    public interface OnEventStart {
        void OnStartedItem(IVideo videoItem);
    }

    public interface OnEventPlaying {
        void OnPlayingItem();
    }

    public interface OnEventStop {
        void OnStopedItem();
    }

    public interface OnEventError {
        void OnErrorItem();
    }

    public interface OnEventChangeSize {
        void OnEventChangeSize(int width, int height);
    }

    public interface OnEventComplete {
        void OnEventComplete();
    }

    public ICorePlayer getCorePlayer() {
        return corePlayer;
    }

    public EPlayerView(Context context) {
        super(context);
        if(!isInEditMode()) {
            init(context, null);
        }
    }

    public EPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    public EPlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EPlayerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if(!isInEditMode()) {
            init(context, attrs);
        }
    }

    private void log(String ss){
        Log.d(TAG, ss);
    }

    private void init(final Context context, AttributeSet attrs){
        this.context = context;
        setGravity(Gravity.CENTER);
        setBackgroundColor(getResources().getColor(android.R.color.black));

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(controlImpl != null){
                    if(((ViewGroup) controlImpl).getVisibility() != VISIBLE) {
                        controlImpl.setVisible(true);
                        mHandler.removeMessages(MSG_SHOW_CONTROL);
                        mHandler.sendEmptyMessageDelayed(MSG_SHOW_CONTROL, MSG_SHOW_CONTROL_TIMEOUT);
                    }else {

                        controlImpl.setVisible(false);

                    }
                }
                if(controlView != null){

                    if(controlView.getVisibility() != VISIBLE){
                        controlView.setVisibility(VISIBLE);
                        mHandler.removeMessages(MSG_SHOW_CONTROL);
                        mHandler.sendEmptyMessageDelayed(MSG_SHOW_CONTROL, MSG_SHOW_CONTROL_TIMEOUT);
                    }else {
                        controlView.setVisibility(GONE);
                    }

                }
            }
        });
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mHandler.removeCallbacksAndMessages(null);
    }

    public void setTypePlayer(int type ){

        this.type = type;
        RelativeLayout.LayoutParams rlParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        rlParams.addRule(CENTER_IN_PARENT);

        if(type == TYPE_DEFAULT_PLAYER) {
            corePlayer = new DefaultVideoView(context);
        }else{
            corePlayer = new VlcVideoView(context);
        }

        ((View)corePlayer).setLayoutParams(rlParams);
        addView((View)corePlayer);

        addOnLayoutChangeListener(new OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {

                widthVideo = right;
                heightVideo = (int) (widthVideo * 0.5625);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) getLayoutParams();
                params.height = heightVideo;
                params.width = right;
                setLayoutParams(params);
                layout(0,0, widthVideo, heightVideo);
                invalidate();
                resize(0,0, widthVideo, heightVideo);
                removeOnLayoutChangeListener(this);
                if(onEventChangeSize != null){
                    onEventChangeSize.OnEventChangeSize(widthVideo, heightVideo);
                }
            }
        });

        corePlayer.setOnEventListener(this);

    }

    public void scaleView(int width, int height){

        widthVideo = width;
        heightVideo = (int) (widthVideo * heightVideo / widthVideo);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) getLayoutParams();
        params.height = heightVideo;
        params.width = widthVideo;
        setLayoutParams(params);
        layout(0,0, widthVideo, heightVideo);
        invalidate();
        resize(0,0, widthVideo, heightVideo);
    }

    @Override
    public void OnPrepared() {

        if(onEventPrepared != null){
            onEventPrepared.OnPrepared();
        }

    }

    @Override
    public void OnStartedItem(IVideo videoItem) {

        if (onEventStart != null){
            onEventStart.OnStartedItem(videoItem);
        }

    }

    @Override
    public void OnPlayingItem() {

        if(controlImpl != null) {
            controlImpl.seekBar().setMax((int) (corePlayer.getLength() / 1000));
            mHandler.removeMessages(MSG_UPDATE_CONTROL);
            mHandler.sendEmptyMessage(MSG_UPDATE_CONTROL);
        }
        if(onEventPlaying != null){
            onEventPlaying.OnPlayingItem();
        }

    }

    @Override
    public void OnStopedItem() {

        if(onEventStop != null){
            onEventStop.OnStopedItem();
        }

    }

    @Override
    public void OnErrorItem() {

        if(onEventError != null){
            onEventError.OnErrorItem();
        }

    }

    @Override
    public void OnEventChangeSize(int width, int height) {
        widthVideo = width;
        heightVideo = height;
        if(onEventChangeSize != null){
            onEventChangeSize.OnEventChangeSize(width, height);
        }

    }

    @Override
    public void OnEventComplete() {

        if(onEventComplete != null){
            onEventComplete.OnEventComplete();
        }

    }


    public void setTitle(String title){

        if(controlImpl != null){
            ((TextView) controlImpl.viewTitle()).setText(title);
        }
    }

    private void updateControl(){

        ((TextView) controlImpl.viewTimeCurrent()).setText(Util.formatTime(corePlayer.getCurrent() / 1000));
        ((TextView) controlImpl.viewTimeDuration()).setText(Util.formatTime(corePlayer.getLength() / 1000));
        controlImpl.seekBar().setProgress((int) (corePlayer.getCurrent() / 1000));
    }
    public void addControl(IControl defaultControl){

        controlImpl = defaultControl;
        RelativeLayout.LayoutParams rlParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rlParams.addRule(ALIGN_PARENT_BOTTOM);
        ((ViewGroup) controlImpl).setLayoutParams(rlParams);
        addView((ViewGroup) controlImpl);

        controlImpl.controlPlay().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isPause) {
                    corePlayer.replay();
                    ((ImageView) controlImpl.controlPlay()).setImageResource(controlImpl.getResPause());
                }else {
                    corePlayer.pause();
                    ((ImageView) controlImpl.controlPlay()).setImageResource(controlImpl.getResPlay());

                }
                isPause = !isPause;

            }
        });

        controlImpl.seekBar().setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                ((TextView) controlImpl.viewTimeCurrent()).setText(Util.formatTime(seekBar.getProgress() ));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                mHandler.removeMessages(MSG_UPDATE_CONTROL);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                int time = seekBar.getProgress();
                corePlayer.seek(time * 1000);
                mHandler.sendEmptyMessage(MSG_UPDATE_CONTROL);


            }
        });

        mHandler.sendEmptyMessageDelayed(MSG_SHOW_CONTROL, MSG_SHOW_CONTROL_TIMEOUT);



    }


    public void addViewControl(View controlView){

        this.controlView = controlView;
        addView(controlView);
        mHandler.sendEmptyMessageDelayed(MSG_SHOW_CONTROL, MSG_SHOW_CONTROL_TIMEOUT);

    }



    public void resize(int l, int t, int r, int b){

        if(corePlayer instanceof VideoView){
//            ((VideoView) corePlayer).layout(l, t, r, b);
            RelativeLayout.LayoutParams params = (LayoutParams) ((VideoView) corePlayer).getLayoutParams();
            params.addRule(CENTER_IN_PARENT);
            params.width = r;
            params.height = b;


        }
    }

    public void start() {

        corePlayer.play();

    }

    public void resume(){

        corePlayer.replay();

    }

    public void stop() {

        corePlayer.stop();

    }

    public void pause() {
        corePlayer.pause();

    }

    public void loadMedia(@NonNull final List<? extends IVideo> videos) {

        corePlayer.setData(videos);

    }

    public void setVideoPath(@NonNull final String path){

        corePlayer.setVideoPath(path);
        if(corePlayer instanceof DefaultVideoView) {
            ((DefaultVideoView) corePlayer).start();

        }
    }



    public void loadMedia(@NonNull final VideoItem video) {

        corePlayer.setData(video);

    }

    public void next() {

        corePlayer.next();

    }

    public void prev() {

        corePlayer.prev();

    }

    public void rewind() {

        corePlayer.rewind();

    }

    public void fastforward() {

        corePlayer.fastforward();

    }

    public void setSizeSubtitle(int size){

        if(mSubtitle != null)
            mSubtitle.setSize(size);

    }

    public void setColorSubtitle(int color){

        if(mSubtitle != null )
            mSubtitle.setColor(color);

    }

    public void setSubtile(final String path){
       new AsyncTask<Void, Void, String>() {
           @Override
           protected String doInBackground(Void... params) {
               try {
                   URL url = new URL(path);
                   URLConnection connection = url.openConnection();
                   connection.connect();
                   InputStream input = new BufferedInputStream(url.openStream());
                   String name = path.substring(path.lastIndexOf("/"), path.length());
                   String pathSave = Environment.getExternalStorageDirectory().getPath() + "/ecf/cache/";
                   new File(pathSave).mkdirs();

                   File file = new File(pathSave, name);
                   if(file.exists()){
                       file.delete();
                   }
                   file.createNewFile();
                   OutputStream outputStream = new FileOutputStream(file);

                   byte data[] = new byte[1024];
                   int count;
                   while ((count = input.read(data)) != -1) {
                       outputStream.write(data, 0, count);
                   }
                   outputStream.flush();
                   outputStream.close();
                   input.close();
                   return pathSave + name;
               } catch (MalformedURLException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
               return null;
           }

           @Override
           protected void onPostExecute(String s) {
               super.onPostExecute(s);
               if(s != null){

                   mSubtitle.startLoadSubtitleTask(s);
                   mHandler.removeMessages(MSG_UPDATE);
                   mHandler.sendEmptyMessageDelayed(MSG_UPDATE, 2000);

               }
           }
       }.execute();

    }


    public boolean isPlaying() {
        return corePlayer.isPlaying();
    }

    public long getLength() {
        return corePlayer.getLength();
    }


    public void seek(long milisecond) {
        corePlayer.seek(milisecond);
    }

    public long getCurrent() {
        return corePlayer.getCurrent();
    }



    private Handler mHandler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what){
                case MSG_UPDATE:

                    long current= corePlayer.getCurrent();
                    mSubtitle.findSubTitle((int) current);
                    mHandler.sendEmptyMessageDelayed(MSG_UPDATE,1000);
                    return  true;

                case MSG_UPDATE_CONTROL:
                    if(controlImpl != null){
                        updateControl();
                        mHandler.removeMessages(MSG_UPDATE_CONTROL);
                        mHandler.sendEmptyMessageDelayed(MSG_UPDATE_CONTROL, MSG_UPDATE_CONTROL_TIMEOUT);
                    }

                    return true;

                case MSG_SHOW_CONTROL:

                    if(controlImpl != null){

                        controlImpl.setVisible(false);

                    }
                    if(controlView != null){

                        controlView.setVisibility(GONE);

                    }
                    return true;





            }
            return false;
        }
    });








}
