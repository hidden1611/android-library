package elcom.nna.ehotel.library.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;

/**
 * Helper class for dialog fragments to show a {@link AlertDialog}. It can be used exactly like
 * a {@link AlertDialog.Builder}
 * 
 * Creation Date: 22.03.16
 * @author Felix Heller, http://flx-apps.com/
 */
public class AlertDialogFragment extends DialogFragment {

    private static AlertDialogFragment newInstance(Bundle args) {
        AlertDialogFragment alertDialogFragment = new AlertDialogFragment();
        alertDialogFragment.setArguments(args);
        return alertDialogFragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();

        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (args.containsKey("title")) {
            builder.setTitle(args.getString("title"));
        }

        if (args.containsKey("message")) {
            builder.setMessage(args.getString("message"));
        }

        if (args.containsKey("viewId")) {
            builder.setView(getActivity().getLayoutInflater().inflate(args.getInt("viewId"), null));
        }

        /*
         args.putCharSequenceArray("items", items);
            args.putParcelable("itemsOnClickListener",createParcelableOnClickListener(onClickListen
         */
        if(args.containsKey("items")){
            builder.setItems(args.getCharSequenceArray("items"), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onButtonClicked("itemsOnClickListener", dialog, which);
                }
            });
        }

        if (args.containsKey("positiveButtonText")) {
            builder.setPositiveButton(args.getString("positiveButtonText"),  new DialogInterface.OnClickListener(){

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onButtonClicked("positiveButtonListener", dialog, which);
                }
            });
        }

        if (args.containsKey("negativeButtonText")) {
            builder.setNegativeButton(args.getString("negativeButtonText"),  new DialogInterface.OnClickListener(){

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onButtonClicked("negativeButtonListener", dialog, which);
                }
            });
        }

        if (args.containsKey("neutralButtonText")) {
            builder.setNeutralButton(args.getString("neutralButtonText"),  new DialogInterface.OnClickListener(){

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onButtonClicked("neutralButtonListener", dialog, which);
                }
            });
        }
        dialog = builder.create();

        if(args.containsKey("onshowListener")){

            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {

                    ParcelableOnShowListener parcelableOnShowListener = getArguments().getParcelable("onshowListener");
                    parcelableOnShowListener.onShow(dialog);

                }
            });

        }

        return dialog;
    }

    private void onButtonClicked(String buttonKey, DialogInterface dialog, int which) {
        ParcelableOnClickListener parcelableOnClickListener = getArguments().getParcelable(buttonKey);
        if (parcelableOnClickListener != null) {
            parcelableOnClickListener.onClick(dialog, which);
        }
    }


    /**
     * Parcelable OnClickListener implementing {@link DialogInterface.OnClickListener}
     */
    public static abstract class ParcelableOnClickListener implements DialogInterface.OnClickListener, Parcelable {
        @Override
        public abstract void onClick(DialogInterface dialog, int which);

        @Override
        public void writeToParcel(Parcel dest, int flags) {}

        @Override
        public int describeContents() {
            return 0;
        }
    }

    public static abstract class ParcelableOnShowListener implements DialogInterface.OnShowListener, Parcelable{
        @Override
        public abstract void onShow(DialogInterface dialog);

        @Override
        public void writeToParcel(Parcel dest, int flags) {}

        @Override
        public int describeContents() {
            return 0;
        }
    }

    // region Convenience Builder Pattern class almost similar to AlertDialog.Builder
    // =============================================================================================
    public static class Builder {
        FragmentActivity activity;
        Bundle args;
        String logTag = AlertDialogFragment.class.getSimpleName();

        public Builder(FragmentActivity activity) {
            this.activity = activity;
            this.args = new Bundle();
        }

        public Builder setTitle(int titleStringId) {
            return setTitle(activity.getString(titleStringId));
        }

        public Builder setTitle(String title) {
            args.putString("title", title);
            return this;
        }

        public Builder setMessage(int messageStringId) {
            return setMessage(activity.getString(messageStringId));
        }

        public Builder setMessage(String message) {
            args.putString("message", message);
            return this;
        }

        public Builder setPositiveButton(int textStringId, DialogInterface.OnClickListener onClickListener) {
            return setPositiveButton(activity.getString(textStringId), onClickListener);
        }

        public Builder setPositiveButton(String text, DialogInterface.OnClickListener onClickListener) {
            args.putString("positiveButtonText", text);
            args.putParcelable("positiveButtonListener", createParcelableOnClickListener(onClickListener));
            return this;
        }

        public Builder setNegativeButton(int textStringId, DialogInterface.OnClickListener onClickListener) {
            return setNegativeButton(activity.getString(textStringId), onClickListener);
        }

        public Builder setNegativeButton(String text, DialogInterface.OnClickListener onClickListener) {
            args.putString("negativeButtonText", text);
            args.putParcelable("negativeButtonListener", createParcelableOnClickListener(onClickListener));
            return this;
        }

        public Builder setNeutralButton(int textStringId, DialogInterface.OnClickListener onClickListener) {
            return setNeutralButton(activity.getString(textStringId), onClickListener);
        }

        public Builder setNeutralButton(String text, DialogInterface.OnClickListener onClickListener) {
            args.putString("neutralButtonText", text);
            args.putParcelable("neutralButtonListener", createParcelableOnClickListener(onClickListener));
            return this;
        }

        public Builder setOnShowListener(DialogInterface.OnShowListener onShowListener){

            args.putParcelable("onshowListener", createParcelableOnShowListener(onShowListener));
            return this;
        }

        /**
         * Inflate the layout to dialog. To get the specific in this layout, follow:
         * .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
         *  @Override
         *  public void onClick(DialogInterface dialogInterface, int which) {
         *      Dialog dialog  = (Dialog) dialogInterface;
         *      TextView textView = (TextView) dialog.findViewById(R.id.txtPercentApk);
         *  }
         *  })
         * @param viewId
         * @return
         */
        public Builder setView(int viewId) {
            args.putInt("viewId", viewId);
            return this;
        }

        public Builder setItems(CharSequence[] items, DialogInterface.OnClickListener onClickListener){
            args.putCharSequenceArray("items", items);
            args.putParcelable("itemsOnClickListener",createParcelableOnClickListener(onClickListener));
           return this;
        }

        public Builder setLogTag(String logTag) {
            this.logTag = logTag;
            return this;
        }

        public void show() {
            AlertDialogFragment.newInstance(args).show(activity.getSupportFragmentManager(), logTag);
        }

        private ParcelableOnClickListener createParcelableOnClickListener(final DialogInterface.OnClickListener onClickListener) {
            if (onClickListener == null) {
                return null;
            }

            return new ParcelableOnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onClickListener.onClick(dialog, which);
                }
            };
        }

        private ParcelableOnShowListener createParcelableOnShowListener(final DialogInterface.OnShowListener onShowListener){
            if(onShowListener == null){
                return null;
            }
            return new ParcelableOnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    onShowListener.onShow(dialog);
                }
            };
        }
    }
    // =============================================================================================
    // endregion
}