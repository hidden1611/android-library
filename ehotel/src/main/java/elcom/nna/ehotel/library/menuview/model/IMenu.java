package elcom.nna.ehotel.library.menuview.model;

/**
 * Created by Ann on 4/29/16.
 */
public interface IMenu {

    String getName();

    String getId();

    String getUrlIcon();

    boolean isSelected();



}
