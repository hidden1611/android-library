package elcom.nna.ehotel.library.player.core;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;

/**
 * Created by Ann on 2/10/17.
 */

public class MediaAudioKaraokePlayer extends AbstractAudioKaraokePlayer {

    MediaPlayer player;

    public MediaAudioKaraokePlayer(Context context) {
        super(context);

        player = new MediaPlayer();
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {

                if (playing) {
                    player.start();
                }else {
                    player.start();
                    player.pause();
                }

            }
        });

        player.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {

                Log.d(TAG, "onError " + what + "," + extra);

                return false;
            }
        });



    }

    @Override
    public void setSource(String urlA, String urlB) {

    }

    @Override
    public void stopImpl() {

    }

    @Override
    public void pauseImpl() {

    }

    @Override
    public void releaseImpl() {

    }

    @Override
    public void nextTrackImpl() {

    }

    @Override
    public void seekAndPlayImpl(long us) {

    }

    @Override
    void seek(long us) {

    }

    @Override
    long getCurrentMs() {
        return 0;
    }

    private static final String TAG = "nna - MediaAu";


    @Override
    public void stop() {
        super.stop();

        try {

            player.stop();
            player.reset();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void release(){
        super.release();
        try {
            player.stop();
            player.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void play(boolean playing){
        super.play(playing);
        player.pause();

    }
    @Override
    public void seekAndPlay(long us){
        super.seekAndPlay(us);

        player.seekTo((int) us);
        player.start();

    }


//    @Override
//    public void setSource(String uri) {
//
//        try {
//            player.stop();
//            player.reset();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//
//            player.setDataSource(uri);
//            player.prepare();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }


}
