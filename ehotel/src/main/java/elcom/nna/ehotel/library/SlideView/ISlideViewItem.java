package elcom.nna.ehotel.library.SlideView;

/**
 * Created by baotruongtuan on 7/25/16.
 */
public interface ISlideViewItem {


    int getId();
    String getName();
    String getImage();
    boolean isSelected();
    void setSelected(boolean value);

}
