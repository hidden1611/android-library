package elcom.nna.ehotel.library.player.dvplayer;

import android.graphics.drawable.Drawable;


import java.io.File;


public class SubtitleItem {
    public boolean isSelected = false;
	private String text;
	private Drawable drawable;
	private String url;
	private String pathSave;
	private String filename;
	private int 	id = 1;

//    public SubtitleItem(Element element){
//
//        text = element.select("name_lang").text();
//        url = element.select("url_lang").text();
//        id = Integer.parseInt(element.select("langid").text());
//
//        filename = url.substring(url.lastIndexOf("/")+1,url.length());
//        pathSave = Globals.path + filename;
//
//
//    }
	public SubtitleItem(String text, String url, Drawable drawable) {
		// TODO Auto-generated constructor stub
		this.text = text;
		this.drawable = drawable;
		this.url = url;
//		pathSave = DAO.spliteSever(url, false);
		/*pathSave = url.substring("http://172.16.9.202:80/".length(), url.length());*/
		filename = pathSave.substring(pathSave.lastIndexOf("/")+1,pathSave.length());
		pathSave = Globals.path + pathSave.substring(0,pathSave.lastIndexOf("/"));
		
	}
	public boolean isDownloaded(){
		File f = new File(pathSave+"/" + filename);
		if(f.exists()) return true;
		else 			return false;
	}
	public void deleteFile(){
		File f = new File(pathSave+"/" + filename);
		if(f.exists())	f.delete();
	}
	public String filename(){
		return filename;
	}
	public String pathSave(){
		return pathSave;
	}
	public String url(){
		return url;
	}
	public Drawable drawable(){
		return drawable;
	}
	public String text(){
		return text;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}
