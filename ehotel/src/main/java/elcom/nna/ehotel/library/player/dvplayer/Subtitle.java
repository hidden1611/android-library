package elcom.nna.ehotel.library.player.dvplayer;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class Subtitle {
	ArrayList<String> listSub;
	ArrayList<String> listTime;
	ArrayList<Integer> listStart;
	ArrayList<Integer> listEnd;
	private boolean isEnable = true;
	private String filepath;
	private TextView mTextSub;
	private int last_pos = 0;
	private boolean is_ready = false;
	private LoadSubtitleTask	loadSubtitleTask;
	private class Block
	{
		int seconds_start,seconds_end;
		String[] contents;
	};
	private Block[] blocks = null;

	public String findSubTitle(int cur_ms)
	{
		try{
			if(isEnable == false) {
				mTextSub.setText(null);
				return null;
			}
			if (blocks==null) {
				mTextSub.setText(null);
				return null;
			}
			if (cur_ms<blocks[0].seconds_start){
				mTextSub.setText(null);
				return null;
			}
			if (cur_ms<blocks[last_pos].seconds_start)
			{
				last_pos = last_pos/2;
				return findSubTitle(cur_ms);
			}
			for (int i=last_pos;i<blocks.length;i++)
			{
				if (cur_ms>=blocks[i].seconds_start && cur_ms<=blocks[i].seconds_end)
				{
					String result = "";
					for (int j=0;j<blocks[i].contents.length;j++)
					{
						result += blocks[i].contents[j]+"\r\n";
					}
					last_pos = i;
					mTextSub.setText(Html.fromHtml(result));
					return result;
				}
				if (cur_ms<=blocks[i].seconds_end){
					mTextSub.setText(null);
					return null;
				}
			}
			mTextSub.setText(null);
		}catch (Exception e) {
		}
		return null;
	}

	private boolean LoadSubtitle(String filepath)
	{
		this.filepath = filepath;
		is_ready = false;
		blocks = null;
		if (filepath==null) return false;
		try {
			RandomAccessFile in = new RandomAccessFile(new File(filepath),"r");
			int size = (int)in.length();
			if (size<10) return false;
			byte[] buffer = new byte[size];
			int byte_0 = in.read();
			int byte_1 = in.read();
			int byte_2 = in.read();
			int byte_3 = in.read();
			String codec = null;
			//Xac dinh codec
			if (byte_0 == 0xef && byte_1 == 0xbb && byte_2 == 0xbf)
				codec = "UTF-8";
			else if (byte_0 == 0xff && byte_1 == 0xfe)
				codec = "UTF-16";
			else if (byte_0 == 0xfe && byte_1 == 0xff)
				codec = "UTF-16";
			else if (byte_0 == 0 && byte_1 == 0 && byte_2 == 0xfe && byte_3 == 0xff)
				codec = "UTF-32";
			else if (byte_0>=0x20 && byte_0<=0x7f)
				codec = "ASCII";
			Log.e("SubtitleUtil","subtitle file codec: "+codec);
			if (codec==null)
			{
				in.close();
				return false;
			}
			in.seek(0);
			in.read(buffer,0,size);
			in.close();
			String[] groups = null;
			if (codec.compareTo("UTF-8")==0)
				groups = (new String(buffer,codec)).trim().split("\r\n\r\n");
			else if (codec.compareTo("ASCII")==0)
			{
				groups = (new String(buffer)).trim().split("\r\n\r\n");
			}
			else
				groups = new String((new String(buffer,codec)).getBytes("UTF-8")).trim().split("\r\n\r\n");
			//get block_count
			int block_count = 0;
			for (int i=0;i<groups.length;i++)
			{
				
				String[] contents = (new String(groups[i])).trim().split("\r\n");
				if (contents.length>2)
				{
					block_count++;
				}
			}
			blocks = new Block[block_count];
			Log.e("SubtitleUtil","block_count: "+block_count);
			block_count = 0;
			for (int i=0;i<groups.length;i++)
			{
				
				String[] contents = (new String(groups[i])).trim().split("\r\n");
				if (contents.length>2)
				{
					blocks[block_count] = new Block();
					parse_times(contents[1],blocks[block_count]);
					int contents_count = contents.length-2;
					blocks[block_count].contents = new String[contents_count];
					for (int j=0;j<contents_count;j++)
					{
						blocks[block_count].contents[j] = contents[2+j];
					}
					block_count++;	
				}
			}
			is_ready = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return is_ready;
	}
	
	private void stopLoadSubtitleTask(){
		if (loadSubtitleTask != null) {
			loadSubtitleTask.cancel(true);
			loadSubtitleTask = null;
		}
	}
	
	public void startLoadSubtitleTask(String path){
		stopLoadSubtitleTask();
		loadSubtitleTask = new LoadSubtitleTask();
		loadSubtitleTask.execute(path);
	}
	
	private class LoadSubtitleTask extends AsyncTask<String, Void, Boolean>{

		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			boolean isLoaded = LoadSubtitle(params[0]);
			return isLoaded;
		}
		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
		
	}
	private int parse_time(String time)
	{
		String[] tmp1 = time.split(":");
		if (tmp1.length!=3) return 0;
		String[] tmp2 = tmp1[2].trim().split(",");
		if (tmp2.length!=2) return 0;
		return ((Integer.valueOf(tmp1[0]).intValue()*60+Integer.valueOf(tmp1[1]).intValue())*60+Integer.valueOf(tmp2[0]).intValue())*1000+Integer.valueOf(tmp2[1]).intValue();
	}
	private void parse_times(String in_times,Block block)
	{
		String[] times = in_times.split(" --> ");
		if (times.length!=2) return;
		block.seconds_start = parse_time(times[0].trim());
		block.seconds_end = parse_time(times[1].trim());
	}
	
	
	
	public Subtitle(TextView mTextSub){
		isEnable = true;
		this.mTextSub = mTextSub;
	}
	
	public Subtitle(String filepath, TextView mTextSub){
		isEnable = true;
		this.filepath = filepath;
		this.mTextSub = mTextSub;
		/*LoadSubtitle(filepath);*/
		startLoadSubtitleTask(filepath);
	}
	public String getLink(){
		return filepath;
	}
//	public void LoadSubtitle(String path){
//		filepath = path;
//		// Open the file
//		listSub = new ArrayList<String>();
//		listTime = new ArrayList<String>();
//		listStart = new ArrayList<Integer>();
//		listEnd = new ArrayList<Integer>();
//		
//		FileInputStream fstream;
//		StringBuilder ss = new StringBuilder();
//		try {
//			fstream = new FileInputStream(filepath);
//			// Get the object of DataInputStream
//			DataInputStream in = new DataInputStream(fstream);
//			BufferedReader br = new BufferedReader(new InputStreamReader(in));
//			String strLine;
//			boolean bEndContain = false;
//			int iCounter = 2;
//			//Read File Line By Line
//			while ((strLine = br.readLine()) != null)   {
//			  // Print the content on the console
//				if(strLine.equals(String.valueOf(iCounter)) && bEndContain  ){
//					bEndContain = false;
//					listSub.add(ss.toString());
//					iCounter++;
//				}
//				ss.append("\n"+strLine);
//				if(strLine.contains("-->")){
//					listTime.add(strLine);
//					ss= new StringBuilder();
//				}
//				if(strLine.equals("") || strLine.equals(" ")){
//					bEndContain = true;
//				}
//				
//			}
//			listSub.add(ss.toString());
//			//Close the input stream
//			in.close();
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		for(int i = 0;i < listTime.size();i++){
//			String start,end;
//			start = listTime.get(i).split(" --> ")[0];
//			end = listTime.get(i).split(" --> ")[1];
//			listStart.add(stringtoMiliTime(start));
//			listEnd.add(stringtoMiliTime(end));
//		}
//		isSubOk = isParseSubtitle();
//	}
	public boolean isReady()
	{
		return is_ready;
	}
	private int stringtoMiliTime(String ss){
		double time=0;
		String[] stime;
		stime = ss.split(":");
		if(stime.length >= 3){
			stime[2]=stime[2].replace(",",".");
			time = Integer.parseInt(stime[0])*3600+ 
					Integer.parseInt(stime[1])*60+ 
					Double.parseDouble(stime[2]);
		}
		return (int)(time*1000);
	}
	
	public boolean isParseSubtitle(){
		if(listSub.size() == 0) 
			return false;
		if((listEnd.size()==listStart.size()) && 
				(listStart.size()==listSub.size()))
			return true;
		else 
			return false;
	}
	
//	public String findSubTitle(int clockVideo){
//		if(!isSubOk)  return "Subtitle Error";
//		if(!isEnable) return null;
//		for(int i=0;i<listStart.size();i++){
//			if((listStart.get(i) < clockVideo) && (listEnd.get(i) > clockVideo)){
//				//mTextSub.setText(listSub.get(i));
//				mTextSub.setText(Html.fromHtml(listSub.get(i)));
//				return listSub.get(i);
//			}
//		}
//		mTextSub.setText(null);
//		return null;
//	}
	public void setColor(int color){
		mTextSub.setTextColor(color);
	}
	public void setSize(float size){
		mTextSub.setTextSize(size);
	}
	public void setPosition(int position){
		if(position == 0) mTextSub.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL);
		else if(position == 1) mTextSub.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
		else if(position == 2) mTextSub.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL);
	}
	public void setEnable(boolean value){
		isEnable = value;
	}
	public void setFont(String file){
		if (!TextUtils.isEmpty(file)){
			Typeface tf = Typeface.createFromFile(file);
			mTextSub.setTypeface(tf);
		}
	}
}
