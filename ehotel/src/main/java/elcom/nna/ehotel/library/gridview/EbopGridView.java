package elcom.nna.ehotel.library.gridview;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import elcom.nna.ehotel.library.menuview.grid.GridAutofitLayoutManager;

/**
 * Created by nna on 25/07/2016.
 */
public class EbopGridView extends LinearLayout {

    private RecyclerView rv;
    private GridLayoutManager linearLayoutManager;
    EgvAdapter egvAdapter;
    private int columnWidth;
    private int columnHeight;
    private int colorBgText;
    private int colorText;
    private OnClickedCell onClickedCell;

    public EbopGridView(Context context) {

        super(context);
        if(!isInEditMode()){
            init(context, null);
        }
    }

    public EbopGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()){
            init(context, attrs);
        }
    }

    public EbopGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()){
            init(context, attrs);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EbopGridView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        if(!isInEditMode()){
            init(context, attrs);
        }
    }

    private void init(Context context, AttributeSet attributeSet){

//        if (attributeSet != null) {
//
//            TypedArray a = context.obtainStyledAttributes(attributeSet, R.styleable.EbopGridView);
//            columnWidth = a.getDimensionPixelSize(R.styleable.EbopGridView_columnWidth, -1);
//            columnHeight = a.getDimensionPixelSize(R.styleable.EbopGridView_columnHeight, -1);
//            colorBgText = a.getColor(R.styleable.EbopGridView_colorBgText, getResources().getColor(R.color.bg_text));
//            colorText = a.getColor(R.styleable.EbopGridView_colorText, getResources().getColor(R.color.textcolor));
//            a.recycle();
//
//        }

        rv = new RecyclerView(getContext());
        rv.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rv.setLayoutParams(params);
        addView(rv);
        initRecycecleView();
    }



    private void initRecycecleView(){

        linearLayoutManager = new GridAutofitLayoutManager(getContext(), 800);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(linearLayoutManager);
        egvAdapter = new EgvAdapter(columnWidth, columnHeight,colorBgText, colorText);
        rv.setAdapter(egvAdapter);
        egvAdapter.setOnItemClickListener(new EgvAdapter.OnItemClickListener() {
            @Override
            public void OnClick(View view, int line, int cell) {

                Log.d("nna", "line:" + line +  ",cell:" + cell);

            }
        });

    }

    public void setData(List<? extends IEgvLine> egvLines){
        egvAdapter.addItems((List<IEgvLine>) egvLines);
    }


    public void setOnClickedCell(OnClickedCell onClickedCell){
        this.onClickedCell = onClickedCell;

    }
    public interface OnClickedCell {
        void onCell(int indexLine, int indexCell);
    }






}
