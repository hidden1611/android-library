package elcom.nna.ehotel.library.menuview.model;

/**
 * Created by Ann on 4/29/16.
 */
public class Menu implements IMenu {

    String name;
    String id;
    String urlIcon;
    boolean isSelected;

    public Menu(String name, String id, String urlIcon){

        this.name = name;
        this.id = id;
        this.urlIcon = urlIcon;
        isSelected = false;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getUrlIcon() {
        return urlIcon;
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public String toString() {
        return "name"  + "," + urlIcon;
    }
}
