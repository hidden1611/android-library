package elcom.nna.ehotel.library.player.core;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.List;

import elcom.nna.ehotel.library.player.model.IVideo;

/**
 * Created by nna on 25/05/2016.
 */
public class DefaultVideoView extends VideoView implements ICorePlayer, MediaPlayer.OnPreparedListener{

    public final static int TIMESEEK = 30*1000;

    List<IVideo> videos;
    int index = 0;
    private OnEventListener mOnEventListener;

    public DefaultVideoView(Context context) {
        super(context);
        init(context, null);
    }

    public DefaultVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public DefaultVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DefaultVideoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){

        setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                mOnEventListener.OnEventComplete();
            }
        });


        setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {

                mOnEventListener.OnErrorItem();

                return true;
            }
        });

        setOnPreparedListener(this);
    }



    @Override
    public void replay() {

        start();

        if(getVisibility() == GONE) {
            setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void play() {
        setVideoPath(videos.get(index).getUrl());
        start();

        mOnEventListener.OnStartedItem(videos.get(index));

        if(getVisibility() == GONE) {
            setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void stop() {

        stopPlayback();
        if(getVisibility() == VISIBLE) {
            setVisibility(View.GONE);
        }

    }



    @Override
    public void setData(@NonNull List<? extends IVideo> videos) {
        this.videos = (List<IVideo>) videos;
        index = 0;

        //check video playing
        for (int i = 0; i < videos.size(); i++) {
            if(videos.get(i).isPlaying()){
                index = i;
                break;
            }
        }



    }

    @Override
    public void setData(@NonNull IVideo video) {
        videos = new ArrayList<>();
        videos.add(video);

    }

    @Override
    public void next() {

        if(++index >= videos.size() ){
            index = 0;
        }
        play();
    }

    @Override
    public void prev() {

        if(--index < 0 ){
            index = videos.size() - 1;
        }
        play();

    }

    @Override
    public void rewind() {

        seek(getCurrent() - TIMESEEK);

    }

    @Override
    public void fastforward() {

        seek(getCurrent() + TIMESEEK);

    }

    @Override
    public void seek(long milisecond) {
        seekTo((int) milisecond);

    }

    @Override
    public long getCurrent() {
        return getCurrentPosition();
    }

    @Override
    public long getLength() {
        return getDuration();
    }

    @Override
    public void setOnEventListener(OnEventListener mOnEventListener) {
        this.mOnEventListener = mOnEventListener;

    }


    @Override
    public void onPrepared(MediaPlayer mp) {

        mp.start();

    }
}
