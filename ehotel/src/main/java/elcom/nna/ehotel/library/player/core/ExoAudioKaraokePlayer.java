package elcom.nna.ehotel.library.player.core;

import android.content.Context;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;

/**
 * Created by Ann on 2/10/17.
 */

public class ExoAudioKaraokePlayer extends AbstractAudioKaraokePlayer {

    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();

    private final Context context;
    private final AdaptiveVideoTrackSelection.Factory videoTrackSelectionFactory;
    private final DefaultTrackSelector trackSelector;
    private final String userAgent;
    private final DataSource.Factory mediaDataSourceFactory;
    SimpleExoPlayer player;

    public ExoAudioKaraokePlayer(Context context) {
        super(context);

        this.context = context;

        userAgent =  Util.getUserAgent(context, "player");
        mediaDataSourceFactory = buildDataSourceFactory(true);
        videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(BANDWIDTH_METER);
        trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        player = ExoPlayerFactory.newSimpleInstance(context,new DefaultTrackSelector(), new DefaultLoadControl(), null, SimpleExoPlayer.EXTENSION_RENDERER_MODE_PREFER);
    }

    @Override
    public void setSource(String urlA, String urlB) {

    }

    @Override
    public void stopImpl() {

    }

    @Override
    public void pauseImpl() {

    }

    @Override
    public void releaseImpl() {

    }

    @Override
    public void nextTrackImpl() {

    }

    @Override
    public void seekAndPlayImpl(long us) {

    }

    @Override
    void seek(long us) {

    }

    @Override
    long getCurrentMs() {
        return 0;
    }

    @Override
    public void stop() {

        super.stop();

        player.setPlayWhenReady(false);

    }

    @Override
    public void release(){
        super.release();

        try {
            player.stop();
            player.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void play(boolean play){

        super.play(play);

        player.setPlayWhenReady(false);
    }


    @Override
    public void seekAndPlay(long us){
        super.seekAndPlay(us);
        player.setPlayWhenReady(true);
    }

//    @Override
//    public void setSource(String uri) {
//        Uri uriData = Uri.parse(uri);
//        MediaSource mediaSource =  new ExtractorMediaSource(uriData, mediaDataSourceFactory,  new DefaultExtractorsFactory(), null, null);
//        player.prepare(mediaSource);
//        player.setPlayWhenReady(playing);
//
//    }

    /**
     * Returns a new DataSource factory.
     *
     * @param useBandwidthMeter Whether to set  as a listener to the new
     *     DataSource factory.
     * @return A new DataSource factory.
     */
    private DataSource.Factory buildDataSourceFactory(boolean useBandwidthMeter) {
        return new DefaultDataSourceFactory(context, useBandwidthMeter ? BANDWIDTH_METER : null,
                buildHttpDataSourceFactory(useBandwidthMeter));
    }

    /**
     * Returns a new HttpDataSource factory.
     *
     * @param useBandwidthMeter Whether to set  as a listener to the new
     *     DataSource factory.
     * @return A new HttpDataSource factory.
     */
    private HttpDataSource.Factory buildHttpDataSourceFactory(boolean useBandwidthMeter) {
        return new DefaultHttpDataSourceFactory(userAgent, useBandwidthMeter ? BANDWIDTH_METER : null);
    }

}
