package elcom.nna.ehotel.library.player.core;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;

import java.io.IOException;
import java.util.List;

import elcom.nna.ehotel.library.player.model.IVideo;

/**
 * Created by Ann on 11/22/16.
 */

public class MediaCorePlayer extends AbstractCorePlayer {


    private MediaPlayer mediaPlayer;

    SurfaceHolder surfaceHolder;

    public MediaCorePlayer(Context context) {
        super(context);
    }

    @Override
    void initUI(Context context, AttributeSet attributeSet) {

        mediaPlayer = new MediaPlayer();

        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {

                mOnEventListener.OnErrorItem();
                return true;
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mOnEventListener.OnEventComplete();
            }
        });

        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                mOnEventListener.OnPlayingItem();
            }
        });

        mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
            @Override
            public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                mOnEventListener.OnEventChangeSize(width, height);

            }
        });



        getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {

                surfaceHolder = holder;
                mediaPlayer.setDisplay(holder);

            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });




    }

    @Override
    public void setData(@NonNull List<? extends IVideo> videos) {
        super.setData(videos);
        play();
    }

    @Override
    public void replay() {

        mediaPlayer.start();
    }

    private static final String TAG = "MediaCorePlayer";
    @Override
    public void play() {

        String url = videos.get(index).getUrl();
        Log.d(TAG, "play " + url);
        if(url == null){
            return;
        }

        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            Log.d(TAG, "play error" + url);
            e.printStackTrace();
        }
    }

    @Override
    public void stop() {

        mediaPlayer.stop();

    }

    @Override
    public void pause() {

        mediaPlayer.pause();

    }

    @Override
    public void seek(long milisecond) {

        mediaPlayer.seekTo((int) milisecond);

    }

    @Override
    public long getCurrent() {
        return mediaPlayer.getCurrentPosition();
    }

    @Override
    public long getLength() {
        return mediaPlayer.getDuration();
    }

    @Override
    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }
}
