package elcom.nna.ehotel.library.mpview;

/**
 * Created by Ann on 5/3/16.
 */
public interface IOnChangeItemListener {

    void onChanged(int index);

}
