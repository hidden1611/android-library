package elcom.nna.ehotel.library.player.core;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.SurfaceView;

import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;

import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;

import elcom.nna.ehotel.library.player.model.IVideo;
import elcom.nna.ehotel.library.player.model.VideoItem;

/**
 * Created by Ann on 11/22/16.
 */

public abstract class AbstractCorePlayer extends SurfaceView{

    public final static int TIMESEEK = 30*1000;

    int index;
    List<IVideo> videos;
    public OnEventListener mOnEventListener;

    public AbstractCorePlayer(Context context) {
        super(context);
        initUI(context, null);
    }

    public AbstractCorePlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context, attrs);
    }

    public AbstractCorePlayer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AbstractCorePlayer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        if(!isInEditMode()){
            initUI(context, attrs);
        }
    }

    abstract void initUI(Context context, AttributeSet attributeSet);

    public abstract void replay();

    public abstract void play();

    public abstract void stop();

    public abstract void pause();

    public abstract void seek(long milisecond);

    public abstract long getCurrent();

    public abstract long getLength();

    public void setMediaSource(MediaSource mediaSource){

        if(!(this instanceof ExoCorePlayer  || this instanceof ExoCorePlayer2)){
            throw new IllegalArgumentException("Only support Exoplayer");
        }

    }
    public void setMultiSource(String ... urls){

        if(!(this instanceof ExoCorePlayer || this instanceof ExoCorePlayer2)){
            throw new IllegalArgumentException("Only support Exoplayer");
        }

    }

    public void setData(@NonNull List<? extends IVideo> videos) {

        this.videos = (List<IVideo>) videos;
        index = 0;

        //check video playing
        for (int i = 0; i < videos.size(); i++) {
            if(videos.get(i).isPlaying()){
                index = i;
                break;
            }
        }

    }

    public void setData(@NonNull IVideo video) {

        List<IVideo> videoItems = new ArrayList<>();
        videoItems.add(video);
        setData(videoItems);

    }

    public void setVideoPath(String url) {

        VideoItem videoItem =new VideoItem();
        videoItem.setUrl(url);
        videoItem.setName(url);

        setData(videoItem);

    }

    public void next() {
        if(++index >= videos.size() ){
            index = 0;
        }
        play();
    }

    public void prev() {

        if(--index < 0 ){
            index = videos.size() - 1;
        }
        play();

    }

    public abstract boolean isPlaying();

    public void rewind() {

        seek(getCurrent() - TIMESEEK);

    }

    public void fastforward() {

        seek(getCurrent() + TIMESEEK);

    }

    public void nextTrackAudio(){

    }



    public interface OnEventListener {

        void OnPrepared();

        void OnStartedItem(IVideo videoItem);

        void OnPlayingItem();

        void OnStopedItem();

        void OnErrorItem();

        void OnEventChangeSize(int width, int height);

        void OnEventComplete();

    }

    public void setOnEventListener(OnEventListener mOnEventListener) {
        this.mOnEventListener = mOnEventListener;
    }
}
