package elcom.nna.ehotel.library.player.core;

import android.content.Context;

/**
 * Created by Ann on 2/10/17.
 */

public abstract class AbstractAudioKaraokePlayer {

    public Context context;
    public boolean playing;

    public String urlA, urlB;
    public int idPlayed;

    public AbstractAudioKaraokePlayer(Context context) {
        this.context = context;
    }

    abstract void setSource(String urlA, String urlB);
    abstract void stopImpl();
    abstract void pauseImpl();
    abstract void releaseImpl();
    abstract void nextTrackImpl();
    abstract void seekAndPlayImpl(long us);
    abstract void seek(long us);
    abstract long getCurrentMs();

    public void stop(){

        playing = false;
        stopImpl();

    }

    public void release(){

        playing = false;
        releaseImpl();

    }

    public void play(boolean playing){

        this.playing = playing;
        pauseImpl();

    }

    public void seekAndPlay(long us){
        playing = true;
        seekAndPlayImpl(us);
    }

    public void nextTrack(){

        if(idPlayed == 0) idPlayed = 1;
        else idPlayed = 0;

        nextTrackImpl();
    }

    public int getIdPlayed() {
        return idPlayed;
    }

    public void setIdPlayed(int idPlayed) {
        this.idPlayed = idPlayed;
    }

    public void setTrackAudio(String urlA, String urlB, int idPlayed){

        this.idPlayed = idPlayed;
        this.urlA = urlA;
        this.urlB = urlB;
        setSource(urlA, urlB);

    }




}
