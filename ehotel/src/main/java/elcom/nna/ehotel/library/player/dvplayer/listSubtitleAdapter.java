package elcom.nna.ehotel.library.player.dvplayer;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import elcom.nna.ehotel.library.R;

public class listSubtitleAdapter extends BaseAdapter{
     private ArrayList<SubtitleItem> contains;
     private LayoutInflater inflater; 
     private ViewHolder holder; 
	 public listSubtitleAdapter(Activity activity, ArrayList<SubtitleItem> contains) {
		 //super();         
		 this.contains = contains;
	     this.inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	 } 
	 
	 
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return contains.size(); 
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return contains.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	public static class ViewHolder  
	{  
        ImageView imgView;  
        TextView txtView1;  
        LinearLayout layout;
	}  
	private ColorStateList newColorStateList( int colorDefault, int colorPressed){
		return new ColorStateList(
    			new int[][]{new int [] {android.R.attr.state_pressed},new int [0]},
    			new int []{colorPressed,colorDefault});
	}
	@Override
	public View getView( final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
        /*if(convertView==null)  
        {  
            holder = new ViewHolder();  
            convertView = inflater.inflate(R.layout.list_item, null);  
            holder.layout = (LinearLayout)convertView.findViewById(R.id.listMediaItem);
            holder.imgView = (ImageView) convertView.findViewById(R.id.image);  
            holder.txtView1 = (TextView) convertView.findViewById(R.id.text1);  
            //holder.txtView1.setTextColor(newColorStateList(R.color.white, R.color.blue));
            convertView.setTag(holder);  
        }  
        else  
            holder=(ViewHolder)convertView.getTag();  
        
        holder.txtView1.setText(contains.get(position).text());
        holder.imgView.setImageDrawable(contains.get(position).drawable());
        holder.imgView.setVisibility(View.GONE);
        holder.txtView1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mItemClickListener != null){
					mItemClickListener.onItemClick(position, -1, -1);
				}
			}
		});
        holder.txtView1.setFocusable(true);
        holder.txtView1.setClickable(true);
        return convertView;  */
		
		View vi = convertView;
		if (convertView == null) {
			vi = inflater.inflate(R.layout.list_item, null);
		}
		ImageView img = (ImageView) vi.findViewById(R.id.image);
		TextView	text = (TextView) vi.findViewById(R.id.text1);
		SubtitleItem item = contains.get(position);
		text.setText(item.text());		
		img.setTag(item.drawable());
		img.setVisibility(View.GONE);

        if(item.isSelected){
            text.setTextColor(Color.RED);
        }else{
            text.setTextColor(Color.WHITE);
        }
		return vi;
		
        
	}
    
	private OnActionItemClickListener mItemClickListener;
	public void setOnActionItemClickListener(OnActionItemClickListener listener) {
		mItemClickListener = listener;
	}
	public interface OnActionItemClickListener {
		public abstract void onItemClick(int pos, int actionId, int state);
	}
	
}
