package elcom.nna.ehotel.library.player.core;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;

import java.util.List;

import elcom.nna.ehotel.library.player.core.helper.TrackSelectionHelper;
import elcom.nna.ehotel.library.player.model.IVideo;

/**
 * Created by Ann on 11/22/16.
 */

public class ExoCorePlayer extends AbstractCorePlayer {

    private final static int TYPE_AUDIO = 2; //0: default, 1: exo , 2:vlc

    Handler mainHandler;
    BandwidthMeter bandwidthMeter;
    TrackSelection.Factory videoTrackSelectionFactory;
    DefaultTrackSelector trackSelector;
    private TrackSelectionHelper trackSelectionHelper;
    SimpleExoPlayer player;
    AbstractAudioKaraokePlayer exoAudioPlayerVoc;
    AbstractAudioKaraokePlayer exoAudioPlayerArtist;

    private String userAgent;
    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
    private DataSource.Factory mediaDataSourceFactory;
    private int trackAudioId;

    private boolean isManualSelectTrack = false;
    private int numTrackAudio = 0;
    private int trackSelected =  0;


    public ExoCorePlayer(Context context) {
        super(context);
    }

    @Override
    void initUI(Context context, AttributeSet attributeSet) {

        mainHandler = new Handler();
        bandwidthMeter = new DefaultBandwidthMeter();
        videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
        trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        trackSelectionHelper = new TrackSelectionHelper(trackSelector, videoTrackSelectionFactory);

        LoadControl loadControl = new DefaultLoadControl();
        userAgent =  Util.getUserAgent(getContext(), "player");
        mediaDataSourceFactory = buildDataSourceFactory(true);
        player = ExoPlayerFactory.newSimpleInstance(context, trackSelector, loadControl, null, SimpleExoPlayer.EXTENSION_RENDERER_MODE_ON);
        player.setVideoListener(new SimpleExoPlayer.VideoListener() {
            @Override
            public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

                mOnEventListener.OnEventChangeSize(width, height);

            }

            @Override
            public void onRenderedFirstFrame() {
                mOnEventListener.OnPlayingItem();

            }

        });


        player.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                switch (playbackState){

                    case ExoPlayer.STATE_READY:
                        getTrack();
                        break;
                    case ExoPlayer.STATE_ENDED:
                        mOnEventListener.OnEventComplete();

                        if(exoAudioPlayerArtist != null) {
                            exoAudioPlayerArtist.stop();
                        }

                        if(exoAudioPlayerVoc != null) {
                            exoAudioPlayerVoc.stop();
                        }




                        break;
                }

            }

            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {

            }


            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

                error.printStackTrace();
                mOnEventListener.OnErrorItem();

            }

            @Override
            public void onPositionDiscontinuity() {

            }
        });

        player.setVideoSurfaceView(this);

    }

    @Override
    public void replay() {

        player.setPlayWhenReady(true);

        if(isManualSelectTrack){

            if(numTrackAudio == 2){

                if(trackSelected == 1){

                    exoAudioPlayerVoc.seekAndPlay(player.getCurrentPosition());

                }else {

                    exoAudioPlayerArtist.seekAndPlay(player.getCurrentPosition());
                }

            }else if(numTrackAudio == 1){
                exoAudioPlayerVoc.seekAndPlay(player.getCurrentPosition());

            }
        }


    }

    @Override
    public void setData(@NonNull List<? extends IVideo> videos) {
        super.setData(videos);

        play();

    }

    private static final String TAG = "ExoCorePlayer";
    @Override
    public void play() {

        String url = videos.get(index).getUrl();
        Log.d(TAG, "play " + url);
        if(url == null){
            return;
        }

        Uri uri = Uri.parse(url);
        String extension = url.substring(url.lastIndexOf(".") + 1);
        MediaSource videoSource = buildMediaSource(uri, extension);


        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        mOnEventListener.OnStartedItem(videos.get(index));

    }


    /**
     * Returns a new DataSource factory.
     *
     * @param useBandwidthMeter Whether to set  as a listener to the new
     *     DataSource factory.
     * @return A new DataSource factory.
     */
    private DataSource.Factory buildDataSourceFactory(boolean useBandwidthMeter) {
        return new DefaultDataSourceFactory(getContext(), useBandwidthMeter ? BANDWIDTH_METER : null,
                buildHttpDataSourceFactory(useBandwidthMeter));
    }

    /**
     * Returns a new HttpDataSource factory.
     *
     * @param useBandwidthMeter Whether to set  as a listener to the new
     *     DataSource factory.
     * @return A new HttpDataSource factory.
     */
    private HttpDataSource.Factory buildHttpDataSourceFactory(boolean useBandwidthMeter) {
        return new DefaultHttpDataSourceFactory(userAgent, useBandwidthMeter ? BANDWIDTH_METER : null);
    }


    private MediaSource buildMediaSource(Uri uri, String overrideExtension) {
        int type = Util.inferContentType(!TextUtils.isEmpty(overrideExtension) ? "." + overrideExtension
                : uri.getLastPathSegment());
        switch (type) {
            case C.TYPE_SS:
                return new SsMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultSsChunkSource.Factory(mediaDataSourceFactory), mainHandler, null);
            case C.TYPE_DASH:
                return new DashMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultDashChunkSource.Factory(mediaDataSourceFactory), mainHandler, null);

            case C.TYPE_HLS:
                return new HlsMediaSource(uri, mediaDataSourceFactory, mainHandler, null);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource(uri, mediaDataSourceFactory, new DefaultExtractorsFactory(),
                        mainHandler, null);
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }
        }

    }

    @Override
    public void stop() {

        player.stop();

        if(exoAudioPlayerVoc != null){
            exoAudioPlayerVoc.stop();
        }

        if(exoAudioPlayerArtist != null){
            exoAudioPlayerArtist.stop();
        }

    }

    @Override
    public void pause() {

        player.setPlayWhenReady(false);

        if(isManualSelectTrack){
            if(numTrackAudio == 2){

                exoAudioPlayerArtist.play(false);
                exoAudioPlayerVoc.play(false);

            }else if(numTrackAudio == 1){

                exoAudioPlayerVoc.play(false);

            }
        }


    }

    @Override
    public void seek(long milisecond) {

        player.seekTo(milisecond);

    }

    @Override
    public long getCurrent() {
        return player.getCurrentPosition();
    }

    @Override
    public long getLength() {
        return player.getDuration();
    }

    @Override
    public boolean isPlaying() {
        return player.getPlayWhenReady();
    }

    public void nextAudio(){

        if(!isManualSelectTrack) {

            trackSelectionHelper.nextTrack(trackSelector.getCurrentMappedTrackInfo(), trackAudioId);

        }else {

            if(numTrackAudio == 1) return;

            if(trackSelected == 1){

                exoAudioPlayerVoc.play(true);
                exoAudioPlayerArtist.seekAndPlay(player.getCurrentPosition());
                trackSelected = 2;

            }else {

                trackSelected = 1;
                exoAudioPlayerArtist.play(true);
                exoAudioPlayerVoc.seekAndPlay(player.getCurrentPosition());

            }
        }



    }

    private void getTrack(){

        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        if (mappedTrackInfo == null) {
            return;
        }

        for (int i = 0; i < mappedTrackInfo.length; i++) {
            TrackGroupArray trackGroups = mappedTrackInfo.getTrackGroups(i);
            if (trackGroups.length != 0) {
                switch (player.getRendererType(i)) {
                    case C.TRACK_TYPE_AUDIO:
                        trackAudioId = i;
                        break;
                    case C.TRACK_TYPE_VIDEO:
                        break;
                    case C.TRACK_TYPE_TEXT:
                        break;
                    default:
                        continue;
                }
            }
        }
    }

    @Override
    public void setMultiSource(String... urls) {
        super.setMultiSource(urls);

        MediaSource mediaSource =  new ExtractorMediaSource(Uri.parse(urls[0]), mediaDataSourceFactory,  new DefaultExtractorsFactory(), null, null);
        player.prepare(mediaSource);

        if (urls.length == 1){

            isManualSelectTrack  = false;

        }else if(urls.length == 2) { // only has audio karaoke

            isManualSelectTrack  = true;

            if(exoAudioPlayerVoc == null) {
                if(TYPE_AUDIO == 0) exoAudioPlayerVoc = new MediaAudioKaraokePlayer(getContext());
                else if (TYPE_AUDIO == 1) exoAudioPlayerVoc = new ExoAudioKaraokePlayer(getContext());
//                else if (TYPE_AUDIO == 2) exoAudioPlayerVoc = new VlcAudioKaraokePlayer(getContext());
            }

            exoAudioPlayerVoc.setTrackAudio(urls[1], urls[1], 0);

        }else if (urls.length == 3){ // has karaoke and artist audio

            isManualSelectTrack  = true;

            trackSelected = 1;
            if(exoAudioPlayerVoc == null) {
                if(TYPE_AUDIO == 0) exoAudioPlayerVoc = new MediaAudioKaraokePlayer(getContext());
                else if (TYPE_AUDIO == 1) exoAudioPlayerVoc = new ExoAudioKaraokePlayer(getContext());
//                else if (TYPE_AUDIO == 2) exoAudioPlayerVoc = new VlcAudioKaraokePlayer(getContext());
            }
            exoAudioPlayerVoc.setTrackAudio(urls[1], urls[1], 0);

            if (exoAudioPlayerArtist == null) {
                if(TYPE_AUDIO == 0) exoAudioPlayerArtist = new MediaAudioKaraokePlayer(getContext());
                else if (TYPE_AUDIO == 1) exoAudioPlayerArtist = new ExoAudioKaraokePlayer(getContext());
//                else if (TYPE_AUDIO == 2) exoAudioPlayerArtist = new VlcAudioKaraokePlayer(getContext());
            }

            exoAudioPlayerArtist.setTrackAudio(urls[2], urls[1], 0);

        }

        numTrackAudio = urls.length - 1;
        player.setPlayWhenReady(true);
        mOnEventListener.OnStartedItem(null);

    }

    public void setMultiSourceNative(String... urls){

        MediaSource[] mediaSources = new MediaSource[urls.length];

        for (int i = 0; i < urls.length; i++) {
            Uri uriData = Uri.parse(urls[i]);
            mediaSources[i] =  new ExtractorMediaSource(uriData, mediaDataSourceFactory,  new DefaultExtractorsFactory(), null, null);
        }
        MergingMediaSource mergedSource = new MergingMediaSource(mediaSources);
        player.prepare(mergedSource);

    }

    @Override
    public void setMediaSource(MediaSource mediaSource) {
        super.setMediaSource(mediaSource);
        player.prepare(mediaSource);
        player.setPlayWhenReady(true);

        mOnEventListener.OnStartedItem(null);

    }

    @Override
    public void nextTrackAudio() {
        super.nextTrackAudio();
        nextAudio();
    }
}
