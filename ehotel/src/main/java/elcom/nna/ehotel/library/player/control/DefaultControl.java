package elcom.nna.ehotel.library.player.control;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import elcom.nna.ehotel.library.R;

/**
 * Created by Ann on 11/14/16.
 */

public class DefaultControl extends RelativeLayout implements IControl {


    public DefaultControl(Context context) {
        super(context);
        if(!isInEditMode()){
            init(context, null);
        }
    }

    public DefaultControl(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()){
            init(context, attrs);
        }
    }

    public DefaultControl(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if(!isInEditMode()){
            init(context, attrs);
        }

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DefaultControl(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        if(!isInEditMode()){
            init(context, attrs);
        }
    }

    private void init(Context context, AttributeSet attrs){


        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.control_default, null, true);
        tvTitle = (TextView)view.findViewById(R.id.tvTitle) ;
        tvTimeCurrent = (TextView)view.findViewById(R.id.tvCurrent) ;
        tvTimeDuration = (TextView)view.findViewById(R.id.tvDuration) ;
        seekBar = (SeekBar) view.findViewById(R.id.seekbar) ;

        addView(view);

    }
    TextView tvTitle, tvTimeCurrent, tvTimeDuration;
    SeekBar seekBar;


    @Override
    public int getResPlay() {
        return 0;
    }

    @Override
    public int getResPause() {
        return 0;
    }

    @Override
    public View viewTitle() {
        return tvTitle;
    }

    @Override
    public View viewTimeDuration() {
        return tvTimeDuration;
    }

    @Override
    public View viewTimeCurrent() {
        return tvTimeCurrent;
    }

    @Override
    public SeekBar seekBar() {
        return seekBar;
    }

    @Override
    public View controlPlay() {
        return null;
    }

    @Override
    public void setVisible(boolean show) {

        if(show){
            setVisibility(VISIBLE);
        }else {
            setVisibility(GONE);
        }

    }
}
