package elcom.nna.ehotel.library.menuview.grid;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import elcom.nna.ehotel.library.menuview.ICoreMenu;
import elcom.nna.ehotel.library.menuview.IOnEventListener;
import elcom.nna.ehotel.library.menuview.model.IMenu;

/**
 * Created by Ann on 4/29/16.
 */
public class CoreGrid implements ICoreMenu {


    private final Context context;

    private final RecyclerView rv;

    private GridAdapter gridAdapter;

    private GridAutofitLayoutManager manager;

    private int columnWidth = -1;
    private int columnHeight = -1;
    private final int colorBgText;
    private final int colorText;


    public CoreGrid(ViewGroup viewGroup, int columnWidth, int columnHeight,
                    int colorBgText, int colorText){

        this.columnWidth = columnWidth;
        this.columnHeight = columnHeight;
        this.colorBgText = colorBgText;
        this.colorText = colorText;

        this.context = viewGroup.getContext();
        rv = new RecyclerView(viewGroup.getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rv.setLayoutParams(params);
        viewGroup.addView(rv);

        initRecycecleView();

    }

    public void setSpanCount(int spanCount){

        manager.setSpanCount(spanCount);
    }

    private void initRecycecleView(){

        manager = new GridAutofitLayoutManager(context, columnWidth);
        rv.setLayoutManager(manager);
        rv.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
        gridAdapter = new GridAdapter(columnWidth, columnHeight, colorBgText, colorText);

        rv.setAdapter(gridAdapter);

    }

    @Override
    public void setData(List<? extends IMenu> menus) {

        gridAdapter.setData(menus);

    }

    @Override
    public IMenu getItem(int post) {

        return gridAdapter.getItem(post);

    }

    @Override
    public List<? extends IMenu> getItems() {
        return gridAdapter.getItems();
    }

    @Override
    public void setOnEventListenner(IOnEventListener onEventListener) {

        gridAdapter.setOnItemClickListener(onEventListener);

    }

    @Override
    public void setSelection(int index) {

        rv.scrollToPosition(index);
        rv.getLayoutManager().scrollToPosition(index);


    }


}
