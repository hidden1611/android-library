package org.videolan.vlc;

import android.content.Context;
import android.content.res.Resources;


import org.videolan.vlc.util.Strings;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ann on 4/22/16.
 */
public class VLCApplication {

    public final static String SLEEP_INTENT = Strings.buildPkgString("SleepIntent");
    public static Context context;

    private static ThreadPoolExecutor mThreadPool = new ThreadPoolExecutor(0, 2, 2, TimeUnit.SECONDS,
            new LinkedBlockingQueue<Runnable>());

    public static Context getAppContext(){

        return context;

    }

    public static Resources getAppResources()
    {
        return context.getResources();
    }

    public static void runBackground(Runnable runnable) {
        mThreadPool.execute(runnable);
    }
}
