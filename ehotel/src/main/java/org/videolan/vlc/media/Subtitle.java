package org.videolan.vlc.media;

import android.os.AsyncTask;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Ann on 4/23/16.
 */
public class Subtitle {

    String name;
    String url;
    String path;

//    public Subtitle(Element element){
//
//        name = element.select("name_lang").text();
//        url = element.select("url_lang").text();
//        String name = url.substring(url.lastIndexOf("/"),url.length());
//        path = Environment.getExternalStorageDirectory() + "/eHome" + name;
//        downloadSub();
//
//    }

    private static final String TAG = "Subtitle";
    private void downloadSub(){
        AsyncHttpClient client = new AsyncHttpClient();
        final File file = new File(path);
//        if(file.exists()){
//            file.delete();
//        }
//        file.deleteOnExit();
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "downloadSub " + url);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    FileUtils.copyURLToFile(new URL(url),file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //Open a URL Stream
//                try {
//                    Connection.Response resultImageResponse = Jsoup.connect(url)
//                            .ignoreContentType(true).execute();
//
//                    Log.d(TAG, "doInBackground " + resultImageResponse.charset());
//
//                    BufferedWriter out = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(file), "UTF-8" ));
//                    out.write(new String(resultImageResponse.bodyAsBytes(),"UTF-8"));
//                    out.close();
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

                return null;
            }
        }.execute();
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getPath() {
        return path;
    }
}
