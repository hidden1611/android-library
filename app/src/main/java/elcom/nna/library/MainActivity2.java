//package elcom.nna.library;
//
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by Ann on 7/24/16.
// */
//public class MainActivity2 extends AppCompatActivity {
//
//
//    private RecyclerView rv;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceStat) {
//        super.onCreate(savedInstanceStat);
//        setContentView(R.layout.activity_main2);
//
//        List<SUBJECT> subjects = new ArrayList<>();
//        rv = (RecyclerView)findViewById(R.id.rv);
//
//        for (int i = 0; i < 10; i++) {
//            SUBJECT subject = new SUBJECT();
//            subject.setName("Name:" + i);
//
//            List<ITEM> items = new ArrayList<>();
//            for (int j = 0; j < 10; j++) {
//
//                ITEM item = new ITEM("item: " + j,
//                        "http://img.zing.vn/upload/tlbb360/source/News/Cosplay/aChau6.jpg");
//
//                items.add(item);
//
//            }
//
//            subject.setItems(items);
//            subjects.add(subject);
//        }
//        Log.d(TAG, "onCreate " + subjects.size());
//
////
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        rv.setLayoutManager(linearLayoutManager);
//        rv.setHasFixedSize(true);
//        TestAdapter testAdapter = new TestAdapter();
//        testAdapter.addItems(subjects);
//        rv.setAdapter(testAdapter);
//
//
//    }
//
//    private static final String TAG = "MainActivity2";
//
//
//}
