package elcom.nna.library;

import java.util.List;

import elcom.nna.ehotel.library.gridview.IEgvCell;
import elcom.nna.ehotel.library.gridview.IEgvLine;

public class SUBJECT implements IEgvLine {

        List<ITEM> items;
        String name;

        public List<ITEM> getItems() {
            return items;
        }

        public void setItems(List<ITEM> items) {
            this.items = items;
        }

    @Override
    public List<? extends IEgvCell> getCells() {
        return items;
    }

    public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }