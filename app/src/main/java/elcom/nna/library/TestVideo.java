package elcom.nna.library;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;

import elcom.nna.ehotel.library.player.dvplayer.DVPlayer;
import elcom.nna.ehotel.library.player.dvplayer.Video;

/**
 * Created by Ann on 4/8/16.
 */
public class TestVideo extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<Channel> videos = new ArrayList<>();
        for(int i=0; i< 5; i++) {
            Channel videoCur = new Channel();
            videoCur.setUrl("http://172.16.9.141/vod_hotel/mp4/1563.mp4");
            videoCur.setTitle("Test" + i);
            if(i == 3) {
                videoCur.setPlay(true);
            }else {
                videoCur.setPlay(false);
            }
            videoCur.setType(Video.TV);
            videos.add(videoCur);
        }
        Intent ii = new Intent(this, DVPlayer.class);
        ii.putParcelableArrayListExtra("BUNDLE_LIST_VIDEO", videos);
        startActivity(ii);
//        VideoPlayerActivity.start();

    }
}
