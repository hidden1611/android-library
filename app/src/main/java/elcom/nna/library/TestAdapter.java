package elcom.nna.library;

/**
 * Created by Ann on 7/24/16.
 */

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import elcom.nna.ehotel.library.menuview.EMenuView;
import elcom.nna.ehotel.library.menuview.IOnEventListener;


public class TestAdapter extends RecyclerView.Adapter<TestAdapter.VH> {

    private List<SUBJECT> items;

    public TestAdapter() {
        this.items = new ArrayList<>();
    }

    public void addItems(List<SUBJECT> item) {
        items = item;
        notifyDataSetChanged();
    }

    public void addItem(SUBJECT item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public SUBJECT getItem(int index) {
        return items.get(index);
    }

    public List<SUBJECT> getItems() {
        return items;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }


    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new VH(v);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {

        holder.setValue(items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class VH extends RecyclerView.ViewHolder {



        TextView tvTitle;
        EMenuView ev;
        public VH(View itemView) {
            super(itemView);
            tvTitle = (TextView)itemView.findViewById(R.id.tvTitle);
            ev = (EMenuView)itemView.findViewById(R.id.ev);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.OnClick(v, getLayoutPosition());
                    }
                }
            });

            ev.setOnEventListener(new IOnEventListener() {
                @Override
                public void onClicked(View view, int pos) {

                }

                @Override
                public void onSelected(View view, int pos) {

                    Log.d(TAG, "onSelected " + pos);

                }
            });
        }

        public void setValue(SUBJECT item) {

            tvTitle.setText(item.getName());
            ev.setData(item.getItems());

        }
    }

    private static final String TAG = "TestAdapter";


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public static OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void OnClick(View view, int position);
    }
}
