package elcom.nna.library;

import elcom.nna.ehotel.library.menuview.model.IMenu;

/**
 * Created by nna on 17/06/2016.
 */
public class MenuTest implements IMenu {
    String name;
    String image;

    public void setName(String name) {
        this.name = name;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public String getUrlIcon() {
        return image;
    }

    @Override
    public boolean isSelected() {
        return false;
    }
}
