package elcom.nna.library;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;

import com.google.android.exoplayer2.audio.Ac3Util;
import com.google.android.exoplayer2.source.MediaSource;

import java.io.IOException;

import elcom.nna.ehotel.library.player.EPlayerView;
import elcom.nna.ehotel.library.player.EPlayerView2;
import elcom.nna.ehotel.library.player.core.SupperpoweredAudio;

/**
 * Created by nna on 14/09/2016.
 */
public class TestEPlayerView extends Activity {

    private EPlayerView2 eplayerview;
//    String PATH = "/mnt/sdcard/";
    String PATH = "/mnt/sda/sda2/skplayer.vn/";

    String[] group1 = new String[]{PATH + "8051531.mkv",
            PATH + "8051531.voc"};
    String[] group2 = new String[]{PATH + "/8063218.mkv" ,
            PATH + "8063218.kar",
            PATH + "8063218.voc"};
    String[] group32 = new String[]{PATH + "/8054770.mkv" ,
            PATH + "8054770.kar",
            PATH + "8054770.voc"};


    int index =1 ;

    MediaPlayer mediaPlayer;
    SupperpoweredAudio supperpoweredAudio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_testeplayerview);
        findViewById(R.id.but).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                eplayerview.nextTrackAudio();
//                eplayerview.seek(eplayerview.getCurrent() + 10000);
//                eplayerview.stop();

                eplayerview.stop();
                if(++index % 2 == 0) {
                    eplayerview.setVideoPath(group1);
                }else {
                    eplayerview.setVideoPath(group2);
                }
            }
        });

        findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(eplayerview.isPlaying()){
//                    eplayerview.pause();
//                }else{
//                    eplayerview.resume();
//                }
//                if(isPlaying){
//                    mediaPlayer.pause();
//                }else {
//                    mediaPlayer.start();
//
//                }
                Log.d(TAG, "onClick " + supperpoweredAudio.getCurrentMs());
                isPlaying = !isPlaying;
                supperpoweredAudio.play(isPlaying);
//                if(eplayerview.getVisibility() == View.GONE) {
//                    eplayerview.setVisibility(View.VISIBLE);
//                }else {
//                    eplayerview.setVisibility(View.GONE);
//
//                }
//                eplayerview.seek(eplayerview.getCurrent() + 10000);
//                eplayerview.stop();
            }
        });


        eplayerview = (EPlayerView2)findViewById(R.id.ePlayerview);
        eplayerview.setTypePlayer(EPlayerView2.TYPE_DEFAULT_PLAYER);
        eplayerview.setOnEventError(new EPlayerView.OnEventError() {
            @Override
            public void OnErrorItem() {

                Log.d(TAG, "OnErrorItem ");

            }
        });
//        eplayerview.setVideoPath("/mnt/sda/sda2/skplayer.vn/8073061.VOB");
//        eplayerview.setVideoPath("/mnt/sda/sda2/skplayer.vn/8073062.mkv");
//        eplayerview.setVideoPath("/mnt/sda/sda2/skplayer.vn/8071383.mkv"); //error
//        eplayerview.setVideoPath(new String[]{"/mnt/sda/sda2/skplayer.vn/8057509.kar"}); //cannot play
//        eplayerview.setVideoPath("file:///mnt/sda/sda2/skplayer.vn/8053195.voc");
//        eplayerview.setVideoPath(new String[]{"/mnt/sda/sda2/skplayer.vn/8050696.mkv" ,
//                "/mnt/sda/sda2/skplayer.vn/8050696.voc",
//                "/mnt/sda/sda2/skplayer.vn/8050696.kar"});

//        eplayerview.setVideoPath(PATH + "8069900.mkv");
//        eplayerview.setVideoPath(group32);
//        eplayerview.setVideoPath("/mnt/sda/sda2/skplayer.vn/mp3/50060.mp3");
        verifyStoragePermissions(this);
//        mediaPlayer = new MediaPlayer();
//        try {
//            mediaPlayer.setDataSource("/mnt/sda/sda2/skplayer.vn/mp3/50060.mp3");
//            mediaPlayer.prepare();
//            mediaPlayer.start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        supperpoweredAudio = new SupperpoweredAudio(this);
        supperpoweredAudio.setSource("/mnt/sda/sda2/skplayer.vn/mp3/50060.mp3", null);

    }

    boolean isPlaying = true;

    private static final String TAG = "TestEPlayerView";

    @Override
    protected void onStop() {
        super.onStop();
        eplayerview.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        eplayerview.stop();
    }

    // Storage Permissions variables
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    //persmission method.
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have read or write permission
        int writePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

}
