package elcom.nna.library;

import elcom.nna.ehotel.library.SlideView.ISlideViewItem;

/**
 * Created by nna on 25/07/2016.
 */
public class SvData implements ISlideViewItem {
    String name;
    String image;
    boolean isSelected;

    public SvData(String name, String image) {
        this.name = name;
        this.image = image;
        isSelected = false;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public void setSelected(boolean value) {
        this.isSelected = value;

    }
}
