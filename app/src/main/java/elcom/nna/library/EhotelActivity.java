package elcom.nna.library;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import elcom.nna.ehotel.library.dialog.AlertDialogFragment;
import elcom.nna.ehotel.library.player.EPlayerView;
import elcom.nna.ehotel.library.player.core.ICorePlayer;
import elcom.nna.ehotel.library.player.model.VideoItem;

/**
 * Created by Ann on 4/10/16.
 */
public class EhotelActivity extends AppCompatActivity {
    EPlayerView ePlayerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ehotel);
        ePlayerView = (EPlayerView) findViewById(R.id.ePlayer);
        final List<VideoItem> videos = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            VideoItem video = new VideoItem();
            video.setName("aaa");
            video.setUrl("http://172.16.9.59/film/arrow-ss1/S01E01-%20Pilot.mkv");
            videos.add(video);
        }

        videos.get(0).setPlaying(true);
        ePlayerView.setTypePlayer(0);
        ePlayerView.loadMedia(videos);
//        ePlayerView.setOnEventError(new ICorePlayer.OnEventError() {
//            @Override
//            public void OnErrorItem() {
//                Toast.makeText(getApplicationContext(),"errr",Toast.LENGTH_LONG).show();;
//            }
//        });

        ePlayerView.setSubtile("http://172.16.9.141/vod_hotel/mp4/1493_vn.srt");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case KeyEvent.KEYCODE_PAGE_UP:
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                ePlayerView.fastforward();
                return true;
            case KeyEvent.KEYCODE_MEDIA_REWIND:
            case KeyEvent.KEYCODE_PAGE_DOWN:
                ePlayerView.rewind();
                return true;
            case KeyEvent.KEYCODE_MENU:
                new AlertDialogFragment.Builder(this)
                        .setTitle("Setting")
                        .setItems(new String[]{"aaa", "bbbb", "subtitle size"}, new DialogInterface.OnClickListener(){


                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        ePlayerView.stop();
                                        break;

                                    case 1:
                                        ePlayerView.start();
                                        break;

                                    case 2:
                                        ePlayerView.setSizeSubtitle(30);
                                        ePlayerView.setColorSubtitle(0xFFFF0000);
                                        break;
                                }
                            } })
                        .show();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //    private void testMpView(){
//        MPView mpView = (MPView)findViewById(R.id.mpview);
//        mpView.setOnChangeItemListener(new IOnChangeItemListener() {
//            @Override
//            public void onChanged(IMusicItem iMusicItem) {
//
//            }
//        });
//    }
//    private void testMenuView(){
//        final EMenuView menuView = (EMenuView)findViewById(R.id.menuView);
//        List<IMenu> menus = new ArrayList<>();
//
///db /        for (int i = 0; i < 20; i++) {
//
//            Menu menu = new Menu("" + i,"1234","http://img.blogtamsu.vn/2015/10/ha-vi-blogtamsu32.jpg");
//            menus.add(menu);
//
//        }
//
//        menuView.setData(menus);
//
//        menuView.setOnEventListener(new IOnEventListener() {
//
//            @Override
//            public void onClicked(View view, int pos) {
//
//                toast("" + menuView.getItem(pos));
//
//            }
//
//            @Override
//            public void onSelected(View view, int pos) {
//
//                toast("" + pos);
//
//            }
//        });
//    }
    private void toast(String ss){
        Toast.makeText(EhotelActivity.this, "" + ss, Toast.LENGTH_SHORT).show();
    }


}

