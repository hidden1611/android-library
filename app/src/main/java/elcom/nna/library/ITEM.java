package elcom.nna.library;

import elcom.nna.ehotel.library.gridview.IEgvCell;

public class ITEM implements IEgvCell {

        private String name;
        private String url;

        public ITEM( String name, String url) {
            this.url = url;
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getId() {
            return null;
        }

        @Override
        public String getUrlIcon() {
            return url;
        }

        @Override
        public boolean isSelected() {
            return false;
        }
    }