package elcom.nna.library;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import elcom.nna.ehotel.library.menuview.EMenuView;
import elcom.nna.ehotel.library.menuview.IOnEventListener;
import elcom.nna.ehotel.library.mpview.IMusicItem;
import elcom.nna.ehotel.library.mpview.MPView;


public class MainActivity extends AppCompatActivity {

//    Update update;
    private EMenuView rv;
    private MPView mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv = (EMenuView) findViewById(R.id.menuview);
        mp = (MPView) findViewById(R.id.mp);
        List<MenuTest> items = new ArrayList<>();
        for(int i = 0; i < 30; i++) {
            MenuTest item = new MenuTest();
            item.setName("aaa");
            item.setImage("http://ebop01.elcom.tv:8989/ebop/resources/image/home/menu/Special-Program.png");
            items.add(item);
        }
        rv.setData(items);
        rv.setSelection(0);
        rv.setOnEventListener(new IOnEventListener() {
            @Override
            public void onClicked(View view, int pos) {
                toast("" + pos);
                mp.playAtindex(pos);

            }

            @Override
            public void onSelected(View view, int pos) {
                toast("" + pos);

            }
        });
//        rv.requestChildFocus();

        List<ItemMusic> itemMusics = new ArrayList<>();
        ItemMusic itemMusic = new ItemMusic("aa", "http://download.s71.stream.nixcdn.com/ba464d6e01a75b00b25ecf37f498df99/579372ea/NhacCuaTui091/HoiThoCuoi-BlackBiftAT_117_kbaw.mp3");
        itemMusics.add(itemMusic);
        itemMusic = new ItemMusic("aa", "http://download.a1.nixcdn.com/87028a64c88a0913783e212b9522d244/579372ea/NhacCuaTui067/MongManh-LBLftGodbinBlackBiSub_ax32.mp3");
        itemMusics.add(itemMusic);
//        mp.setData(itemMusics);


    }

    private static final String TAG = "MainActivity";
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        Log.d(TAG, "onKeyUp " + getCurrentFocus());
        return super.onKeyUp(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void toast(String ss){
        Toast.makeText(MainActivity.this, "" + ss, Toast.LENGTH_SHORT).show();
    }


    class ItemMusic implements IMusicItem{

        String name;
        String url;

        public ItemMusic(String name, String url) {
            this.name = name;
            this.url = url;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getUrl() {
            return url;
        }

        @Override
        public boolean isPlaying() {
            return false;
        }

        @Override
        public void setPlaying(boolean value) {

        }
    }
}

