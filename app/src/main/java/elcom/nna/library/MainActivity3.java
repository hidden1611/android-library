package elcom.nna.library;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import elcom.nna.ehotel.library.SlideView.SlideView;
import elcom.nna.ehotel.library.gridview.EbopGridView;

/**
 * Created by Ann on 7/24/16.
 */
public class MainActivity3 extends AppCompatActivity {


    private EbopGridView rv;
    private SlideView sv;

    @Override
    protected void onCreate(Bundle savedInstanceStat) {
        super.onCreate(savedInstanceStat);
        setContentView(R.layout.activity_main3);
//        rv = (EbopGridView) findViewById(R.id.egv);
//        List<SvData> items = new ArrayList<>();
//        for (int j = 0; j < 10; j++) {
//
//            SvData item = new SvData("item: " + j,
//                    "http://img.zing.vn/upload/tlbb360/source/News/Cosplay/aChau6.jpg");
//
//            items.add(item);
//
//        }
//
//        sv.setData(items, new RecycleItemSelectedListener() {
//            @Override
//            public void onItemFocusedListener(View view, int position) {
//
//            }
//
//            @Override
//            public void onItemSelectedListener(View view, int position) {
//
//            }
//        });

        testEgv();



    }

    private void testEgv(){
        List<SUBJECT> subjects = new ArrayList<>();
        rv = (EbopGridView) findViewById(R.id.egv);
        for (int i = 0; i < 10; i++) {
            SUBJECT subject = new SUBJECT();
            subject.setName("Name:" + i);

            List<ITEM> items = new ArrayList<>();
            for (int j = 0; j < 10; j++) {

                ITEM item = new ITEM("item: " + j,
                        "http://img.zing.vn/upload/tlbb360/source/News/Cosplay/aChau6.jpg");

                items.add(item);

            }

            subject.setItems(items);
            subjects.add(subject);

        }
        Log.d(TAG, "onCreate " + subjects.size());

        rv.setData(subjects);

    }

    private static final String TAG = "MainActivity2";


}
