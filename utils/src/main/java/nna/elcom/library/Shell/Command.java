package nna.elcom.library.Shell;

import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class Command extends AsyncTask<String, Void, Void> {
	public String ip;

	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub
		List<String> result = Shell.SU.run(new String[] { "id", params[0] });

		for (String ss : result) {
			log(ss);
		}
		return null;
	}

	private void log(String ss) {
		Log.d("nna", ss);
	}

}
