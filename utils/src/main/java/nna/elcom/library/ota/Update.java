package nna.elcom.library.ota;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;
import java.io.IOException;

import nna.elcom.library.Shell.Shell;

/**
 * Created by Ann on 4/5/16.
 */
public class Update {

    String linkOTA;
    String packageName;
    Context context;
    Ota ota;
    OnListenerUpdateOTA onListenerUpdateOTA;
    private AsyncHttpClient client;

    public void setOnListenerUpdateOTA(OnListenerUpdateOTA onListenerUpdateOTA){

        this.onListenerUpdateOTA = onListenerUpdateOTA;

    }
    public interface OnListenerUpdateOTA {
        void error();
        void success();

    }

    public Update(Context context,String packageName, String linkOTA){
        this.context = context;
        this.linkOTA = linkOTA;
        this.packageName = packageName;


    }

    public void checkOTA(){

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(linkOTA, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                ota = new Ota(new String(responseBody));

                if(ota.urlapk == null || ota.version == 0 ){
                    onListenerUpdateOTA.error();
                }

                try {
                    PackageInfo pInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                    if(ota.getVersion() > pInfo.versionCode){
                        downFile(ota);
                    }else{
                       onListenerUpdateOTA.success();
                    }

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                    downFile(ota);
                }



            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                onListenerUpdateOTA.error();

            }
        });


    }

    private void downFile(final Ota ota) {

        if(client == null) {
            client = new AsyncHttpClient();
        }

        //create temporary file to store the apk
        File file = new File(Environment.getExternalStorageDirectory(), "_temp.apk");
        if(file.exists()){
            file.delete();
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ota.setFile(file.getAbsolutePath());
        client.get(context,ota.getUrlapk(), new FileAsyncHttpResponseHandler(file) {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, File file) {
                throwable.printStackTrace();
                onListenerUpdateOTA.error();
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, File file) {
                    installApp(ota);
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
            }
        });


    }


    private void installApp(final Ota app){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Shell.SU.run("pm install -r " + app.getFile());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                onListenerUpdateOTA.success();
            }
        }.execute();


    }

}
