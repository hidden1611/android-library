/**
 * 
 */
package nna.elcom.library.videoplayer.control;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import nna.elcom.library.videoplayer.R;


/**
 * http://stackoverflow.com/questions/4817014/animate-a-custom-dialog
 * http://www.londatiga.net/it/how-to-create-quickaction-dialog-in-android/
 * 
 * @author HB
 *
 */
public class DialogConfirm1 extends Dialog {
	private final String 						TAG = DialogConfirm1.class.getSimpleName();
	private Context 							mContext;
	private TextView 							mTextNotify;
	private Button 								btnOK, btnCancel;
	private boolean								showCancel = false;
	private View.OnClickListener	onClickOK, onClickCancel;

	/*private Typeface							mTypeFace;*/
	/**
	 * @return the mTextNotify
	 */
	public TextView getmTextNotify() {
		return mTextNotify;
	}

	/**
	 */
	public void setmTextNotify(String str) {
		this.mTextNotify.setText(str);
	}

	/**
	 * @return the btnOK
	 */
	public Button getBtnOK() {
		return btnOK;
	}

	/**
	 * @param btnOK the btnOK to set
	 */
	public void setBtnOK(Button btnOK) {
		this.btnOK = btnOK;
	}


	/**
	 * @return the btnCancel
	 */
	public Button getBtnCancel() {
		return btnCancel;
	}

	/**
	 * @param btnCancel the btnCancel to set
	 */
	public void setBtnCancel(Button btnCancel) {
		this.btnCancel = btnCancel;
	}

	public DialogConfirm1(Context context) {
		super(context, R.style.StyleDialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mContext = context;
		setContentView(R.layout.dialog_confirm_1);
		setCancelable(true);
		setupView();
	}

	public DialogConfirm1(Context context, boolean show_cancel) {
		super(context, R.style.StyleDialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mContext = context;
//		DAO.init(context);
//	    DAO.setLanguagePage(context, DAO.getLanguage(context));
//		showCancel = show_cancel;
//		setContentView(R.layout.dialog_confirm_1);
		setCancelable(true);
		setupView();
	}

	private void setupView() {

		/*mTypeFace = UtilsE.getTypeFace(mContext);*/

		mTextNotify = (TextView)findViewById(R.id.tvNotify);
		btnOK = (Button)findViewById(R.id.btnOK);
		btnCancel = (Button)findViewById(R.id.btnCancel);
		if(showCancel){
			btnCancel.setVisibility(View.VISIBLE);
			if(onClickCancel != null){
				btnCancel.setOnClickListener(onClickCancel);
			}else{
				btnOK.setOnClickListener(defaultClick);
			}
		}else{
			btnCancel.setVisibility(View.GONE);
		}
		if(onClickOK != null){
			btnOK.setOnClickListener(onClickOK);
		}else{
			btnOK.setOnClickListener(defaultClick);
		}

		/*setupFont();*/
	}

	/*private void setupFont(){
		mTypeFace = UtilsE.getTypeFace(mContext);

		btnOK.setTypeface(mTypeFace);
		btnCancel.setTypeface(mTypeFace);
		mTextNotify.setTypeface(mTypeFace);
	}*/

	private View.OnClickListener defaultClick = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			dismiss();

		}
	};
	public void setOnClickOK(View.OnClickListener onClickOKConfirm) {
		this.onClickOK = onClickOKConfirm;
	}

	public void setOnClickCancel(View.OnClickListener onClickCancelConfirm) {
		this.onClickCancel = onClickCancelConfirm;
	}

	public void showAndSetClick(View.OnClickListener onClickOKConfirm){
		btnOK.setOnClickListener(onClickOKConfirm);
		show();
	}
}
