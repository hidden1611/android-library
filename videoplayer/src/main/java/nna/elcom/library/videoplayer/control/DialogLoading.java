/**
 * 
 */
package nna.elcom.library.videoplayer.control;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import nna.elcom.library.videoplayer.R;


/**
 * @author HB
 * 
 */
public class DialogLoading extends Dialog {
	TextView tvPercent;

	public DialogLoading(Context context) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_loading);
		tvPercent = (TextView) findViewById(R.id.tvPercent);
		setCancelable(true);
	}

	Handler handleTick = null;
	Runnable runableTick = null;
	int count = 0;
	int currentDelay = 100; // delay for 1 sec.

    public void setText(String ss){
        tvPercent.setVisibility(View.VISIBLE);
        tvPercent.setText(ss);
    }
    public void hideText(){
        tvPercent.setVisibility(View.GONE);
    }

	/**
	 * tick time to add and remove a view on layout when time change
	 * 
	 *            max time to tick
	 */
	public void tickTimeCheckView() {
		tvPercent.setVisibility(View.VISIBLE);
		stopTick();
		handleTick = new Handler();
		runableTick = new Runnable() {
			@Override
			public void run() {
				count++;
				tvPercent.setText(count + "%");
				if (count == currentDelay) {
					count = 0;
					tvPercent.setVisibility(View.GONE);
					stopSide();
				}

				if (handleTick != null)
					handleTick.postDelayed(this, 50);
			}
		};
		if (handleTick != null)
			handleTick.post(runableTick);
	}

	public void stopSide() {
		stopTick();
	}

	private void stopTick() {
		// TODO Auto-generated method stub
		if (handleTick != null) {
			if (runableTick != null) {
				handleTick.removeCallbacks(runableTick);
				runableTick = null;
			}
			handleTick = null;
		}
	}
}
