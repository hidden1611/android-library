package nna.elcom.library.videoplayer.overlay;

import android.R;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;

public class overlay {
	private  View mInfo =  null;
    private static final int SURFACE_BEST_FIT = 0;
    private static final int SURFACE_FIT_HORIZONTAL = 1;
    private static final int SURFACE_FIT_VERTICAL = 2;
    private static final int SURFACE_FILL = 3;
    private static final int SURFACE_16_9 = 4;
    private static final int SURFACE_4_3 = 5;
    private static final int SURFACE_ORIGINAL = 6;
    private static final int OVERLAY_TIMEOUT = 4000;
    private static final int OVERLAY_INFINITE = 3600000;
    private static final int FADE_OUT = 1;
    private static final int SHOW_PROGRESS = 2;
    private static final int SURFACE_SIZE = 3;
    private static final int FADE_OUT_INFO = 4;
    private int mCurrentSize = SURFACE_BEST_FIT;
    private  Context mContext;
    private ImageButton	img;
    private int resources = 0;
    private boolean isAlwaysHide = false;
	public overlay(View mInfo, Context mContext){
		this.mInfo = mInfo;
		this.mContext = mContext;
	}	
	
	public overlay(View mInfo, Context mContext, ImageButton imgs, int resouces){
		this.mInfo = mInfo;
		this.mContext = mContext;
		this.img = imgs;
		this.resources = resouces;
	}
	
	public void clickListener(int mCurrentSize){
        switch (mCurrentSize) {
            case SURFACE_BEST_FIT:
               /* showInfo("Best Fit", 1000);*/
                break;
            case SURFACE_FIT_HORIZONTAL:
               /* showInfo("fit horizontal", 1000);*/
                break;
            case SURFACE_FIT_VERTICAL:
                /*showInfo("fit vertical", 1000);*/
                break;
            case SURFACE_FILL:
                /*showInfo("FullScreen", 1000);*/
                break;
            case SURFACE_16_9:
                /*showInfo("16:9", 1000);*/
                break;
            case SURFACE_4_3:
                /*showInfo("4:3", 1000);*/
                break;
            case SURFACE_ORIGINAL:
                /*showInfo("original", 1000);*/
                break;
        }
	}
	public void showInfo(int duration){
        if(!isAlwaysHide) {
            mInfo.setVisibility(View.VISIBLE);
            mVideoHandler.removeMessages(FADE_OUT_INFO);
            mVideoHandler.sendEmptyMessageDelayed(FADE_OUT_INFO, duration);
        }
	}
	public void showInfo(String text) {
       /* mInfo.setVisibility(View.VISIBLE);
        ((TextView) mInfo).setText(text);*/
        mVideoHandler.removeMessages(FADE_OUT_INFO);
    }


    public boolean isAlwaysHide() {
        return isAlwaysHide;
    }

    public void setAlwaysHide(boolean alwaysHide) {
        isAlwaysHide = alwaysHide;
        if(isAlwaysHide){
            mInfo.setVisibility(View.GONE);
        }
    }


    /**
     * Show text in the info view for "duration" milliseconds
     * @param text: string
     * @param duration: ms
     */
    public void showInfo(String text, int duration) {
        mInfo.setVisibility(View.VISIBLE);
        ((TextView)mInfo).setText(text);
        mVideoHandler.removeMessages(FADE_OUT_INFO);
        mVideoHandler.sendEmptyMessageDelayed(FADE_OUT_INFO, duration);
    }
    /**
     * hide the info view with "delay" milliseconds delay
     * @param delay
     */
    public void hideInfo(int delay) {
        mVideoHandler.sendEmptyMessageDelayed(FADE_OUT_INFO, delay);
    }

    /**
     * hide the info view
     */
    public void hideInfo() {
        hideInfo(0);
    }
	private void fadeOutInfo() {
		if(mInfo != null){
			if (mInfo.getVisibility() == View.VISIBLE)
	            mInfo.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_out));
	        mInfo.setVisibility(View.GONE);
	        /*Toast.makeText(mContext, "sjdjsajdhsad", Toast.LENGTH_LONG).show();*/
	        if (img != null) {
	        	img.setBackgroundResource(resources);
			}
	        
	    }
    }
    private Handler mVideoHandler = new VideoPlayerHandler();
    private class VideoPlayerHandler extends Handler{

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FADE_OUT_INFO:
                	fadeOutInfo();
                    break;
            }
        }
    };
}
