package nna.elcom.library.videoplayer;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TableRow;
import android.widget.TextView;


/**
 * @author Administrator
 *
 */

/**
 * @author Administrator
 *
 */
public class DialogSettings extends Dialog {

	private TextView	txtSubtitlesubposselected;
	private TextView 	txtSubtitlesubcolselected;
	private TextView	txtSubtitlefontfileselected;
	private TextView	txtSubtitleSizeSelected;
	
	private CheckBox    chkSetting_subtitlehide_box; 
	
	private TableRow 	tblrowsubtitlesubcolselected;
	private TableRow	tblrowsubtitlesubfileselected;
	private TableRow	tblrowsubtitlesize;
	private TableRow	tblrowsubtitlefontfileselected;
	private TableRow	tblrowsubtitlesubposselected;
	
	private Button		btnOK;
	private Button		btnDefault;
	
	private Typeface	mTypeface;
	
	public DialogSettings(Context context) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.settings);
		
		mTypeface = Util.getTypeFace(context);
		
		setCancelable(true);
		setUpView();
		setupFonts();
	}
	
	
	/**
	 * 
	 */
	private void setUpView(){
		
		txtSubtitlesubposselected 	= 	(TextView) findViewById(R.id.subtitlesubposselected);
		txtSubtitlesubcolselected 	= 	(TextView) findViewById(R.id.subtitlesubcolselected);
		txtSubtitlefontfileselected	=	(TextView) findViewById(R.id.subtitlefontfileselected);
		txtSubtitleSizeSelected		=	(TextView) findViewById(R.id.subtitleSizeSelected);
		
		tblrowsubtitlesize			=	(TableRow) findViewById(R.id.tblrowsubtitlesize);
		tblrowsubtitlesubfileselected=	(TableRow) findViewById(R.id.tblrowsubtitlesubfileselected);
		tblrowsubtitlesubcolselected = 	(TableRow) findViewById(R.id.tblrowsubtitlesubcolselected);
		tblrowsubtitlefontfileselected=	(TableRow) findViewById(R.id.tblrowsubtitlefontfileselected);
		tblrowsubtitlesubposselected=	(TableRow) findViewById(R.id.tblrowsubtitlesubposselected);
		
		chkSetting_subtitlehide_box = 	(CheckBox) findViewById(R.id.setting_subtitlehide_box);
		
		btnOK					  	= 	(Button) findViewById(R.id.OK);
		btnDefault				  	=	(Button) findViewById(R.id.Default);	
	}

	
	private void setupFonts(){
		txtSubtitlesubposselected.setTypeface(mTypeface);
		txtSubtitlesubcolselected.setTypeface(mTypeface);
		txtSubtitlefontfileselected.setTypeface(mTypeface);
		txtSubtitleSizeSelected.setTypeface(mTypeface);
		
		btnOK.setTypeface(mTypeface);
		btnDefault.setTypeface(mTypeface);

	}

	/**
	 * @return
	 */
	public TextView getTxtSubtitlesubposselected() {
		return txtSubtitlesubposselected;
	}


	/**
	 * @param txtSubtitlesubposselected
	 */
	public void setTxtSubtitlesubposselected(TextView txtSubtitlesubposselected) {
		this.txtSubtitlesubposselected = txtSubtitlesubposselected;
	}


	/**
	 * @return
	 */
	public TextView getTxtSubtitlesubcolselected() {
		return txtSubtitlesubcolselected;
	}


	/**
	 * @param txtSubtitlesubcolselected
	 */
	public void setTxtSubtitlesubcolselected(TextView txtSubtitlesubcolselected) {
		this.txtSubtitlesubcolselected = txtSubtitlesubcolselected;
	}


	/**
	 * @return
	 */
	public TextView getTxtSubtitlefontfileselected() {
		return txtSubtitlefontfileselected;
	}


	/**
	 * @param txtSubtitlefontfileselected
	 */
	public void setTxtSubtitlefontfileselected(TextView txtSubtitlefontfileselected) {
		this.txtSubtitlefontfileselected = txtSubtitlefontfileselected;
	}


	/**
	 * @return
	 */
	public TextView getTxtSubtitleSizeSelected() {
		return txtSubtitleSizeSelected;
	}


	/**
	 * @param txtSubtitleSizeSelected
	 */
	public void setTxtSubtitleSizeSelected(TextView txtSubtitleSizeSelected) {
		this.txtSubtitleSizeSelected = txtSubtitleSizeSelected;
	}


	/**
	 * @return
	 */
	public CheckBox getChkSetting_subtitlehide_box() {
		return chkSetting_subtitlehide_box;
	}


	/**
	 * @param chkSetting_subtitlehide_box
	 */
	public void setChkSetting_subtitlehide_box(CheckBox chkSetting_subtitlehide_box) {
		this.chkSetting_subtitlehide_box = chkSetting_subtitlehide_box;
	}


	/**
	 * @return
	 */
	public Button getBtnOK() {
		return btnOK;
	}


	/**
	 * @param btnOK
	 */
	public void setBtnOK(Button btnOK) {
		this.btnOK = btnOK;
	}


	/**
	 * @return
	 */
	public Button getBtnDefault() {
		return btnDefault;
	}


	/**
	 * @param btnDefault
	 */
	public void setBtnDefault(Button btnDefault) {
		this.btnDefault = btnDefault;
	}


	/**
	 * @return
	 */
	public TableRow getTblrowsubtitlesubcolselected() {
		return tblrowsubtitlesubcolselected;
	}


	/**
	 * @param tblrowsubtitlesubcolselected
	 */
	public void setTblrowsubtitlesubcolselected(
			TableRow tblrowsubtitlesubcolselected) {
		this.tblrowsubtitlesubcolselected = tblrowsubtitlesubcolselected;
	}

	/**
	 * @return
	 */
	public TableRow getTblrowsubtitlesubfileselected() {
		return tblrowsubtitlesubfileselected;
	}


	/**
	 * @param tblrowsubtitlesubfileselected
	 */
	public void setTblrowsubtitlesubfileselected(
			TableRow tblrowsubtitlesubfileselected) {
		this.tblrowsubtitlesubfileselected = tblrowsubtitlesubfileselected;
	}


	/**
	 * @return
	 */
	public TableRow getTblrowsubtitlesize() {
		return tblrowsubtitlesize;
	}


	/**
	 * @param tblrowsubtitlesize
	 */
	public void setTblrowsubtitlesize(TableRow tblrowsubtitlesize) {
		this.tblrowsubtitlesize = tblrowsubtitlesize;
	}


	public TableRow getTblrowsubtitlefontfileselected() {
		return tblrowsubtitlefontfileselected;
	}


	public void setTblrowsubtitlefontfileselected(
			TableRow tblrowsubtitlefontfileselected) {
		this.tblrowsubtitlefontfileselected = tblrowsubtitlefontfileselected;
	}


	public TableRow getTblrowsubtitlesubposselected() {
		return tblrowsubtitlesubposselected;
	}


	public void setTblrowsubtitlesubposselected(
			TableRow tblrowsubtitlesubposselected) {
		this.tblrowsubtitlesubposselected = tblrowsubtitlesubposselected;
	}
	
	
	
}
