package nna.elcom.library.videoplayer;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import nna.elcom.library.videoplayer.control.CtrAlertConfirmShow;
import nna.elcom.library.videoplayer.control.DialogLoading;
import nna.elcom.library.videoplayer.overlay.overlay;


public class DVPlayer extends Activity implements OnCompletionListener,
        OnErrorListener, OnInfoListener, OnBufferingUpdateListener,
        OnPreparedListener, OnSeekCompleteListener, OnVideoSizeChangedListener,
        MediaController.MediaPlayerControl {

    private static final int SETTING_REQ = 0x10;
    private static SharedPreferences settings;
    Display currentDisplay;
    VideoView videoview;
    String mPath;
    View mainView;
    TextView mcCurrentTime;
    TextView mcTotalTime;
    TextView mcFileName;
    ImageButton mcPlay;
    SeekBar mcSeek;
    LinearLayout mcLayout;
    private overlay mOverPlayMc;
    private ImageButton mSubSetting;
    private TextView mInfo;
    private TextView textHeader;
    private ListView mListSubtitle;
    private LinearLayout mLinearSubtitle;
    private DialogLoading mLoading;
    private listSubtitleAdapter mlistSubAdapeter;
    private ArrayList<SubtitleItem> mlistSubtitleItem;
    private overlay mOvetPlaySub;
    private static final int SURFACE_FILL = 1;
    private static final int SURFACE_16_9 = 2;
    private static final int SURFACE_4_3 = 3;
    private static final int SURFACE_ORIGINAL = 0;
    private int mCurrentSize = SURFACE_ORIGINAL;
    private overlay mOverPlay;
    // size of the videoCur
    private int mVideoHeight;
    private int mVideoWidth;
    // private int mSarNum;
    // private int mSarDen;
    TextView mTextSubtitle;
    private Subtitle mSubtitle;
    private Switch switchSubtitle;
    int totalDuration;
    int playedDuration;
    private static boolean paused;
    private boolean bSeeking;
    public final static String LOGTAG = "STREAMING_VIDEO_PLAYER";
    private Updater seekBarUpdater = new Updater();
    private Handler mHandler = new Handler();
    private String str_lang_ = "";

    private CtrAlertConfirmShow ctrAlert;
    int durationFilm = 0;
    Boolean isWaitSeek = false;

    String[] url = null;
    Context context;
    Typeface mTypeface;
    private Video videoCur;
    private List<Video> videos;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // gui start
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(LayoutParams.FLAG_FULLSCREEN);
        context = DVPlayer.this;
        mLoading = new DialogLoading(context);
        mLoading.show();
        setContentView(R.layout.videoview);
        if(getIntent().hasExtra("BUDDLE_VIDEO")) {
            videoCur = getIntent().getParcelableExtra("BUDDLE_VIDEO");
        }else if(getIntent().hasExtra("BUNDLE_LIST_VIDEO")){
            videos = getIntent().getParcelableArrayListExtra("BUNDLE_LIST_VIDEO");
            for(Video video:videos){
                if(video.isPlay()){
                    videoCur = video;
                    break;
                }
            }
            if(videoCur == null){
                toast("Could not play this video");
                finish();
            }
        }else{
            videoCur = new Video();
            videoCur.setUrl("http://172.16.9.141/vod_hotel/mp4/1563.mp4");
            videoCur.setTitle("Test");
        }

        initUI();
        playVideo();
        currentDisplay = getWindowManager().getDefaultDisplay();
        setUpFont();
    }





    private void log(String ss) {
        Log.d("EVPLayer", ss);
    }

    private void toast(String ss) {

        Toast.makeText(DVPlayer.this, ss, Toast.LENGTH_SHORT).show();
    }


    private Dialog alertSubPosition(final TextView txtPositionSelected) {
        final DialogDetail dialog = new DialogDetail(context);

        TextView txtTitle = (TextView) dialog.findViewById(R.id.alert_title);
        TextView txt1 = (TextView) dialog.findViewById(R.id.alert_txt1);
        TextView txt2 = (TextView) dialog.findViewById(R.id.alert_txt2);
        TextView txt3 = (TextView) dialog.findViewById(R.id.alert_txt3);
        Button butOK = (Button) dialog.findViewById(R.id.alert_ok);
        Button butCancle = (Button) dialog.findViewById(R.id.alert_cancle);

        txtTitle.setTypeface(mTypeface);
        txt1.setTypeface(mTypeface);
        txt2.setTypeface(mTypeface);
        txt3.setTypeface(mTypeface);
        butOK.setTypeface(mTypeface);
        butCancle.setTypeface(mTypeface);

        txtTitle.setText(getString(R.string.position));
        txt1.setText(getString(R.string.top));
        txt2.setText(getString(R.string.center));
        txt3.setText(getString(R.string.bottom));
        butOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        butCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                writeSubPosition(2);
                mSubtitle.setPosition(2);
                txtPositionSelected
                        .setText(Globals.subtitlepos[Globals.dbSubtitlePos]);
                dialog.dismiss();
            }
        });
        txt2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                writeSubPosition(1);
                mSubtitle.setPosition(1);
                txtPositionSelected
                        .setText(Globals.subtitlepos[Globals.dbSubtitlePos]);
                dialog.dismiss();
            }
        });
        txt3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                writeSubPosition(0);
                mSubtitle.setPosition(0);
                txtPositionSelected
                        .setText(Globals.subtitlepos[Globals.dbSubtitlePos]);
                dialog.dismiss();
            }
        });

        txtPositionSelected.setTypeface(mTypeface);
		
		/*writeSubPosition(0);*/

        dialog.show();
        return dialog;
    }

    private Dialog alertSubColor(final TextView txtColorSelected) {
        final DialogDetail dialog = new DialogDetail(context);
        TextView txtTitle = (TextView) dialog.findViewById(R.id.alert_title);
        TextView txt1 = (TextView) dialog.findViewById(R.id.alert_txt1);
        TextView txt2 = (TextView) dialog.findViewById(R.id.alert_txt2);
        TextView txt3 = (TextView) dialog.findViewById(R.id.alert_txt3);
        Button butOK = (Button) dialog.findViewById(R.id.alert_ok);
        Button butCancle = (Button) dialog.findViewById(R.id.alert_cancle);

        txtTitle.setTypeface(mTypeface);
        txt1.setTypeface(mTypeface);
        txt2.setTypeface(mTypeface);
        txt3.setTypeface(mTypeface);
        butOK.setTypeface(mTypeface);
        butCancle.setTypeface(mTypeface);

        txtTitle.setText(getString(R.string.color));
        txt1.setText(getString(R.string.white));
        txt2.setText(getString(R.string.red));
        txt3.setText(getString(R.string.green));
        butOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        butCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                writeSubColor(0);
                mSubtitle.setColor(FileManager.getSubTitleColor());
                txtColorSelected
                        .setText(Globals.subtitlecol[Globals.dbSubtitleCol]);
                dialog.dismiss();
            }
        });
        txt2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                writeSubColor(1);
                mSubtitle.setColor(FileManager.getSubTitleColor());
                txtColorSelected
                        .setText(Globals.subtitlecol[Globals.dbSubtitleCol]);
                dialog.dismiss();
            }
        });
        txt3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                writeSubColor(2);
                mSubtitle.setColor(FileManager.getSubTitleColor());
                txtColorSelected
                        .setText(Globals.subtitlecol[Globals.dbSubtitleCol]);
                dialog.dismiss();
            }
        });

        txtColorSelected.setTypeface(mTypeface);

        dialog.show();
        return dialog;
    }

    private Dialog alertSubSize(final TextView txtSizeSelected) {
        final DialogDetail dialog = new DialogDetail(context);

        TextView txtTitle = (TextView) dialog.findViewById(R.id.alert_title);
        TextView txt1 = (TextView) dialog.findViewById(R.id.alert_txt1);
        TextView txt2 = (TextView) dialog.findViewById(R.id.alert_txt2);
        TextView txt3 = (TextView) dialog.findViewById(R.id.alert_txt3);
        Button butOK = (Button) dialog.findViewById(R.id.alert_ok);
        Button butCancle = (Button) dialog.findViewById(R.id.alert_cancle);

        txtTitle.setTypeface(mTypeface);
        txt1.setTypeface(mTypeface);
        txt2.setTypeface(mTypeface);
        txt3.setTypeface(mTypeface);
        butOK.setTypeface(mTypeface);
        butCancle.setTypeface(mTypeface);

        txtTitle.setText(getString(R.string.size));
        txt1.setText(getString(R.string.footer_size_large));
        txt2.setText(getString(R.string.footer_size_medium));
        txt3.setText(getString(R.string.footer_size_small));
        butOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        butCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                writeSubSize(2);
                mSubtitle.setSize((float) FileManager.getSubTitleSize());
                txtSizeSelected
                        .setText(Globals.subtitlesize[Globals.dbSubtitleSize]);
                dialog.dismiss();
            }
        });
        txt2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                writeSubSize(1);
                mSubtitle.setSize((float) FileManager.getSubTitleSize());
                txtSizeSelected
                        .setText(Globals.subtitlesize[Globals.dbSubtitleSize]);
                dialog.dismiss();
            }
        });
        txt3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                writeSubSize(0);
                mSubtitle.setSize((float) FileManager.getSubTitleSize());
                txtSizeSelected
                        .setText(Globals.subtitlesize[Globals.dbSubtitleSize]);
                dialog.dismiss();
            }
        });

        txtSizeSelected.setTypeface(mTypeface);
        dialog.show();
        return dialog;
    }

    private Dialog alertSetting() {

        final DialogSettings dialog = new DialogSettings(context);

        TableRow rowSubFile = dialog.getTblrowsubtitlesubfileselected();
        rowSubFile.setVisibility(View.GONE);

        TableRow rowSubFont = dialog.getTblrowsubtitlefontfileselected();
        rowSubFont.setVisibility(View.GONE);

        Button butOk = (Button) dialog.getBtnOK();
        Button butDefault = (Button) dialog.getBtnDefault();


        TableRow rowSubColor = (TableRow) dialog .getTblrowsubtitlesubcolselected();

        TableRow rowSubPosition = (TableRow) dialog .getTblrowsubtitlesubposselected();

        TableRow rowSubSize = (TableRow) dialog.getTblrowsubtitlesize();


        final TextView txtSizeSelected = (TextView) dialog .getTxtSubtitleSizeSelected();

        final TextView txtPositionSelected = (TextView) dialog .getTxtSubtitlesubposselected();

        final TextView txtColorSelected = (TextView) dialog .getTxtSubtitlesubcolselected();

        Globals.subtitlesize = getResources().getStringArray(
                R.array.SubtitleSize);
        Globals.subtitlepos = getResources()
                .getStringArray(R.array.SubtitlePos);
        Globals.subtitlecol = getResources()
                .getStringArray(R.array.SubtitleCol);

        txtSizeSelected.setText(Globals.subtitlesize[Globals.dbSubtitleSize]);
        txtPositionSelected.setText(Globals.subtitlepos[Globals.dbSubtitlePos]);
        txtColorSelected.setText(Globals.subtitlecol[Globals.dbSubtitleCol]);

        txtSizeSelected.setTypeface(mTypeface);
        txtPositionSelected.setTypeface(mTypeface);
        txtColorSelected.setTypeface(mTypeface);

        CheckBox checkEnable = (CheckBox) dialog
                .findViewById(R.id.setting_subtitlehide_box);
        checkEnable.setChecked(Globals.dbSubtitle);

        checkEnable.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                mSubtitle.setEnable(isChecked);
                writeSubEnable(isChecked);
            }
        });

        rowSubColor.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                alertSubColor(txtColorSelected);
            }
        });
        rowSubPosition.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                alertSubPosition(txtPositionSelected);
            }
        });
        rowSubSize.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                alertSubSize(txtSizeSelected);
            }
        });
        butOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        butDefault.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                writeSubPosition(0);
                mSubtitle.setPosition(0);
                txtPositionSelected
                        .setText(Globals.subtitlepos[Globals.dbSubtitlePos]);

                writeSubColor(0);
                mSubtitle.setColor(FileManager.getSubTitleColor());
                txtColorSelected
                        .setText(Globals.subtitlecol[Globals.dbSubtitleCol]);

                writeSubSize(1);
                mSubtitle.setSize((float) FileManager.getSubTitleSize());
                txtSizeSelected
                        .setText(Globals.subtitlesize[Globals.dbSubtitleSize]);
                // dialog.dismiss();
            }
        });
        dialog.show();
        return dialog;
    }

    private void setUpFont(){

        mcCurrentTime.setTypeface(mTypeface);
        mcTotalTime.setTypeface(mTypeface);
        mcCurrentTime.setTypeface(mTypeface);
        mInfo.setTypeface(mTypeface);
        mcFileName.setTypeface(mTypeface);

        textHeader.setTypeface(mTypeface);
    }





    private void initUIMediaControll() {

        mcCurrentTime = (TextView) findViewById(R.id.txtCurTime);
        mcTotalTime = (TextView) findViewById(R.id.txtEnTime);
        mcSeek = (SeekBar) findViewById(R.id.playerSeekBar);
        mcFileName = (TextView) findViewById(R.id.txtFilmName);
        switchSubtitle = (Switch) findViewById(R.id.witchSubtile);
        switchSubtitle.toggle();
        switchSubtitle.setOnCheckedChangeListener(onSubTitleChecked);
        mcSeek.setOnSeekBarChangeListener(mcSeekChangeListener);
        mcLayout = (LinearLayout) findViewById(R.id.videoControler);
        mcPlay = (ImageButton) findViewById(R.id.btnStop);
        ctrAlert = (CtrAlertConfirmShow) findViewById(R.id.ctrAlert);

        mcPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tooglePlay();
            }
        });
        mOverPlayMc = new overlay(mcLayout, getBaseContext());
        mOverPlayMc.setAlwaysHide(videoCur.hasTV());
        mOverPlayMc.showInfo(Globals.timeOverplay);

    }

    private void initUISubtitle() {

        textHeader = (TextView) findViewById(R.id.text);
        mSubSetting = (ImageButton) findViewById(R.id.btnSubtitle);
        mTextSubtitle = (TextView) findViewById(R.id.subtitle);
        mLinearSubtitle = (LinearLayout) findViewById(R.id.lineSubtitle);
        mListSubtitle = (ListView) findViewById(R.id.listSubtitle);

        ImageView img = (ImageView) findViewById(R.id.imageFooter);
        img.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                alertSetting();
            }
        });

        mlistSubtitleItem = new ArrayList<SubtitleItem>();


        mListSubtitle.setOnItemClickListener(onItemSubtileClick);
        mListSubtitle.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                view.setBackgroundColor(Color.TRANSPARENT);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mListSubtitle.setItemsCanFocus(false);
        mSubSetting.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLinearSubtitle.getVisibility() == View.VISIBLE) {
                    mLinearSubtitle.setVisibility(View.GONE);
                    mSubSetting.setBackgroundResource(R.drawable.bg_subtitle);
                } else {
                    mSubSetting
                            .setBackgroundResource(R.drawable.button_subtitle_focus);
                    mOvetPlaySub.showInfo(Globals.timeOverplay);
                }
            }
        });

        mOvetPlaySub = new overlay((View) mLinearSubtitle, context,
                mSubSetting, R.drawable.bg_subtitle);
		/*mOvetPlaySub.showInfo(Globals.timeOverplay);*/
    }


    private void initUIMain() {
        mainView = this.findViewById(R.id.MainView);
        mInfo = (TextView) findViewById(R.id.player_overlay_info);
        videoview = (VideoView) findViewById(R.id.videoview);
        mainView.setOnClickListener(mMainViewListener);
        mOverPlay = new overlay((View) mInfo, context);

        videoview.setOnCompletionListener(this);
        videoview.setOnErrorListener(this);
        videoview.setOnPreparedListener(this);
    }

    private void initUI() {
        initUIMain();
        initUIMediaControll();
        initUISubtitle();
        setUpFont();
    }



    OnItemClickListener onItemSubtileClick = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

            for (SubtitleItem subtitleItem : mlistSubtitleItem) {
                subtitleItem.isSelected = false;
            }
            mlistSubtitleItem.get(position).isSelected = true;
            mlistSubAdapeter.notifyDataSetChanged();
            if (!mlistSubtitleItem.get(position).isDownloaded()) {
                new DownloadTask().execute(mlistSubtitleItem.get(position)
                                .url(), mlistSubtitleItem.get(position).text(),
                        mlistSubtitleItem.get(position).pathSave(),
                        mlistSubtitleItem.get(position).filename());

            } else {
                String result = mlistSubtitleItem.get(position).pathSave()
                        + "/" + mlistSubtitleItem.get(position).filename();
				/* mSubtitle.LoadSubtitle(result); */
                mSubtitle.startLoadSubtitleTask(result);
                Globals.setSubTitleFile(result);
				/*
				 * if (mSubtitle.isReady()) {
				 * mOverPlay.showInfo("Load Subtitle: " + result, 3000); }
				 */
            }
            mOvetPlaySub.showInfo(Globals.timeOverplay);
        }
    };


    int lang, cur;
    private void loadSub() {
        if(videoCur.getSubtitle() == null){
            return;
        }
        readSettings();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(3, 10000);
        client.get(videoCur.getSubtitle(), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                Document document = Jsoup.parse(new String(responseBody));
                for (Element element:document.select("ListUrl")){
                    SubtitleItem item = new SubtitleItem(element);
                    mlistSubtitleItem.add(item);
                }

                String strLang = null;
                if (strLang != null) {
                    if (strLang.equals("vi")) {
                        lang = 1;
                    }
                    if (strLang.equals("en")) {
                        lang = 2;
                    } else {
                        lang = 1;
                    }
                } else
                    lang = 1;

                mlistSubAdapeter = new listSubtitleAdapter(DVPlayer.this, mlistSubtitleItem);

                if (mlistSubtitleItem != null && mlistSubtitleItem.size() > 0) {
                    mListSubtitle.setAdapter(mlistSubAdapeter);
                    Handler mHandler = new Handler();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                for (int i = 0; i < mlistSubtitleItem.size(); i++) {
                                    if (lang == mlistSubtitleItem.get(i).getId()) {
                                        // lang_sub_display = lang;
                                        cur = i;
                                        mlistSubtitleItem.get(i).isSelected = true;
                                    } else {
                                        mlistSubtitleItem.get(i).isSelected = false;
                                    }
                                }
                                mlistSubAdapeter.notifyDataSetChanged();
                                if (!mlistSubtitleItem.get(cur).isDownloaded()) {
                                    new DownloadTask().execute(
                                            mlistSubtitleItem.get(cur).url(),
                                            mlistSubtitleItem.get(cur).text(),
                                            mlistSubtitleItem.get(cur).pathSave(),
                                            mlistSubtitleItem.get(cur).filename());

                                } else {
                                    String strResult = mlistSubtitleItem.get(cur).pathSave() + "/" + mlistSubtitleItem.get(cur).filename();
                                    mSubtitle.startLoadSubtitleTask(strResult);
                                    Globals.setSubTitleFile(strResult);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 1000);
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {

            }

        });


    }


    private class LoadSubTask extends AsyncTask<Void, Void, Void> {
        int lang = 1;
        // int lang_sub_display = -1;
        int cur = 0;

        @Override
        protected Void doInBackground(Void... params) {
//			String server =
//			try {
//                log(mID);
//                //http://192.168.2.2:8383//eHotel2Media/CoreMedia?command=5&contentid=1652
//				vo = vod.getUrlSub(Integer.parseInt(mID));
//				for (VodUrlVo urlVo : vo) {
//					// urlVo.getUrl()
//					SubtitleItem item = new SubtitleItem(urlVo.getName_lang(), urlVo.getUrl(), null);
//					item.setId(urlVo.getLangid());
//					mlistSubtitleItem.add(item);
//					log("language:" + urlVo.getName_lang() + "===IdSug:" + item.getId() + "link:" + item.url());
//				}
//
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//			String strLang = DAO.getLanguage(DVPlayer.this);
            String strLang = null;
            if (strLang != null) {
                if (strLang.equals("vi")) {
                    lang = 1;
                }
                if (strLang.equals("en")) {
                    lang = 2;
                } else {
                    lang = 1;
                }
            } else
                lang = 1;

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
			/* mlistSubAdapeter.notifyDataSetChanged(); */

            mlistSubAdapeter = new listSubtitleAdapter(DVPlayer.this, mlistSubtitleItem);
            if (mlistSubtitleItem != null) {
                if (mlistSubtitleItem.size() > 0) {
                    mListSubtitle.setAdapter(mlistSubAdapeter);
                    Handler mHandler = new Handler();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                for (int i = 0; i < mlistSubtitleItem.size(); i++) {
                                    if (lang == mlistSubtitleItem.get(i) .getId()) {
                                        // lang_sub_display = lang;
                                        cur = i;
                                        mlistSubtitleItem.get(i).isSelected = true;
                                    }else{
                                        mlistSubtitleItem.get(i).isSelected = false;
                                    }
                                }
                                mlistSubAdapeter.notifyDataSetChanged();
                                if (!mlistSubtitleItem.get(cur).isDownloaded()) {
                                    new DownloadTask().execute(
                                            mlistSubtitleItem.get(cur).url(),
                                            mlistSubtitleItem.get(cur).text(),
                                            mlistSubtitleItem.get(cur) .pathSave(),
                                            mlistSubtitleItem.get(cur) .filename());

                                } else {
                                    String strResult = mlistSubtitleItem.get( cur).pathSave() + "/" + mlistSubtitleItem.get(cur) .filename();
									/* mSubtitle.LoadSubtitle(strResult); */
                                    mSubtitle.startLoadSubtitleTask(strResult);
                                    Globals.setSubTitleFile(strResult);
									/*
									 * if (mSubtitle.isReady()) {
									 * mOverPlay.showInfo("Load Subtitle: " +
									 * strResult, 3000); }
									 */
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 1000);
                }
            }

            //
        }

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // setSurfaceSize(mVideoWidth, mVideoHeight, mSarNum, mSarDen);
        super.onConfigurationChanged(newConfig);
    }


    private void changeSurfaceSize() {

        // get screen size
        DisplayMetrics disp = context.getResources().getDisplayMetrics();
        int dw = disp.widthPixels, dh = disp.heightPixels;

        // getWindow().getDecorView() doesn't always take orientation into
        // account, we have to correct the values
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) videoview
                .getLayoutParams();
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
        lp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
        lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);

        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        if (dw > dh && isPortrait || dw < dh && !isPortrait) {
            int d = dw;
            dw = dh;
            dh = d;
        }
        if (dw * dh == 0)
            return;

        // compute the aspect ratio
        double ar, vw;
        double density = (double) videoview.getWidth()
                / (double) videoview.getHeight();
        if (density == 1.0) {
			/* No indication about the density, assuming 1:1 */
            vw = mVideoWidth;
            ar = (double) mVideoWidth / (double) mVideoHeight;
        } else {
			/* Use the specified aspect ratio */
            vw = mVideoWidth * density;
            ar = vw / mVideoHeight;
        }

        // compute the display aspect ratio
        double dar = (double) dw / (double) dh;

        switch (mCurrentSize) {
            case SURFACE_16_9:
                lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                ar = 16.0 / 9.0;
                if (dar < ar)
                    dh = (int) (dw / ar);
                else
                    dw = (int) (dh * ar);
                break;
            case SURFACE_4_3:
                lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                ar = 4.0 / 3.0;
                if (dar < ar)
                    dh = (int) (dw / ar);
                else
                    dw = (int) (dh * ar);
                break;
            case SURFACE_ORIGINAL:
                lp.width = RelativeLayout.LayoutParams.WRAP_CONTENT;
                lp.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
                break;
            case SURFACE_FILL:
                lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        }

        // surfaceHolder.setFixedSize(mVideoWidth, mVideoHeight);

        lp.width = dw;
        lp.height = dh;
        videoview.setLayoutParams(lp);
        videoview.invalidate();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        paused = false;
        try {
            videoview.setVideoPath(mPath);
        } catch (Exception e) {
            Log.v(LOGTAG, e.getMessage());
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generate method stub

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SETTING_REQ && resultCode == RESULT_CANCELED) {
            // save the infrmation weg tfro msettings activity
            boolean subtitle = data.getBooleanExtra("SUBTITLE",
                    Globals.dbSubtitle);
            int subtitlesize = data.getIntExtra("SUBTITLESIZE",
                    Globals.dbSubtitleSize);
            String subtitlefont = data.getStringExtra("SUBTITLEFONT");
            String subtitlefile = data.getStringExtra("SUBTITLEFILE");
            int subtitlepos = data.getIntExtra("SUBTITLEPOS",
                    Globals.dbSubtitlePos);
            int subtitlecol = data.getIntExtra("SUBTITLECOL",
                    Globals.dbSubtitleCol);

            writeSettings(subtitle, subtitlesize, subtitlefont, subtitlefile,
                    subtitlepos, subtitlecol);

            mSubtitle.setEnable(subtitle);
            mSubtitle.setFont(subtitlefont);
            mSubtitle.setColor(FileManager.getSubTitleColor());
            mSubtitle.setSize((float) FileManager.getSubTitleSize());
            mSubtitle.setPosition(subtitlepos);

            if (subtitle && (subtitlefile != null)) {
                if (!subtitlefile.contains("http")) {
                    if (!subtitlefile.equals(mSubtitle.getLink())) {
						/* mSubtitle.LoadSubtitle(subtitlefile); */
                        mSubtitle.startLoadSubtitleTask(subtitlefile);
						/*
						 * if (mSubtitle.isParseSubtitle()) {
						 * mOverPlay.showInfo("Loaded Subtitle: " +
						 * subtitlefile, Globals.timeOverplay); }
						 */
                    }
                } else {
                    new DownloadTask().execute(subtitlefile);
                }
            } else {
				/* mOverPlay.showInfo("Disable Subtitle", Globals.timeOverplay); */
            }
            videoview.start();
            seekBarUpdater = new Updater();
            mHandler.postDelayed(seekBarUpdater, 10);
            mcPlay.setBackgroundResource(R.drawable.bg_pause);
            paused = !paused;
        }
    }

    public void writeSubSize(int subtitlesize) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(Globals.PREFS_SUBTITLESIZE, subtitlesize);
        editor.commit();
        Globals.setSubTitleSize(subtitlesize);
    }

    public void writeSubColor(int subtitlecol) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(Globals.PREFS_SUBTITLECOL, subtitlecol);
        editor.commit();
        Globals.setSubTitleCol(subtitlecol);
    }

    public void writeSubPosition(int subtitlepos) {
        try{
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt(Globals.PREFS_SUBTITLEPOS, subtitlepos);
            editor.commit();
            Globals.setSubTitlePos(subtitlepos);
        }catch(Exception ex){ex.printStackTrace();}
    }

    public void writeSubEnable(boolean subtitle) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(Globals.PREFS_SUBTITLE, subtitle);
        editor.commit();
        Globals.setShowSubTitle(subtitle);
    }

    public void writeSettings(boolean subtitle, int subtitlesize,
                              String subtitlefont, String subtitlefile, int subtitlepos,
                              int subtitlecol) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(Globals.PREFS_SUBTITLE, subtitle);
        editor.putInt(Globals.PREFS_SUBTITLESIZE, subtitlesize);
        editor.putString(Globals.PREFS_SUBTITLEFONT, subtitlefont);
        editor.putString(Globals.PREFS_SUBTITLEFILE, subtitlefile);
        editor.putInt(Globals.PREFS_SUBTITLEPOS, subtitlepos);
        editor.putInt(Globals.PREFS_SUBTITLECOL, subtitlecol);
        editor.commit();

        Globals.setShowSubTitle(subtitle);
        Globals.setSubTitleSize(subtitlesize);
        Globals.setSubTitleFont(subtitlefont);
        Globals.setSubTitleFile(subtitlefile);
        Globals.setSubTitlePos(subtitlepos);
        Globals.setSubTitleCol(subtitlecol);
    }

    public void readSettings() {

        settings = getSharedPreferences(Globals.PREFS_NAME, MODE_PRIVATE);
        Globals.dbSubtitle = settings.getBoolean(Globals.PREFS_SUBTITLE,
                Globals.dbSubtitle);
        Globals.dbSubtitleSize = settings.getInt(Globals.PREFS_SUBTITLESIZE,
                Globals.dbSubtitleSize);
        Globals.dbSubtitleFont = settings.getString(Globals.PREFS_SUBTITLEFONT,
                Globals.dbSubtitleFont);
        Globals.dbSubtitleFile = settings.getString(Globals.PREFS_SUBTITLEFILE,
                Globals.dbSubtitleFile);
        Globals.dbSubtitlePos = settings.getInt(Globals.PREFS_SUBTITLEPOS,
                Globals.dbSubtitlePos);
        Globals.dbSubtitleCol = settings.getInt(Globals.PREFS_SUBTITLECOL,
                Globals.dbSubtitleCol);

        Globals.setShowSubTitle(Globals.dbSubtitle);
        Globals.setSubTitleSize(Globals.dbSubtitleSize);
        Globals.setSubTitleFont(Globals.dbSubtitleFont);
        Globals.setSubTitleFile(Globals.dbSubtitleFile);
        Globals.setSubTitlePos(Globals.dbSubtitlePos);
        Globals.setSubTitleCol(Globals.dbSubtitleCol);

        mSubtitle.setEnable(Globals.dbSubtitle);
        mSubtitle.setColor(FileManager.getSubTitleColor());
        mSubtitle.setPosition(Globals.dbSubtitlePos);
        mSubtitle.setSize(FileManager.getSubTitleSize());

        // DemoRenderer.UpdateValuesFromSettings();
    }

    private class DownloadTask extends AsyncTask<String, Integer, String> {
        /* ProgressDialog mProgressDialog; */
        DialogDownloadSub mProgressDownload;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            mProgressDownload = new DialogDownloadSub(DVPlayer.this);
			/*
			 * mProgressDownload.setAler(getString(R.string.downloading_subtile))
			 * ; mProgressDownload.setMax(100); /*mProgressDownload.show();
			 */
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String fileName, pathSave, fileLanguague;
            fileLanguague = params[1];
            fileName = params[3];
            pathSave = params[2];
            try {
                log( "Url " + params[0]);
                URL url = new URL(params[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // this will be useful so that you can show a typical 0-100%
                // progress bar
                int fileLength = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());
                (new File(pathSave)).mkdirs();
                RandomAccessFile output = new RandomAccessFile(pathSave + "/" + fileName, "rw");
                byte data[] = new byte[1024];
                int count;
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    output.write(data, 0, count);
                    total += count;
                    publishProgress((int) (total * 100 / fileLength));
                }
                // output.flush();
                output.close();
                input.close();
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }

            return (pathSave + "/" + fileName);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
			/*
			 * mProgressDownload.setProgressSeekBarDownload(values[0]);
			 * mProgressDownload.setPercentDownloading(String.valueOf(values[0])
			 * + " %");
			 */
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            mProgressDownload.dismiss();
            if (result != null) {
				/* mSubtitle.LoadSubtitle(result); */
                mSubtitle.startLoadSubtitleTask(result);
                Globals.setSubTitleFile(result);
				/*
				 * if (mSubtitle.isReady()) {
				 * mOverPlay.showInfo("Load Subtitle: " + result, 3000); }
				 */
            } else {
                // mOverPlay.showInfo(this.getString(R.string.c), 3000);
            }
        }
    }

    public void restartUpdater() {
        seekBarUpdater.stopIt();
        seekBarUpdater = new Updater();
        mHandler.postDelayed(seekBarUpdater, 100);
    }



    OnSeekBarChangeListener mcSeekChangeListener = new OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            log("stop SeekBar");
            int progress = seekBar.getProgress();
            videoview.seekTo(progress * totalDuration);
            bSeeking = false;
            if (!paused) {
                restartUpdater();
            }
            mOverPlay.hideInfo();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            log("start SeekBar");
            bSeeking = true;
            isWaitSeek = true;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
            if (fromUser) {
                int currentSecsMoved = (int) ((totalDuration * ((float) (progress / 10F))) / 100);
                String timeMoved = Util.formatTime(currentSecsMoved);
                // scrolledtime.setText(timeMoved);
                mcCurrentTime.setText(timeMoved);
                // resetAutoHider();
                mOverPlay.showInfo(timeMoved);
            }
        }
    };
    OnClickListener mMainViewListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (mcLayout.getVisibility() == View.VISIBLE) {
                mcLayout.setVisibility(View.GONE);
            } else {
                mOverPlayMc.showInfo(Globals.timeOverplay);
                // mcLayout.setVisibility(View.VISIBLE);
            }

            if(videoCur.hasTV()){
                mcLayout.setVisibility(View.GONE);
            }
        }
    };

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        Log.v(LOGTAG, "surfaceChanged Called");
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.v(LOGTAG, "surfaceDestroyed Called");
    }

    public void onCompletion(MediaPlayer mp) {
        log("oncompletion");
        if (durationFilm > 0) {
            if (mp.getCurrentPosition() >= (durationFilm - 10)) {
                videoview.stopPlayback();
                onBackPressed();
            }
        }
    }

    int curPositionLink = 0;

    public boolean onError(MediaPlayer mp, int whatError, int extra) {
        Log.v(LOGTAG, "onError Called");
        if (whatError == MediaPlayer.MEDIA_ERROR_SERVER_DIED) {
            Log.v(LOGTAG, "Media Error, Server Died " + extra);
        } else if (whatError == MediaPlayer.MEDIA_ERROR_UNKNOWN) {
            Log.v(LOGTAG, "Media Error, Error Unknown " + extra);
        }
        try {
            if (url != null && url.length > 0) {
                curPositionLink++;
                if (curPositionLink < url.length) {

                    mPath = url[curPositionLink];
                    videoview.setOnCompletionListener(DVPlayer.this);
                    videoview.setOnErrorListener(DVPlayer.this);
                    videoview.setOnPreparedListener(DVPlayer.this);
                    videoview.setVideoPath(mPath);
                } else {
                    curPositionLink = 0;
                    mPath = url[curPositionLink];
                    videoview.setOnCompletionListener(DVPlayer.this);
                    videoview.setOnErrorListener(DVPlayer.this);
                    videoview.setOnPreparedListener(DVPlayer.this);
                    videoview.setVideoPath(mPath);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean onInfo(MediaPlayer mp, int whatInfo, int extra) {
        if (whatInfo == MediaPlayer.MEDIA_INFO_BAD_INTERLEAVING) {
            Log.v(LOGTAG, "Media Info, Media Info Bad Interleaving " + extra);
        } else if (whatInfo == MediaPlayer.MEDIA_INFO_NOT_SEEKABLE) {
            Log.v(LOGTAG, "Media Info, Media Info Not Seekable " + extra);
        } else if (whatInfo == MediaPlayer.MEDIA_INFO_UNKNOWN) {
            Log.v(LOGTAG, "Media Info, Media Info Unknown " + extra);
        } else if (whatInfo == MediaPlayer.MEDIA_INFO_VIDEO_TRACK_LAGGING) {
            Log.v(LOGTAG, "MediaInfo, Media Info Video Track Lagging " + extra);
        }
        return false;
    }

    private void initScreen() {
        // set Screen
        mCurrentSize = SURFACE_FILL;
        changeSurfaceSize();
        mOverPlay.clickListener(mCurrentSize);

    }

    public void onPrepared(MediaPlayer mp) {
        durationFilm = mp.getDuration();
        Log.v(LOGTAG, "onPrepared Called");
        mp.start();
        mVideoWidth = mp.getVideoWidth();
        mVideoHeight = mp.getVideoHeight();
//		 mp.seekTo(currentPosition);
        mcTotalTime.setText(Util.formatTime(durationFilm / 1000));
        mSubtitle = new Subtitle(mTextSubtitle);
        initScreen();
        if(seekBarUpdater != null){
            seekBarUpdater.stopIt();
        }
        seekBarUpdater = new Updater();
        mHandler.postDelayed(seekBarUpdater, 10);
        loadSub();
        mLoading.dismiss();
    }

    private class Updater implements Runnable {
        private boolean stop;

        public void stopIt() {
            System.out.println("Stopped updater");
            stop = true;
        }

        @Override
        public void run() {
            if (!bSeeking) {
                if (!stop) {
                    playedDuration = videoview.getCurrentPosition() / 1000;
                    mcCurrentTime.setText(Util.formatTime(videoview
                            .getCurrentPosition() / 1000));
                    mHandler.postDelayed(seekBarUpdater, 100);
                    totalDuration = durationFilm / 1000;
                    if (totalDuration != 0) {
                        int progress = (int) ((1000 * playedDuration) / totalDuration);
                        mcSeek.setProgress(progress);
                        mcTotalTime.setText(Util.formatTime(totalDuration));
                        mSubtitle.findSubTitle(1000 * playedDuration);
                    }
                }
            }
        }
    }

    public void onSeekComplete(MediaPlayer mp) {
        Log.v(LOGTAG, "onSeekComplete Called");
    }

    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        // Log.v(LOGTAG, "onVideoSizeChanged Called");
    }

    public void onBufferingUpdate(MediaPlayer mp, int bufferedPercent) {
        // Log.v(LOGTAG, "MediaPlayer Buffering: " + bufferedPercent + "%");
    }

    public boolean canPause() {
        return true;
    }

    public boolean canSeekBackward() {
        return true;
    }

    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    public int getBufferPercentage() {
        return 0;
    }

    public int getCurrentPosition() {
        return videoview.getCurrentPosition();
    }

    public int getDuration() {
        return videoview.getDuration();
    }

    public boolean isPlaying() {
        return videoview.isPlaying();
    }

    public void pause() {
        if (videoview.isPlaying()) {
            videoview.pause();
        }
    }

    private void playVideo(){
        mOverPlayMc.showInfo(Globals.timeOverplay);
        paused = false;
        mcFileName.setText(videoCur.getTitle());
        videoview.stopPlayback();
        mLoading.show();
        videoview.setVideoPath(videoCur.getUrl());
    }
    private void tooglePlay(){
        if (paused) {
            videoview.start();
            if(seekBarUpdater != null){
                seekBarUpdater.stopIt();
            }
            seekBarUpdater = new Updater();
            mHandler.postDelayed(seekBarUpdater, 100);
            mcPlay.setBackgroundColor(Color.TRANSPARENT);
            mcPlay.setImageResource(R.drawable.bg_pause);
        } else {
            videoview.pause();
            seekBarUpdater.stopIt();
            mcPlay.setBackgroundColor(Color.TRANSPARENT);
            mcPlay.setImageResource(R.drawable.bg_play);
        }
        paused = !paused;
    }



    public void next(){
        int index = videos.indexOf(videoCur);
        index = ++index >= videos.size() ? 0 : index;
        videoCur = videos.get(index);
        playVideo();
    }

    public void prev(){
        int index = videos.indexOf(videoCur);
        index = --index < 0 ? videos.size() - 1 : index;
        videoCur = videos.get(index);
        playVideo();
    }

    public void rewind(){
        videoview.seekTo(videoview.getCurrentPosition() - 30000);
    }
    public void fastforward(){
        videoview.seekTo(videoview.getCurrentPosition() + 30000);
    }


    public void seekTo(int pos) {
        videoview.seekTo(pos);
    }

    public void start() {
        videoview.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        videoview.stopPlayback();
        seekBarUpdater.stopIt();
        for (SubtitleItem item : mlistSubtitleItem) {
            item.deleteFile();
        }
    }



    OnCheckedChangeListener onSubTitleChecked = new OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            if (!isChecked) {
                mTextSubtitle.setVisibility(View.GONE);
                mListSubtitle.setVisibility(View.GONE);
            } else {
                mTextSubtitle.setVisibility(View.VISIBLE);
                mListSubtitle.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_MENU:
                mOverPlayMc.showInfo(Globals.timeOverplay);
                return true;
            case KeyEvent.KEYCODE_ENTER:
                mOverPlayMc.showInfo(Globals.timeOverplay);
                return true;
            case KeyEvent.KEYCODE_DEL:
                onBackPressed();
                return true;
            case KeyEvent.KEYCODE_PAGE_DOWN:
                mOverPlayMc.showInfo(Globals.timeOverplay);
                prev();
                return true;
            case KeyEvent.KEYCODE_PAGE_UP:
                mOverPlayMc.showInfo(Globals.timeOverplay);
                next();
                return true;
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                mOverPlayMc.showInfo(Globals.timeOverplay);
                tooglePlay();
                return true;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                fastforward();
                mOverPlayMc.showInfo(Globals.timeOverplay);
                return true;
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                rewind();
                mOverPlayMc.showInfo(Globals.timeOverplay);
                return true;

        }
        return super.onKeyUp(keyCode, event);
    }

}