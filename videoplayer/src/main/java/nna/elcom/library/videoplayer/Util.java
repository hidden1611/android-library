package nna.elcom.library.videoplayer;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.provider.SyncStateContract;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by nna on 9/17/14.
 */
public class Util {

    public static void setBackgroud(Resources id, View view){
        BitmapDrawable bitmapDrawable = new BitmapDrawable(id);
        bitmapDrawable.setTileModeXY(Shader.TileMode.MIRROR, Shader.TileMode.MIRROR);
        view.setBackground(bitmapDrawable);
    }
    public static void setBackgroud(Resources resources, Bitmap bitmap, final View view){
        final BitmapDrawable bitmapDrawable = new BitmapDrawable(resources,bitmap);
        bitmapDrawable.setTileModeXY(Shader.TileMode.MIRROR, Shader.TileMode.MIRROR);
        Drawable currentBG = view.getBackground();
        if(currentBG == null){
            view.setBackground(bitmapDrawable);
        }else {
            TransitionDrawable transitionDrawable = new TransitionDrawable(new Drawable[]{currentBG, bitmapDrawable});
            transitionDrawable.setCrossFadeEnabled(true);
            view.setBackground(transitionDrawable);
            transitionDrawable.startTransition(1000);
        }
    }
    public static void setBackgroud(String themePath, View view){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(themePath, options);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
        bitmapDrawable.setTileModeXY(Shader.TileMode.MIRROR, Shader.TileMode.MIRROR);
        view.setBackground(bitmapDrawable);
    }
    public static String getTime(){
        SimpleDateFormat timeFormat = new SimpleDateFormat("EEE, MMM d | hh:mm aa");
        Date dt = new Date();

        String strValue = timeFormat.format(dt);
        return strValue;
    }
    public static String getTime2(){
        SimpleDateFormat timeFormat = new SimpleDateFormat("EEE, MMM d\nhh:mm aa");
        Date dt = new Date();
        String strValue = timeFormat.format(dt);
        return strValue;
    }

    public static int convertPxToDp(Context context, int px) {
        WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        float logicalDensity = metrics.density;
        int dp = Math.round(px / logicalDensity);
        return dp;
    }
    public static int convertDpToPx(Context context, int dp) {
        WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics));
    }
    public static float pixelsToSp(Context context, Float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px/scaledDensity;
    }
    public static float spTopixels(Context context, Float sp) {
        float px =  TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
        return px;
    }
    public static String formatTime(long seconds) {
        String output = "";
        //long seconds = millis / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        seconds = seconds % 60;
        minutes = minutes % 60;
        hours = hours % 24;

        String secondsD = String.valueOf(seconds);
        String minutesD = String.valueOf(minutes);
        String hoursD = String.valueOf(hours);

        if (seconds < 10)
            secondsD = "0" + seconds;
        if (minutes < 10)
            minutesD = "0" + minutes;
        if (hours < 10){
            hoursD = "0" + hours;
        }

        if( days > 0 ){
            output = days +"d ";
        }
        if(hours > 0) {
            output += hoursD + ":";
        }
        //output += hoursD + ":" + minutesD + ":" + secondsD;
        output += minutesD + ":" + secondsD;

        return output;
    }

    public static final String PATH_FONTS = "fonts/VinpearlFonts.ttf";
    public static Typeface getTypeFace(Context context){
        Typeface mTypeface = Typeface.createFromAsset(context.getAssets(),
                PATH_FONTS);
        return mTypeface;
    }




}
