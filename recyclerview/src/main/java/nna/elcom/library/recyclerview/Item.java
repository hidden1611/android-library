package nna.elcom.library.recyclerview;

/**
 * Created by Ann on 4/4/16.
 */
public class Item {

    String textName;
    String urlIcon;

    public String getTextName() {
        return textName;
    }

    public void setTextName(String textName) {
        this.textName = textName;
    }

    public String getUrlIcon() {
        return urlIcon;
    }

    public void setUrlIcon(String urlIcon) {
        this.urlIcon = urlIcon;
    }
}
