package nna.elcom.library.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.List;

/**
 * Created by Ann on 4/4/16.
 */
public class RecyclerView extends android.support.v7.widget.RecyclerView {

    private int columnWidth = -1;
    private ItemAdapter itemAdapter;
    private GridLayoutManager manager;

    public RecyclerView(Context context) {
        super(context);
        init(context, null);

    }

    public RecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, heightSpec);
        if (columnWidth > 0) {
            int spanCount = Math.max(1, getMeasuredWidth() / columnWidth);
            manager.setSpanCount(spanCount);
        }
    }

    public void init(Context context, AttributeSet attrs){

        if (attrs != null) {
            int[] attrsArray = { android.R.attr.columnWidth };
            TypedArray array = context.obtainStyledAttributes(
                    attrs, attrsArray);
            columnWidth = array.getDimensionPixelSize(0, -1);
            array.recycle();
        }

        manager = new GridLayoutManager(getContext(), 1);
        setLayoutManager(manager);
        itemAdapter = new ItemAdapter();
        itemAdapter.setOnItemClickListener(new ItemAdapter.OnItemClickListener() {
            @Override
            public void OnClick(View view, int position) {
                Log.d("TAG", "OnClick " + position);

            }
        });
        setAdapter(itemAdapter);
    }

    public void setData(List<Item> items){
        itemAdapter.setItems(items);
    }










}
